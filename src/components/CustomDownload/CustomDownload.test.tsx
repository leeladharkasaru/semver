import { render } from '@testing-library/react';
import React from 'react';
import { INVOICE_DOWNLOAD_AS_BUTTON } from '../../common/constants';
import CustomDownload from './CustomDownload';
import { CustomDownloadProps } from './CustomDownload.types';

describe('Download Order Component', () => {
  const props: CustomDownloadProps = {
    includePricing: true,
    exportDownload: jest.fn(),
    labels: {
      downloadInlabel: 'Download',
      downloadOutlabel: 'Download As',
    },
  };

  const renderComponent = () => render(<CustomDownload {...props} />);

  it('Renders the Custom Download AS button for desktop', () => {
    const { getByText } = renderComponent();
    // Act
    expect(getByText(INVOICE_DOWNLOAD_AS_BUTTON));
  });
});
