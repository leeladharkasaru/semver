import { ComponentStoryObj, Meta } from '@storybook/react';
import React from 'react';
import { Quote } from '../../common/models/quote/QuoteDetails';
import CustomDownload from './CustomDownload';

const meta: Meta<typeof CustomDownload> = {
  component: CustomDownload,
  title: 'Components/CustomDownload',
  argTypes: {},
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof CustomDownload>;

export const CustomDownloadDefault: ComponentStory = (args) => (
  <CustomDownload {...args} />
);

export const CustomDownloadQuoteDefault: ComponentStory = (args) => (
  <CustomDownload {...args} />
);

const OrderResponse = {
  result: {
    lineItems: [
      {
        itemNumber: '12525',
        itemType: null,
        sendToMincronDirectly: false,
        unitOfMeasureDisplay: '/BDL',
        displayQuantityMsg: null,
        pdpUrl:
          '/productDetail/C-012525?skuId=12525&Color=Weathered+Wood&Dimension=17%22+x+40%22',
        description: null,
        productOrItemNumber: 'GAFGSFOWW',
        unitPriceNull: false,
        subTotal: 48.54,
        productOrItemDescription: 'GAF FO GRAND SEQUOIA WEATHER WD',
        isAddedToFavorites: true,
        orderQuantity: 1,
        productImageUrl: '/images/large/12525_default_thumb.jpg',
        unitPrice: 48.54,
        itemUnitPrice: 0,
        quantity: 1,
        itemOrProductDescription:
          'GAF FO GRAND SEQUOIA WEATHER WD 0650900FO               5BDL/SQ',
        productId: 'C-012525',
        unitOfMeasure: 'BDL',
        skuPDPDisplayTitle:
          'GAF 17" x 40" Grand Sequoia&reg; Shingles Weathered Wood',
        orderUnitOfMeasure: 'BDL',
        productOnErrorImageUrl: '/images/large/brand/gaf_brand_thumb.jpg',
        productNumber: null,
        orderOfMeasureDisplay: '/BDL',
        orderSubTotal: 48.54,
        shipQuantity: 1,
        ATGItemMapping: null,
        itemSubTotal: 0,
        nonStockItem: false,
        sendDescriptionOnlyToMincron: false,
        lineComments: '',
        orderUnitPrice: 48.54,
        quantityMsg: null,
        extendedDescription: '0650900FO               5BDL/SQ',
        subTotalNull: false,
      },
      {
        itemNumber: '729634',
        itemType: null,
        sendToMincronDirectly: false,
        unitOfMeasureDisplay: '/BDL',
        displayQuantityMsg: null,
        pdpUrl:
          '/productDetail/C-635001?skuId=729634&Color=Cedar+Falls&Dimension=13-1%2F4%22+x+39-3%2F8%22',
        description: null,
        productOrItemNumber: 'GAFTMBLBSGFOCF',
        unitPriceNull: false,
        subTotal: 6000,
        productOrItemDescription: 'GAF FO TMBLINE HDZ SG CDR FA',
        isAddedToFavorites: true,
        orderQuantity: 150,
        productImageUrl: '/images/large/729634_default_thumb.jpg',
        unitPrice: 40,
        itemUnitPrice: 0,
        quantity: 150,
        itemOrProductDescription:
          'GAF FO TMBLINE HDZ SG CDR FA 0487165FO        HDZ STAINGUARD TIMBERLINE  3BDL/SQ 52 BDLS / PALLET  CEDAR FALLS',
        productId: 'C-635001',
        unitOfMeasure: 'BDL',
        skuPDPDisplayTitle:
          'GAF 13-1/4" x 39-3/8" Timberline HDZ&trade; Shingles with StainGuard Protection Cedar Falls',
        orderUnitOfMeasure: 'BDL',
        productOnErrorImageUrl: '/images/large/brand/gaf_brand_thumb.jpg',
        productNumber: null,
        orderOfMeasureDisplay: '/BDL',
        orderSubTotal: 6000,
        shipQuantity: 150,
        ATGItemMapping: null,
        itemSubTotal: 0,
        nonStockItem: false,
        sendDescriptionOnlyToMincron: false,
        lineComments: '',
        orderUnitPrice: 40,
        quantityMsg: null,
        extendedDescription:
          '0487165FO        HDZ STAINGUARD TIMBERLINE  3BDL/SQ 52 BDLS / PALLET  CEDAR FALLS',
        subTotalNull: false,
      },
    ],
    DTInfo: null,
    branchDTEnabled: true,
    hasDTError: false,
    DTEnabled: true,
    DTStatus: null,
    EVEligibleForUpgrade: false,
    DTPhotos: null,
    EVReportId: 0,
    order: {
      atgOrderID: null,
      shipInfoMsg: null,
      orderId: 'TD44632',
      orderRelatedDocuments: null,
      otherCharges: 0,
      totalMsg: null,
      source: 'Yes',
      subTotal: 6048.54,
      taxNull: false,
      creditHold: '',
      purchaseOrderNo: '',
      orderPlacedDate: '01-08-2024',
      total: 6308.63,
      shipping: {
        shippingBranch: '300',
        address: {
          address3: '',
          address2: '',
          city: 'EASTVILLE',
          address1: 'TEST',
          postalCode: '23347',
          state: 'VA',
        },
        shippingBranchDisplayName: 'DENVER HUB BRANCH',
        shippingMethod: 'O',
      },
      inStore: 'Yes',
      showShipTips: null,
      orderStatusValue: 'Pending',
      otherChargesNull: false,
      UUID: '',
      sellingBranch: '300',
      onHold: true,
      specialInstruction: '["Partial release of Q2O"]',
      displayTotalMsg: null,
      showDropDown: null,
      tax: 260.09,
      accountId: '280381',
      shipTipsMsg: null,
      orderStatusCode: 'N',
      shipContentMsg: null,
      accountToken: '3XJXjM7RhLMGwoIcNvg8CSUiM9SAPbf2hmlMhFhMZqE=',
      job: {
        jobName: 'SHOP',
        jobLocationAddress2: null,
        jobLocationAddress3: null,
        jobLocationZipCode: null,
        jobLocationCity: null,
        jobLocationAddress1: null,
        customerName: null,
        jobNumber: 'SHOP',
        customerTelephoneNumber: null,
        jobLocationState: null,
      },
      subTotalNull: false,
      creditCardTokenExists: false,
      showDeliveryNotification: true,
      totalNull: false,
    },
  },
  success: true,
  messages: null,
};

const quoteResponse: Quote = {
  workTypeName: 'Remodel',
  quoteType: '',
  otherCharges: 0,
  postalCode: '80216',
  gst: 0,
  subTotal: 136,
  createdUserName: 'BDD User4 AAXIS',
  mailingAddress3: '',
  standardQuoteItems: [
    {
      unitPrice: 68,
      itemNumber: '735299',
      itemType: 'I',
      mincronDisplayName: '',
      quantity: 1,
      color: null,
      productId: 'C-734607',
      unitOfMeasure: 'BDL',
      displayName:
        'GAF 13-1/4" x 39-3/8" Timberline&reg; UHDZ&trade; Shingles with StainGuard Plus&trade; Barkwood',
      itemTotalPrice: 68,
      imageOnErrorUrl: '/images/large/brand/gaf_brand_thumb.jpg',
      currencySymbol: '$',
      productNumber: 'GAFTLUHDZSFBA',
      formatItemTotalPrice: '68.00',
      bodyComments: null,
      PDPUrl:
        '/productDetail/C-734607?skuId=735299&color=Barkwood&size=13-1%2F4%22+x+39-3%2F8%22',
      deleteStatus: null,
      formatUnitPrice: '68.00',
      imageURL: '/images/large/735299_default_thumb.jpg',
      stickerImageURL: null,
      id: '',
      itemDescription: 'GAF SF TL UHDZ SGPL BARKWOOD',
      dimensions: null,
    },
    {
      unitPrice: 68,
      itemNumber: '735304',
      itemType: 'I',
      mincronDisplayName: '',
      quantity: 1,
      color: null,
      productId: 'C-734607',
      unitOfMeasure: 'BDL',
      displayName:
        'GAF 13-1/4" x 39-3/8" Timberline&reg; UHDZ&trade; Shingles with StainGuard Plus&trade; Weathered Wood',
      itemTotalPrice: 68,
      imageOnErrorUrl: '/images/large/brand/gaf_brand_thumb.jpg',
      currencySymbol: '$',
      productNumber: 'GAFTLUHDZSFWW',
      formatItemTotalPrice: '68.00',
      bodyComments: null,
      PDPUrl:
        '/productDetail/C-734607?skuId=735304&color=Weathered+Wood&size=13-1%2F4%22+x+39-3%2F8%22',
      deleteStatus: null,
      formatUnitPrice: '68.00',
      imageURL: '/images/large/735304_default_thumb.jpg',
      stickerImageURL: null,
      id: 'null',
      itemDescription: 'GAF SF TL UHDZ SGPL WEATH WOOD',
      dimensions: 'null',
    },
  ],
  mailingAddress2: '',
  mailingAddress1: '3000 BRIGHTON BLVD',
  siteID: 'null',
  formatQuoteTotal: '136.00',
  id: '554700089',
  mailingState: 'Colorado',
  state: 'Colorado',
  createdUser: null,
  quoteItems: [
    {
      unitPrice: 0,
      itemNumber: '0',
      itemType: 'E',
      mincronDisplayName: '',
      quantity: 5,
      color: null,
      productId: null,
      unitOfMeasure: 'EA',
      displayName: 'CUSTOMITEM_7510518',
      itemTotalPrice: 0,
      imageOnErrorUrl: '/images/default_not_found.jpg',
      currencySymbol: '$',
      productNumber: '/WEB29208956',
      formatItemTotalPrice: '0.00',
      bodyComments: null,
      PDPUrl: null,
      deleteStatus: null,
      formatUnitPrice: '0.00',
      imageURL: '/images/default_not_found.jpg',
      stickerImageURL: null,
      id: 'null',
      itemDescription: 'CUSTOMITEM_7510518',
      dimensions: null,
    },
    {
      unitPrice: 68,
      itemNumber: '735299',
      itemType: 'I',
      mincronDisplayName: '',
      quantity: 1,
      color: null,
      productId: 'C-734607',
      unitOfMeasure: 'BDL',
      displayName:
        'GAF 13-1/4" x 39-3/8" Timberline&reg; UHDZ&trade; Shingles with StainGuard Plus&trade; Barkwood',
      itemTotalPrice: 68,
      imageOnErrorUrl: '/images/large/brand/gaf_brand_thumb.jpg',
      currencySymbol: '$',
      productNumber: 'GAFTLUHDZSFBA',
      formatItemTotalPrice: '68.00',
      bodyComments: null,
      PDPUrl:
        '/productDetail/C-734607?skuId=735299&color=Barkwood&size=13-1%2F4%22+x+39-3%2F8%22',
      deleteStatus: null,
      formatUnitPrice: '68.00',
      imageURL: '/images/large/735299_default_thumb.jpg',
      stickerImageURL: null,
      id: 'null',
      itemDescription: 'GAF SF TL UHDZ SGPL BARKWOOD',
      dimensions: null,
    },
    {
      unitPrice: 68,
      itemNumber: '735304',
      itemType: 'I',
      mincronDisplayName: '',
      quantity: 1,
      color: null,
      productId: 'C-734607',
      unitOfMeasure: 'BDL',
      displayName:
        'GAF 13-1/4" x 39-3/8" Timberline&reg; UHDZ&trade; Shingles with StainGuard Plus&trade; Weathered Wood',
      itemTotalPrice: 68,
      imageOnErrorUrl: '/images/large/brand/gaf_brand_thumb.jpg',
      currencySymbol: '$',
      productNumber: 'GAFTLUHDZSFWW',
      formatItemTotalPrice: '68.00',
      bodyComments: null,
      PDPUrl:
        '/productDetail/C-734607?skuId=735304&color=Weathered+Wood&size=13-1%2F4%22+x+39-3%2F8%22',
      deleteStatus: null,
      formatUnitPrice: '68.00',
      imageURL: '/images/large/735304_default_thumb.jpg',
      stickerImageURL: null,
      id: 'null',
      itemDescription: 'GAF SF TL UHDZ SGPL WEATH WOOD',
      dimensions: null,
    },
  ],
  jobName: 'SHOP ACCOUNT',
  uploadedFiles: null,
  bidLockEmail: 'beckyhu@aaxisdigital.com',
  addtionalQuoteItems: [
    {
      unitPrice: 0,
      itemNumber: '0',
      itemType: 'E',
      mincronDisplayName: '',
      quantity: 5,
      color: null,
      productId: null,
      unitOfMeasure: 'EA',
      displayName: 'CUSTOMITEM_7510518',
      itemTotalPrice: 0,
      imageOnErrorUrl: '/images/default_not_found.jpg',
      currencySymbol: '$',
      productNumber: '/WEB29208956',
      formatItemTotalPrice: '0.00',
      bodyComments: null,
      PDPUrl: null,
      deleteStatus: null,
      formatUnitPrice: '0.00',
      imageURL: '/images/default_not_found.jpg',
      stickerImageURL: null,
      id: 'null',
      itemDescription: 'CUSTOMITEM_7510518',
      dimensions: null,
    },
  ],
  tax: 11.98,
  creationDate: '12-05-2023',
  formatSubTotal: '136.00',
  phoneNumber: '1234567890',
  quoteTotalPrice: 136,
  profileId: 'user508870',
  jobNumber: 'SHOP',
  status: 'CUSTOMER_REVIEW',
  expires: '01-04-2024',
  formatOtherCharges: '0.00',
  city: 'DENVER',
  displayName: '_AAXISBDD_408190',
  sourceOfSale: 'E',
  mailingZip: '80216',
  mincronId: '9265287',
  formatTax: '11.98',
  total: 147.98,
  mincronOrderId: 'NV68990',
  deleteStatus: false,
  lastModifiedUser: null,
  quoteNotes: 'Quote Rejected. Reason:Lost Bid',
  formatTotal: '147.98',
  atgOrderId: 'o105263829',
  address2: '',
  lastModifiedDate: 'null',
  mailingCity: 'DENVER',
  address1: '3000 BRIGHTON BLVD',
  lastModifiedUserEmail: '',
  documentName: 'null',
  accountNumber: '280381',
  lastModifiedDate4Integration: 'null',
  bidLockTimestamp: '2023-12-13-05.18.02.928510',
  creationDate4Integration: '2023-12-05 00:00:00.0000000',
  profileEmail: 'Ankita.Bhandula-con@becn.com',
  statusDescription: 'Printed',
  workType: 'R',
  pageControl: null,
};

CustomDownloadDefault.args = {
  includePricing: true,
  exportDownload: () => {},
  labels: {
    downloadInlabel: 'Download',
    downloadOutlabel: 'Download As',
  },
};

const exportQuoteD = () => {};
CustomDownloadQuoteDefault.args = {
  includePricing: false,
  exportDownload: exportQuoteD,
  labels: {
    downloadInlabel: 'Download',
    downloadOutlabel: 'Download As',
  },
};
