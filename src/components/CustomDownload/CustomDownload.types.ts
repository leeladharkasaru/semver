import { DownloadLabels } from '../DownloadOrder/DownloadOrder.types';

export interface CustomDownloadProps {
  includePricing?: boolean;
  exportDownload: Function;
  labels: DownloadLabels;
}

export interface DownloadFormProps {
  pricing: boolean;
  type: string;
}
