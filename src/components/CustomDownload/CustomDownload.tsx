import React, { useState } from 'react';
import { DOWNLOAD_FILE_TYPE } from '../../common/constants';
import { DownloadOrder, DownloadOrderProps } from '../DownloadOrder';
import { CustomDownloadProps, DownloadFormProps } from './CustomDownload.types';

const CustomDownload: React.FC<CustomDownloadProps> = ({
  includePricing,
  exportDownload,
  labels,
}) => {
  const [anchorEl, setAnchorEl] = useState<any>(null);
  const [downloadFormValues, setDownloadFormValues] = useState<DownloadFormProps>({
    pricing: includePricing,
    type: DOWNLOAD_FILE_TYPE.CSV,
  });

  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleChange = (e: any) => {
    setDownloadFormValues({
      ...downloadFormValues,
      [e.target.name]:
        e.target.type === 'checkbox' ? e.target.checked : e.target.value,
    });
  };
  const props: DownloadOrderProps = {
    open,
    type: downloadFormValues.type,
    pricing: downloadFormValues.pricing,
    handleChange,
    anchorEl,
    handleClose,
    handleClick,
    exportDownload,
    labels,
    includePricing,
  };
  return <DownloadOrder {...props} />;
};

export default CustomDownload;
