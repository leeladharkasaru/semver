import CustomDownload from './CustomDownload';

export type { CustomDownloadProps, DownloadFormProps } from './CustomDownload.types';
export { CustomDownload };
