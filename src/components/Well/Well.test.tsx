import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import React from "react";
import Well from "./Well";
import { WellProps } from "./Well.types";

describe("Well Component", () => {
    let props: WellProps;

    beforeEach(() => {
        props = {
            children: "Test Well",
            variant: "default",
            testId: "test-well",
            wellStyle: {
                container: {
                    backgroundColor: "red",
                },
            },
        } as any;
    });

    const renderComponent = () => render(<Well {...props} />);

    it("should render the well", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-well")).toBeInTheDocument();
    });

    it("should render with the default variant", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-well")).toHaveClass("becn-well--default");
    });

    it("should render with the contrast variant", () => {
        // Arrange
        props.variant = "contrast";

        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-well")).toHaveClass("becn-well--contrast");
    });

    it("should render with the subtle variant", () => {
        // Arrange
        props.variant = "subtle";

        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-well")).toHaveClass("becn-well--subtle");
    });

    it("should apply the wellStyle prop to the container", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-well")).toHaveStyle({ backgroundColor: "red" });
    });
});