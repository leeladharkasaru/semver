import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import Well from './Well';

const meta: Meta<typeof Well> = {
  component: Well,
  title: 'Components/Well',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Well>;

export const Default: ComponentStory = (args: any) => (
  <Well {...args} />
);
Default.args = {
  children: 'Well',
};

export const Contrast: ComponentStory = (args: any) => (
  <Well {...args} />
);
Contrast.args = {
  children: 'Well',
  variant: 'contrast',
};

export const Subtle: ComponentStory = (args: any) => (
  <Well {...args} />
);
Subtle.args = {
  children: 'Well',
  variant: 'subtle',
};

export const OverrideStyle: ComponentStory = (args: any) => (
  <Well {...args} />
);
OverrideStyle.args = {
  children: 'Well',
  variant: 'default',
  wellStyle: {
    container: {
      backgroundColor: 'red',
    },
  },
};
