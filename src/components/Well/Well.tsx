import './Well.scss';
import React from 'react';
import { Box } from '@material-ui/core';
import { WellProps } from './Well.types';

const internalClassName = 'becn-well';

const Well: React.FC<WellProps> = ({
  children,
  className = '',
  variant = 'default',
  testId,
  wellStyle,
}) => (
  <Box
    className={`${className} ${internalClassName} ${internalClassName}--${variant}`}
    data-testid={testId}
    style={wellStyle?.container}
  >
    {children}
  </Box>
);

export default Well;
