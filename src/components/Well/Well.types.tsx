export interface WellProps {
    className?: string;
    variant?: WellVariant;
    testId?: string;
    wellStyle?: WellStyle;
}

export type WellVariant = "default" | "contrast" | "subtle";

export type WellStyle = {
    container?: Object;
}
