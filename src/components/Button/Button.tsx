import React from 'react';
import Button from '@material-ui/core/Button/Button';
import { ButtonProps } from './Button.types';

const BeaconButton: React.FC<ButtonProps> = ({
  size,
  variant,
  color,
  disabled,
  text,
  onClick,
}) => (
  <Button
    type="button"
    onClick={onClick}
    disabled={disabled}
    variant={variant}
    color={color === 'primary' ? 'primary' : 'secondary'}
    size={size}
  >
    {text}
  </Button>
);

export default BeaconButton;
