import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import Button from './Button';

const meta: Meta<typeof Button> = {
  component: Button,
  title: 'Components/Button',
  argTypes: {},
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Button>;

export const Primary: ComponentStory = (args) => (
  <Button data-testid="button-id" {...args} />
);
Primary.args = {
  color: 'primary',
  disabled: false,
  text: 'Primary',
  variant: 'contained',
};
export const Outlined: ComponentStory = (args) => (
  <Button data-testid="button-id" {...args} />
);
Outlined.args = {
  color: 'primary',
  disabled: false,
  text: 'Outlined',
  variant: 'outlined',
};
export const Secondary: ComponentStory = (args) => (
  <Button data-testid="button-id" {...args} />
);
Secondary.args = {
  color: 'secondary',
  disabled: false,
  text: 'Secondary',
  variant: 'contained',
};

export const Disabled: ComponentStory = (args) => (
  <Button data-testid="button-id" {...args} />
);
Disabled.args = {
  color: 'secondary',
  disabled: true,
  text: 'Disabled',
  variant: 'contained',
};

export const Small: ComponentStory = (args) => (
  <Button data-testid="button-id" {...args} />
);
Small.args = {
  color: 'primary',
  disabled: false,
  size: 'small',
  text: 'Small',
  variant: 'contained',
};

export const Medium: ComponentStory = (args) => (
  <Button data-testid="button-id" {...args} />
);
Medium.args = {
  color: 'primary',
  disabled: false,
  size: 'medium',
  text: 'Medium',
  variant: 'contained',
};

export const Large: ComponentStory = (args) => (
  <Button data-testid="button-id" {...args} />
);
Large.args = {
  color: 'primary',
  disabled: false,
  size: 'large',
  text: 'Large',
  variant: 'contained',
};
