export interface TabsProps {
    id: string;
    value: number;
    items: TabItemProps[];
    testId?: string;
    tabStyle?: TabStyle;
    onTabChange: (index: number) => void;
}

export interface TabItemProps {
    index: number;
    label: string;
    panel: React.ReactNode;
}

export interface TabPanelProps {
    value: number;
    index: number;
    testId?: string;
    style?: Object;
}

export type TabStyle = {
    container?: Object;
    tabs?: Object;
    tab?: Object;
    tabPanel?: Object;
}
