import './Tabs.scss';
import React, { ChangeEvent } from 'react';
import {
  Box, Tab, Tabs as MaterialTabs, useMediaQuery,
} from '@material-ui/core';
import { TabItemProps, TabPanelProps, TabsProps } from './Tabs.types';

const internalClassName = 'becn-tabs';

const TabPanel: React.FC<TabPanelProps> = ({
  children, value, index, testId, style,
}) => value === index && (
<div
  role="tabpanel"
  hidden={value !== index}
  id={`simple-tabpanel-${index}`}
  aria-labelledby={`simple-tab-${index}`}
  className={`${internalClassName}__tabpanel`}
  data-testid={testId ? `${testId}-tabpanel-${index}` : undefined}
  style={style}
>
  {children}
</div>
);

const Tabs: React.FC<TabsProps> = ({
  id,
  testId,
  value,
  items,
  tabStyle,
  onTabChange,
}) => {
  // Is mobile?
  const isMobile = useMediaQuery('(max-width: 600px)');

  // Handle changes to the tabs
  const handleTabChange = (event: ChangeEvent<{ value: number }>, newValue: number) => {
    onTabChange(newValue);
  };

  // Generate additional props for the tabs
  const a11yProps = (index: number) => ({
    id: `${id}-tab-${index}`,
    'aria-controls': `${id}-${index}`,
  });

  // Render the tabs
  return (
    <Box
      data-testid={testId ? `${testId}-container` : undefined}
      className={`${internalClassName}__container`}
      style={tabStyle?.container}
    >
      <MaterialTabs
        data-testid={testId ? `${testId}-tabs` : undefined}
        variant={isMobile ? 'scrollable' : 'fullWidth'}
        scrollButtons={isMobile ? 'off' : 'auto'}
        className={`${internalClassName}__tabs`}
        value={value}
        style={tabStyle?.tabs}
        onChange={handleTabChange}
      >
        {items.map((item: TabItemProps) => (
          <Tab
            data-testid={testId ? `${testId}-tab-${item.index}` : undefined}
            className={`${internalClassName}__tab`}
            key={item.index}
            label={item.label}
            style={tabStyle?.tab}
            {...a11yProps(item.index)}
          />
        ))}
      </MaterialTabs>
      {items.map((item: TabItemProps) => (
        <TabPanel
          key={item.index}
          value={value}
          index={item.index}
          children={item.panel}
          testId={testId}
          style={tabStyle?.tabPanel}
        />
      ))}
    </Box>
  );
};

export default Tabs;
