import Tabs from './Tabs';

export type {
  TabsProps,
  TabItemProps,
  TabPanelProps,
  TabStyle,
} from './Tabs.types';
export { Tabs };
