import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import Tabs from './Tabs';

const meta: Meta<typeof Tabs> = {
  component: Tabs,
  title: 'Components/Tabs',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Tabs>;

export const Default: ComponentStory = (args: any) => (
  <Tabs {...args} />
);
Default.args = {
  id: 'becn-tabs',
  value: 4,
  items: [
    {
      index: 0,
      label: 'Tab 1',
      panel: <div>Tab 1 content</div>,
    },
    {
      index: 1,
      label: 'Tab 2',
      panel: <div>Tab 2 content</div>,
    },
    {
      index: 2,
      label: 'Tab 3',
      panel: <div>Tab 3 content</div>,
    },
    {
      index: 3,
      label: 'Tab 4',
      panel: <div>Tab 4 content</div>,
    },
    {
      index: 4,
      label: 'Tab 5',
      panel: <div>Tab 5 content</div>,
    },
  ],
  onTabChange: (index: number) => {
    console.log(`Tab changed to index ${index}`);
  },
};

export const QuoteTabs: ComponentStory = (args: any) => (
  <Tabs {...args} />
);
QuoteTabs.args = {
  id: 'becn-tabs',
  value: 3,
  items: [
    {
      index: 0,
      label: 'Draft Quotes',
      panel: <div />,
    },
    {
      index: 1,
      label: 'In Progress Quotes',
      panel: <div />,
    },
    {
      index: 2,
      label: 'Received Quotes',
      panel: <div />,
    },
    {
      index: 3,
      label: 'Active Quotes',
      panel: <div />,
    },
  ],
  onTabChange: (index: number) => {
    console.log(`Tab changed to index ${index}`);
  },
};

export const OverrideStyle: ComponentStory = (args: any) => (
  <Tabs {...args} />
);
OverrideStyle.args = {
  id: 'becn-tabs',
  value: 4,
  items: [
    {
      index: 0,
      label: 'Tab 1',
      panel: <div>Tab 1 content</div>,
    },
    {
      index: 1,
      label: 'Tab 2',
      panel: <div>Tab 2 content</div>,
    },
    {
      index: 2,
      label: 'Tab 3',
      panel: <div>Tab 3 content</div>,
    },
    {
      index: 3,
      label: 'Tab 4',
      panel: <div>Tab 4 content</div>,
    },
    {
      index: 4,
      label: 'Tab 5',
      panel: <div>Tab 5 content</div>,
    },
  ],
  tabStyle: {
    container: { backgroundColor: 'red' },
    tabs: { backgroundColor: 'blue' },
    tab: { backgroundColor: 'green' },
    tabPanel: { backgroundColor: 'yellow' },
  },
  onTabChange: (index: number) => {
    console.log(`Tab changed to index ${index}`);
  },
};
