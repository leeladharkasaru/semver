import '@testing-library/jest-dom';
import { render, screen, fireEvent } from '@testing-library/react';
import React from 'react';
import Tabs from './Tabs';
import { TabsProps } from './Tabs.types';

describe('Tabs Component', () => {
  let props: TabsProps;

  beforeEach(() => {
    props = {
      id: 'test-tabs',
      testId: 'test-tabs',
      value: 0,
      items: [
        { index: 0, label: 'Tab 1', panel: <div>Panel 1</div> },
        { index: 1, label: 'Tab 2', panel: <div>Panel 2</div> },
      ],
      onTabChange: jest.fn(),
    };
  });

  const renderComponent = () => render(<Tabs {...props} />);

  it('renders without crashing', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-tabs-container')).toBeInTheDocument();
  });

  it('displays correct tabs', () => {
    // Act
    renderComponent();

    // Assert
    expect(screen.getByTestId('test-tabs-tab-0')).toHaveTextContent('Tab 1');
    expect(screen.getByTestId('test-tabs-tab-1')).toHaveTextContent('Tab 2');
  });

  it('displays correct panel', () => {
    // Act
    renderComponent();

    // Assert
    expect(screen.getByTestId('test-tabs-tabpanel-0')).toHaveTextContent('Panel 1');
    expect(screen.queryByTestId('test-tabs-tabpanel-1')).not.toBeInTheDocument();
  });

  it('handles tab change correctly', () => {
    // Act
    renderComponent();
    fireEvent.click(screen.getByTestId('test-tabs-tab-1'));

    // Assert
    expect(props.onTabChange).toHaveBeenCalledWith(1);
  });
});
