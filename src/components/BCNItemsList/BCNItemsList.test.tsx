import { render } from '@testing-library/react';
import React from 'react';
import BCNItemsList from './BCNItemsList';
import { BCNItemsListProps } from './BCNItemsList.types';

describe('Test Component', () => {
  let props: BCNItemsListProps;

  beforeEach(() => {
    props = {
      children: 'bar',
    };
  });

  const renderComponent = () => render(<BCNItemsList {...props} />);

  it('should render beacon text correctly', () => {
    // Assign
    const { getByTestId } = renderComponent();
    // Act
    const component = getByTestId('BCNItemsList');
    // Assert
    expect(component).toBeDefined();
  });
});
