import { Box } from '@material-ui/core';
import React from 'react';
import { BCNItemsListProps } from './BCNItemsList.types';

const BCNItemsList: React.FC<BCNItemsListProps> = ({ children }) => (
  <Box data-testid="BCNItemsList">{children}</Box>
);

export default BCNItemsList;
