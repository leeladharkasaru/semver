import { Button, Input, TableCell } from '@material-ui/core';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import React, { ChangeEvent, useState } from 'react';
import Summary from '../Summary/Summary';
import BCNTable from '../Table/Table';
import BCNItemsList from './BCNItemsList';

const meta: ComponentMeta<typeof BCNItemsList> = {
  component: BCNItemsList,
  title: 'Components/BCNItemsList',
  argTypes: {},
};
export default meta;

type Story = ComponentStory<typeof BCNItemsList>;

const standardQuoteItems = [
  {
    unitPrice: 67.75,
    itemNumber: '407262',
    itemType: 'I',
    mincronDisplayName: '',
    quantity: 1,
    color: null,
    productId: 'C-349425',
    unitOfMeasure: 'BDL',
    displayName:
      'Atlas Roofing StormMaster&reg; Slate Shingles with Scotchgard&trade; Protector Weathered Slate',
    itemTotalPrice: 67.75,
    imageOnErrorUrl: '/images/large/brand/atlas_roofing_brand_thumb.jpg',
    currencySymbol: '$',
    productNumber: 'ATLSMSLSPWE',
    formatItemTotalPrice: '67.75',
    bodyComments: null,
    PDPUrl: '/productDetail/C-349425?skuId=407262&Color=Weathered+Slate',
    deleteStatus: null,
    formatUnitPrice: '67.75',
    imageURL: '/images/large/407262_default_thumb.jpg',
    stickerImageURL: null,
    id: null,
    itemDescription: 'ATL AR SMASTER SLATE WEATHERED',
    dimensions: null,
  },
  {
    unitPrice: 26.66,
    itemNumber: '428681',
    itemType: 'I',
    mincronDisplayName: '',
    quantity: 1,
    color: null,
    productId: 'C-428681',
    unitOfMeasure: 'BDL',
    displayName: 'IKO Cambridge&reg; Shingles Aged Redwood',
    itemTotalPrice: 26.66,
    imageOnErrorUrl: '/images/large/brand/iko_brand_thumb.jpg',
    currencySymbol: '$',
    productNumber: 'IKOCAMAR',
    formatItemTotalPrice: '26.66',
    bodyComments: null,
    PDPUrl: '/productDetail/C-428681?skuId=428681&Color=Aged+Redwood',
    deleteStatus: null,
    formatUnitPrice: '26.66',
    imageURL: '/images/large/428681_default_thumb.jpg',
    stickerImageURL: null,
    id: null,
    itemDescription: 'IKO+CAMBRIDGE AR AGED REDWOD',
    dimensions: null,
  },
];

const DesktopSummary = {
  summary: {
    'Sub Total': '120',
    'Other Charges': '5',
    Tax: 5,
    Total: 20,
  },
  label: 'ORDER SUMMARY',
  summaryStyle: {
    summaryContainer: {
      marginLeft: '50%',
      marginTop: '2.25rem',
    },
    headingStyle: {
      //   height: "3.5rem",
      fontFamily: 'Inter',
      fontSize: '1.25rem',
      fontWeight: 600,
      lineHeight: '1.5rem',
      letterSpacing: 0,
      textAlign: 'left',
      color: '#242424',
    },
    listWapper: {
      borderBottom: '1px solid #DDD',
      borderTop: '1px solid #DDD',
      width: '27.813rem',
    },
    listBoxStyle: {
      display: 'flex',
      borderBottom: '1px solid #DDD',
    },

    summaryListLabelStyle: {
      fontFamily: 'Proxima Nova',
      fontSize: '0.875rem',
      marginTop: '0.594rem',

      fontWeight: 700,
      lineHeight: '1.313rem',
      letterSpacing: 0,
      textAlign: 'left',
    },
    summaryListHeaderStyle: {
      fontFamily: 'Proxima Nova',
      fontSize: '0.875rem',
      marginTop: '0.594rem',
      fontWeight: 700,
      width: '13.438rem',
      lineHeight: '1.313rem',
      letterSpacing: '0em',
      textAlign: 'left',
    },
  },
};
interface ItemProps {
  Product?: any;
  Details?: any;
  Unit?: any;
  Quantity?: any;
  Action?: any;
}

const SampleTable = {
  columns: [
    { header: 'Product', type: 'image' },
    { header: 'Details' },
    { header: 'Unit' },
    { header: 'Quantity', type: 'text' },
    { header: 'Action', type: 'button' },
  ],
  meta: {
    cell: ({ value, style, type }: any) => {
      const [changeValue, setValue] = useState(value);
      const changehandler = (e: ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value);
      };
      return (
        <TableCell style={style}>
          {type === 'button' ? (
            <Button>{value}</Button>
          ) : type === 'text' ? (
            <Input value={changeValue} onChange={changehandler} />
          ) : type === 'image' ? (
            <img src={value} style={style} />
          ) : (
            value
          )}
        </TableCell>
      );
    },
  },
  rows: standardQuoteItems.map((item, index) => {
    const itemData = {
      Product: `https://beaconproplus.com${item.imageURL}`,
      Details: item.displayName,
      Unit: item.unitOfMeasure,
      Quantity: item.quantity,
      Action: 'Delete',
    };
    const row = { cellData: itemData };
    return row;
  }),
  columnStyles: [
    {
      color: 'rgba(0,0,0,.54)',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      width: 'fit-content',
      fontSize: '1rem',
      fontWeight: 600,
    },
    {
      color: 'rgba(0,0,0,.54)',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 600,
    },
    {
      color: 'rgba(0,0,0,.54)',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 600,
    },
    {
      color: 'rgba(0,0,0,.54)',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 600,
    },
    {
      color: 'rgba(0,0,0,.54)',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 600,
    },
  ],
  rowStyle: [
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      // background: "red",
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
    },
  ],
};

export const DesktopBCNItemsList: Story = (args) => <BCNItemsList {...args} />;

DesktopBCNItemsList.args = {
  children: (
    <>
      <BCNTable {...SampleTable} />
      <Summary {...DesktopSummary} />
    </>
  ),
};
