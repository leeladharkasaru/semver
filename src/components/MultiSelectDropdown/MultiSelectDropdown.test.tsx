import { render } from "@testing-library/react";
import React from "react";
import MultiSelectDropdown from "./MultiSelectDropdown";
import { MultiSelectDropdownProps } from "./MultiSelectDropdown.types";
import { THICKNESS } from "./data";

describe("MultiSelectDropdown Component", () => {
  const props: MultiSelectDropdownProps = {
    name: "thickness",
    label: "Thickness",
    items: THICKNESS,
    onChange: jest.fn(),
  };

  const renderComponent = () => render(<MultiSelectDropdown {...props} />);

  it("it should render MultiSelectDropdown", () => {
    const { getAllByText } = renderComponent();
    expect(getAllByText(props.label as string)[0]).toBeDefined();
  });
});
