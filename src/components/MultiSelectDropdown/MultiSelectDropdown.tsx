import { InputLabel } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import React from 'react';
import './MultiSelectDropdown.scss';
import { MultiSelectDropdownProps } from './MultiSelectDropdown.types';
import { INTERNAL_CLASS_NAME, MENU_PROPS } from './data';

const MultiSelectDropdown: React.FC<MultiSelectDropdownProps> = ({
  formControlClassName,
  inputLableClassName,
  selectClassName,
  menuItemClassName,
  checkboxClassName,
  listItemClassName,
  selectId,
  labelId,
  items,
  value,
  defaultValue,
  valueSeparator = ',',
  separatorPreFix = '',
  separatorPostFix = ' ',
  ellipsisPostSelectedItems,
  label,
  name,
  itemTextKey = 'label',
  itemValueKey = 'value',
  itemSelectedKey = 'selected',
  radius = 'normal',
  fullWidth,
  disabled,
  testId,
  dropDownListStyle,
  variant = 'outlined',
  onChange,
  renderValue
}) => {
  let initialData: string[] = value && value.split(valueSeparator);

  if (!initialData || !initialData.length) {
    initialData = [];
    items.forEach((item) => {
      if (item[itemSelectedKey]) {
        initialData.push(item[itemValueKey]);
      }
    });
  }
  // const initialData: string[] = valuesArray || selectedArray || [];
  const [selectedValues, setSelectedValues] =
    React.useState<string[]>(initialData);

  // Handle OnChange event of the Dropdown
  const handleOnChange = (event: any) => {
    const { value: targetValue } = event.target;
    // const selectedValues = value;

    // Set selected values to the local state
    setSelectedValues(targetValue);

    // If a custom onChange is provided,then execute it by sending the selected values as argument
    if (onChange) {
      onChange(selectedValues, name);
    }
  };

  // Handle renderValue event of the Dropdown
  const handleRenderValue = (selected: any) => {
    const values = selected;

    // If a custom render value is provided, use it
    if (renderValue) {
      return renderValue(values, name);
    }
    const names: string[] = [];

    // Get all the names of the selected values
    items.forEach((item) => {
      if (values.indexOf(item[itemValueKey]) !== -1) {
        names.push(item[itemTextKey]);
      }
    });

    // In case of selected values are more than 2 then show ellipsis (...) after the last value
    if (ellipsisPostSelectedItems && names.length > ellipsisPostSelectedItems) {
      names.length = ellipsisPostSelectedItems;
      names.push('...');
    }
    return names.join(
      (separatorPreFix || '') + valueSeparator + (separatorPostFix || '')
    );
  };

  // If selected values or default value are not defined
  const isEmpty =
    (value === undefined || value === '') &&
    (defaultValue === undefined || defaultValue === '');

  // If any value is selected then sort the item list to show the selected values on top of the list
  if (selectedValues.length) {
    items.sort((item) =>
      selectedValues.indexOf(item[itemValueKey]) !== -1 ? -1 : 1
    );
  }

  return (
    <FormControl
      variant={variant}
      fullWidth={fullWidth}
      className={formControlClassName}
      style={dropDownListStyle?.container}>
      {label && (
        <InputLabel
          id={labelId}
          className={`${inputLableClassName} ${INTERNAL_CLASS_NAME}__label ${
            disabled ? `${INTERNAL_CLASS_NAME}__label--disabled` : ''
          }`}>
          {label}
        </InputLabel>
      )}
      <Select
        id={selectId}
        label={label}
        labelId={labelId}
        data-testid={testId}
        IconComponent={(props) => (
          <ExpandMoreIcon
            style={{ width: '1rem', height: '1rem' }}
            {...props}
          />
        )}
        name={name}
        className={`${selectClassName} ${INTERNAL_CLASS_NAME} ${INTERNAL_CLASS_NAME}--radius-${radius} ${
          isEmpty ? `${INTERNAL_CLASS_NAME}--empty` : ''
        }`}
        multiple
        defaultValue={defaultValue}
        disabled={disabled}
        value={selectedValues}
        style={dropDownListStyle?.select}
        onChange={handleOnChange}
        renderValue={handleRenderValue}
        MenuProps={MENU_PROPS}
        variant={variant}>
        {items.map((item, index) => (
          <MenuItem
            key={index}
            value={item[itemValueKey]}
            className={menuItemClassName}>
            <Checkbox
              className={checkboxClassName}
              checked={selectedValues.indexOf(item[itemValueKey]) !== -1}
            />
            <ListItemText
              primary={item[itemTextKey]}
              className={listItemClassName}
            />
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default MultiSelectDropdown;
