import { DropDownListItem } from '../DropDownList';
import {
  MenuPropsStyle,
  MultiSelectDropDownListStyle,
} from './MultiSelectDropdown.types';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

export const MENU_PROPS: MenuPropsStyle = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 9 + ITEM_PADDING_TOP,
      width: 200,
    },
  },
};
export const INTERNAL_CLASS_NAME = 'bcn-dropdownlist';

export const THICKNESS: DropDownListItem[] = [
  {
    label: '0.1"',
    value: '4294800831',
  },
  {
    label: '0.2"',
    value: '4294800832',
  },
  {
    label: '0.3"',
    value: '4294800833',
  },
  {
    label: '0.4"',
    value: '4294800834',
  },
  {
    label: '0.5"',
    value: '4294800835',
  },
  {
    label: '0.6"',
    value: '4294800836',
  },
  {
    label: '0.7"',
    value: '4294800837',
  },
];
export const THICKNESS_WITH_PRE_SELECTED_ITEMS: any[] = [
  {
    label: '0.1"',
    value: '4294800831',
    selected: false,
  },
  {
    label: '0.2"',
    value: '4294800832',
    selected: false,
  },
  {
    label: '0.3"',
    value: '4294800833',
    selected: true,
  },
  {
    label: '0.4"',
    value: '4294800834',
    selected: false,
  },
  {
    label: '0.5"',
    value: '4294800835',
    selected: true,
  },
  {
    label: '0.6"',
    value: '4294800836',
    selected: false,
  },
  {
    label: '0.7"',
    value: '4294800837',
    selected: true,
  },
];
export const DROP_DOWN_LIST_STYLE: MultiSelectDropDownListStyle = {
  container: {
    minWidth: MENU_PROPS.PaperProps.style.width,
  },
};
