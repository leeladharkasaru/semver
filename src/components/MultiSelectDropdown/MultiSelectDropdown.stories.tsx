import { ComponentMeta } from '@storybook/react';
import React from 'react';
import MultiSelectDropdown from './MultiSelectDropdown';
import {
  MultiSelectDropDownListStyle,
  MultiSelectDropdownProps,
} from './MultiSelectDropdown.types';
import {
  DROP_DOWN_LIST_STYLE,
  THICKNESS,
  THICKNESS_WITH_PRE_SELECTED_ITEMS,
} from './data.ts';

const meta: ComponentMeta<typeof MultiSelectDropdown> = {
  component: MultiSelectDropdown,
  title: 'Components/MultiSelectDropdown',
  argTypes: {},
};
export default meta;

const dropDownListStyle: MultiSelectDropDownListStyle = {
  ...DROP_DOWN_LIST_STYLE,
  container: { minWidth: '350px' },
};

// Handle the on change to get the selected values and the field name
const handleChange = (values: string[], name: string) => {
  console.log('values', values, 'name', name);
};

// Show with default features
export const Default = (args: MultiSelectDropdownProps) => (
  <MultiSelectDropdown {...args} />
);
const defaultProps: MultiSelectDropdownProps = {
  name: 'thickness',
  label: 'Thickness',
  items: THICKNESS,
  dropDownListStyle: DROP_DOWN_LIST_STYLE,
  onChange: handleChange,
};
Default.args = defaultProps;

// Show with custom width
export const CustomWidth = (args: MultiSelectDropdownProps) => (
  <MultiSelectDropdown {...args} />
);
const customWidthProps: MultiSelectDropdownProps = {
  name: 'thickness',
  label: 'Thickness',
  items: THICKNESS,
  dropDownListStyle,
  onChange: handleChange,
};
CustomWidth.args = customWidthProps;

// Show with custom width
export const PreSelectedItems = (args: MultiSelectDropdownProps) => (
  <MultiSelectDropdown {...args} />
);
const preSelectedItemsProps: MultiSelectDropdownProps = {
  name: 'thickness',
  label: 'Thickness',
  items: THICKNESS_WITH_PRE_SELECTED_ITEMS,
  dropDownListStyle,
  onChange: handleChange,
};
PreSelectedItems.args = preSelectedItemsProps;

// Show with full width
export const FullWidth = (args: MultiSelectDropdownProps) => (
  <MultiSelectDropdown {...args} />
);
const fullWidthProps: MultiSelectDropdownProps = {
  name: 'thickness',
  label: 'Thickness',
  items: THICKNESS,
  fullWidth: true,
  onChange: handleChange,
};
FullWidth.args = fullWidthProps;

// Show with ellipsis after the provided numbers of the selected items.
export const ShowEllipsis = (args: MultiSelectDropdownProps) => (
  <MultiSelectDropdown {...args} />
);
const showEllipsisProps: MultiSelectDropdownProps = {
  name: 'thickness',
  label: 'Thickness',
  items: THICKNESS,
  dropDownListStyle: DROP_DOWN_LIST_STYLE,
  ellipsisPostSelectedItems: 2,
  onChange: handleChange,
};
ShowEllipsis.args = showEllipsisProps;

// Show with Pipe sign ("|") as a value separator that will be used to join
// the selected values and the display text relative to the selected values
export const PipeSignAsSeparator = (args: MultiSelectDropdownProps) => (
  <MultiSelectDropdown {...args} />
);

const pipeSignAsSeparatorProps: MultiSelectDropdownProps = {
  name: 'thickness',
  label: 'Thickness',
  items: THICKNESS,
  dropDownListStyle: DROP_DOWN_LIST_STYLE,
  valueSeparator: '|',
  separatorPreFix: ' ',
  separatorPostFix: ' ',
  onChange: handleChange,
};
PipeSignAsSeparator.args = pipeSignAsSeparatorProps;
