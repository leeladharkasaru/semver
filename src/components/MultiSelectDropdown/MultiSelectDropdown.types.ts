import { DropDownListRadius } from '../DropDownList/DropDownList.types';

export interface MultiSelectDropdownProps {
  formControlClassName?: string;
  inputLableClassName?: string;
  selectClassName?: string;
  menuItemClassName?: string;
  checkboxClassName?: string;
  listItemClassName?: string;
  selectId?: string;
  labelId?: string;
  items: Array<any>;
  value?: string;
  defaultValue?: string;
  valueSeparator?: string;
  separatorPreFix?: string;
  separatorPostFix?: string;
  ellipsisPostSelectedItems?: number;
  label?: string;
  name: string;
  itemTextKey?: string;
  itemValueKey?: string;
  itemSelectedKey?: string;
  radius?: DropDownListRadius;
  fullWidth?: boolean;
  disabled?: boolean;
  testId?: string;
  dropDownListStyle?: MultiSelectDropDownListStyle;
  variant?: "standard" | "outlined" | "filled";
  onChange: (selectedValues: string[], name: string) => void;
  renderValue?: (selectedValues: string[], name: string) => React.ReactNode;
}

export interface MultiSelectDropDownListStyle {
  [key: string]: React.CSSProperties;
}

export interface MenuPropsStyle {
  [key: string]: MultiSelectDropDownListStyle;
}
