import { render } from '@testing-library/react';
import React from 'react';
import GuidedOrderTable from './guided-order';
import { GuidedOrderProps } from './guided-order.types';

describe('Test Component', () => {
  let props: GuidedOrderProps;

  beforeEach(() => {
    props = {
    };
  });

  const renderComponent = () => render(<GuidedOrderTable {...props} />);

  it('should render beacon text correctly', () => {
    // Assign
    const { getByTestId } = renderComponent();
    // Act
    const component = getByTestId('guided-order');
    // Assert
    expect(component).toBeDefined();
  });
});
