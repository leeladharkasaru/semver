import { TableCell, TextField } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { ProductImageProps } from '../ProductImage';
import { BCNTable, Row, TableProps } from '../Table';
import * as res from './data';
import './guided-order.scss';

const GuidedOrderTable = () => {
  const [guidedRows, setGuidedRows] = useState<Row<any>[]>([]);

  useEffect(() => {
    const rows = res.response.items.map((item) => {
      const row = {
        '': false,
        Code: item.currentSKU,
        'Product Description': item,
        Price: item.currentSKU.unitPrice,
        Unit: item.currentSKU.currentUOM,
        Qty: 1
      };
      return { cellData: row };
    });
    setGuidedRows(rows);
  }, []);
  const imageProps: ProductImageProps = {
    src: 'https://dev-static.becn.digital/insecure/plain/images/large/C-635001_default_small.jpg',
    alt: 'Product Image',
    isFavorite: false,
    testId: 'test-product-image',
    productImageStyle: { image: { width: '56px', height: '56px' } },
    onFavoriteChange: (isFavorite: boolean) => {
      console.log(`isFavorite: ${isFavorite}`);
    }
  };
  const handleCheck = (e: any, index: number) => {
    const data = guidedRows;
    data[index].cellData[''] = e.currentTarget.checked;
    // let rowSpan:Row<any> ={
    //     cellData:{
    //         "": false,
    //         Code: data[index].cellData.value,
    //         "Product Description": res.response.items[index].productName,
    //     }
    // }
    // data.splice(index+1,0,rowSpan);
    setGuidedRows([...data]);
  };
  const handleChange = (e: any, index: number) => {
    const rows = guidedRows;
    rows[index].cellData.Qty = e.target.value;
    setGuidedRows([...rows]);
  };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const tableprops: TableProps = {
    columns: [
      { header: '', type: 'checkbox' },
      { header: 'Code', type: 'code' },
      { header: 'Product Description', type: 'desc' },
      { header: 'Price' },
      { header: 'Unit' },
      { header: 'Qty', type: 'text' }
    ],
    rows: [...guidedRows],
    rowStyle: [
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '5%'
      },
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '10%'
      },
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '60%'
      },
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '5%'
      },
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '5%'
      },
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '10%'
      }
    ],
    columnStyles: [
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '5%'
      },
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '10%'
      },
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '60%'
      },
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '5%'
      },
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '5%'
      },
      {
        border: 'none',
        padding: '1rem',
        paddingTop: 0,
        width: '10%'
      }
    ],
    meta: {
      cell: ({ value, style, type, index }: any): any => {
        if (type === 'code') {
          imageProps.src = `https://dev-static.becn.digital/insecure/plain${value.itemImage}`;
        }
        return (
          <TableCell
            style={{
              ...style,
              colSpan:
                !guidedRows[index].cellData[''] && type === 'code' ? 0 : 2
            }}>
            {type === 'checkbox' ? (
              <input
                type="checkbox"
                onClick={(e) => {
                  handleCheck(e, index);
                }}
              />
            ) : !guidedRows[index].cellData[''] && type === 'code' ? (
              <a style={{ textDecoration: 'none', color: '#025F9E' }} href="#">
                {value.itemNumber}
              </a>
            ) : !guidedRows[index].cellData[''] && type === 'desc' ? (
              <label>{value.productName}</label>
            ) : type === 'code' && guidedRows[index].cellData[''] ? (
              <div>
                <a
                  style={{ textDecoration: 'none', color: '#025F9E' }}
                  href="#">
                  {value.itemNumber}
                </a>
                <div style={{ marginTop: '1rem' }}>
                  <img
                    width="56px"
                    alt="guided-order-plain"
                    height="56px"
                    src={`https://dev-static.becn.digital/insecure/plain${value.itemImage}`}
                  />
                </div>
              </div>
            ) : guidedRows[index].cellData[''] && type === 'desc' ? (
              <div>
                <label>{value.productName}</label>
                <div
                  style={{
                    marginTop: '1rem',
                    display: 'grid',
                    gridTemplateColumns: '1fr 1fr',
                    gridTemplateRows: 'auto'
                  }}>
                  <label>{value.currentSKU.variations.color[0]}</label>
                  <label>{value.currentSKU.variations.thickness[0]}</label>
                </div>
              </div>
            ) : type === 'text' ? (
              <TextField
                size="small"
                variant="outlined"
                value={guidedRows[index].cellData.Qty}
                onChange={(e) => {
                  handleChange(e, index);
                }}
              />
            ) : (
              value
            )}
          </TableCell>
        );
      }
    }
  };
  useEffect(() => {
    console.log('table props changed', tableprops.meta);
  }, [tableprops, guidedRows]);

  return (
    <div className="guided-order-table" data-testid="guided-order">
      <BCNTable {...tableprops} />
    </div>
  );
};

export default GuidedOrderTable;
