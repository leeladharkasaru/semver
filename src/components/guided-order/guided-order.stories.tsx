import { ComponentStoryObj, Meta } from '@storybook/react';
import React from 'react';
import GuidedOrderTable from './guided-order';

const meta: Meta<typeof GuidedOrderTable> = {
  component: GuidedOrderTable,
  title: 'guided-order/guided-order-grid',
  argTypes: {},
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof GuidedOrderTable>;

export const GuidedOrderGrid: ComponentStory = (args) => <GuidedOrderTable {...args} />;

GuidedOrderGrid.args = {

};
