import { ComponentStoryObj, Meta } from '@storybook/react';
import React from 'react';
import UsageIndicator from './UsageIndicator';

const meta: Meta<typeof UsageIndicator> = {
  component: UsageIndicator,
  title: 'Components/UsageIndicator',
  argTypes: {},
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof UsageIndicator>;

export const Default: ComponentStory = (args: any) => <UsageIndicator {...args} />;
Default.args = {
  usedSegment: {
    label: 'Ordered',
    color: '#AECAEA',
    borderColor: '#93AECC',
  },
  additionalSegment: {
    label: 'Added to Order',
    color: '#A7F3D0',
    borderColor: '#6EE7B7',
  },
  remainingSegment: {
    label: 'Remaining',
    color: '#FDD28C',
    borderColor: '#FCBF5F',
  },
  usedQuantity: 200,
  additionalQuantity: 200,
  totalQuantity: 1000,
  testId: 'test-usage-indicator',
};

export const AllOrdered: ComponentStory = (args: any) => <UsageIndicator {...args} />;
AllOrdered.args = {
  usedSegment: {
    label: 'Ordered',
    color: '#AECAEA',
    borderColor: '#93AECC',
  },
  additionalSegment: {
    label: 'Added to Order',
    color: '#A7F3D0',
    borderColor: '#6EE7B7',
  },
  remainingSegment: {
    label: 'Remaining',
    color: '#FDD28C',
    borderColor: '#FCBF5F',
  },
  usedQuantity: 1000,
  additionalQuantity: 0,
  totalQuantity: 1000,
  testId: 'test-usage-indicator',
};

export const AllAdded: ComponentStory = (args: any) => <UsageIndicator {...args} />;
AllAdded.args = {
  usedSegment: {
    label: 'Ordered',
    color: '#AECAEA',
    borderColor: '#93AECC',
  },
  additionalSegment: {
    label: 'Added to Order',
    color: '#A7F3D0',
    borderColor: '#6EE7B7',
  },
  remainingSegment: {
    label: 'Remaining',
    color: '#FDD28C',
    borderColor: '#FCBF5F',
  },
  usedQuantity: 0,
  additionalQuantity: 1000,
  totalQuantity: 1000,
  testId: 'test-usage-indicator',
};

export const AllRemaining: ComponentStory = (args: any) => <UsageIndicator {...args} />;
AllRemaining.args = {
  usedSegment: {
    label: 'Ordered',
    color: '#AECAEA',
    borderColor: '#93AECC',
  },
  additionalSegment: {
    label: 'Added to Order',
    color: '#A7F3D0',
    borderColor: '#6EE7B7',
  },
  remainingSegment: {
    label: 'Remaining',
    color: '#FDD28C',
    borderColor: '#FCBF5F',
  },
  usedQuantity: 0,
  additionalQuantity: 0,
  totalQuantity: 1000,
  testId: 'test-usage-indicator',
};

export const PartialOrdered: ComponentStory = (args: any) => <UsageIndicator {...args} />;
PartialOrdered.args = {
  usedSegment: {
    label: 'Ordered',
    color: '#AECAEA',
    borderColor: '#93AECC',
  },
  additionalSegment: {
    label: 'Added to Order',
    color: '#A7F3D0',
    borderColor: '#6EE7B7',
  },
  remainingSegment: {
    label: 'Remaining',
    color: '#FDD28C',
    borderColor: '#FCBF5F',
  },
  usedQuantity: 400,
  additionalQuantity: 0,
  totalQuantity: 1000,
  testId: 'test-usage-indicator',
};

export const PartialAdded: ComponentStory = (args: any) => <UsageIndicator {...args} />;
PartialAdded.args = {
  usedSegment: {
    label: 'Ordered',
    color: '#AECAEA',
    borderColor: '#93AECC',
  },
  additionalSegment: {
    label: 'Added to Order',
    color: '#A7F3D0',
    borderColor: '#6EE7B7',
  },
  remainingSegment: {
    label: 'Remaining',
    color: '#FDD28C',
    borderColor: '#FCBF5F',
  },
  usedQuantity: 0,
  additionalQuantity: 400,
  totalQuantity: 1000,
  testId: 'test-usage-indicator',
};

export const MinOrdered: ComponentStory = (args: any) => <UsageIndicator {...args} />;
MinOrdered.args = {
  usedSegment: {
    label: 'Ordered',
    color: '#AECAEA',
    borderColor: '#93AECC',
  },
  additionalSegment: {
    label: 'Added to Order',
    color: '#A7F3D0',
    borderColor: '#6EE7B7',
  },
  remainingSegment: {
    label: 'Remaining',
    color: '#FDD28C',
    borderColor: '#FCBF5F',
  },
  usedQuantity: 1,
  additionalQuantity: 0,
  totalQuantity: 1000,
  testId: 'test-usage-indicator',
};

export const MinAdded: ComponentStory = (args: any) => <UsageIndicator {...args} />;
MinAdded.args = {
  usedSegment: {
    label: 'Ordered',
    color: '#AECAEA',
    borderColor: '#93AECC',
  },
  additionalSegment: {
    label: 'Added to Order',
    color: '#A7F3D0',
    borderColor: '#6EE7B7',
  },
  remainingSegment: {
    label: 'Remaining',
    color: '#FDD28C',
    borderColor: '#FCBF5F',
  },
  usedQuantity: 0,
  additionalQuantity: 1,
  totalQuantity: 1000,
  testId: 'test-usage-indicator',
};

export const MinRemaining: ComponentStory = (args: any) => <UsageIndicator {...args} />;
MinRemaining.args = {
  usedSegment: {
    label: 'Ordered',
    color: '#AECAEA',
    borderColor: '#93AECC',
  },
  additionalSegment: {
    label: 'Added to Order',
    color: '#A7F3D0',
    borderColor: '#6EE7B7',
  },
  remainingSegment: {
    label: 'Remaining',
    color: '#FDD28C',
    borderColor: '#FCBF5F',
  },
  usedQuantity: 500,
  additionalQuantity: 499,
  totalQuantity: 1000,
  testId: 'test-usage-indicator',
};

export const MinOrderedAndAdded: ComponentStory = (args: any) => <UsageIndicator {...args} />;
MinOrderedAndAdded.args = {
  usedSegment: {
    label: 'Ordered',
    color: '#AECAEA',
    borderColor: '#93AECC',
  },
  additionalSegment: {
    label: 'Added to Order',
    color: '#A7F3D0',
    borderColor: '#6EE7B7',
  },
  remainingSegment: {
    label: 'Remaining',
    color: '#FDD28C',
    borderColor: '#FCBF5F',
  },
  usedQuantity: 1,
  additionalQuantity: 1,
  totalQuantity: 1000,
  testId: 'test-usage-indicator',
};

export const OverrideStyles: ComponentStory = (args: any) => <UsageIndicator {...args} />;
OverrideStyles.args = {
  usedSegment: {
    label: 'Ordered',
    color: 'red',
    borderColor: 'red',
  },
  additionalSegment: {
    label: 'Added to Order',
    color: 'yellow',
    borderColor: 'yellow',
  },
  remainingSegment: {
    label: 'Remaining',
    color: 'purple',
    borderColor: 'purple',
  },
  usedQuantity: 333,
  additionalQuantity: 333,
  totalQuantity: 999,
  testId: 'test-usage-indicator',
  usageIndicatorStyle: {
    container: {
      color: 'red',
    },
    upperText: {
      color: 'red',
    },
    upperTextContainer: {
      color: 'red',
    },
    upperTextLabel: {
      color: 'red',
    },
    upperTextValue: {
      color: 'red',
    },
    slider: {
      color: 'red',
    },
    sliderSegment: {
      color: 'red',
    },
    lowerText: {
      color: 'red',
    },
    lowerTextContainer: {
      color: 'red',
    },
    lowerTextLabel: {
      color: 'red',
    },
    lowerTextValue: {
      color: 'red',
    },
  },
};
