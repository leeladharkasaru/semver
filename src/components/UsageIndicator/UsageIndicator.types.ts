export interface UsageIndicatorProps {
    className?: string;
    usedSegment?: UsageIndicatorSegment;
    additionalSegment?: UsageIndicatorSegment;
    remainingSegment?: UsageIndicatorSegment;
    totalQuantity: number;
    usedQuantity: number;
    additionalQuantity: number;
    testId?: string;
    usageIndicatorStyle?: UsageIndicatorStyle;
}

export type UsageIndicatorSegment = {
    label: string;
    color: string;
    borderColor: string;
}

export type UsageIndicatorStyle = {
    container?: Object;
    upperText?: Object;
    upperTextContainer?: Object;
    upperTextLabel?: Object;
    upperTextValue?: Object;
    slider?: Object;
    sliderSegment?: Object;
    lowerText?: Object;
    lowerTextContainer?: Object;
    lowerTextLabel?: Object;
    lowerTextValue?: Object;
}
