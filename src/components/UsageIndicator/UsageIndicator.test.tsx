import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import React from 'react';
import UsageIndicator from './UsageIndicator';
import { UsageIndicatorProps } from './UsageIndicator.types';

describe('UsageIndicator Component', () => {
  let props: UsageIndicatorProps;

  beforeEach(() => {
    props = {
      testId: 'test-usage-indicator',
      totalQuantity: 100,
      usedQuantity: 30,
      additionalQuantity: 20,
    };
  });

  const renderComponent = () => render(<UsageIndicator {...props} />);

  it('renders without crashing', () => {
    // Act
    const { container } = renderComponent();

    // Assert
    expect(container).toBeInTheDocument();
  });

  it('displays correct used quantity', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-usage-indicator-used-value')).toHaveTextContent(props.usedQuantity.toString());
  });

  it('displays correct additional quantity', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-usage-indicator-additional-value')).toHaveTextContent(props.additionalQuantity.toString());
  });

  it('displays correct remaining quantity', () => {
    // Arrange
    const remainingQuantity = props.totalQuantity - props.usedQuantity - props.additionalQuantity;

    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-usage-indicator-remaining-value')).toHaveTextContent(remainingQuantity.toString());
  });
});
