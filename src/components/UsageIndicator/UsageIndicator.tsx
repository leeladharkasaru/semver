/* eslint-disable react/no-children-prop */
import './UsageIndicator.scss';
import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { UsageIndicatorProps } from './UsageIndicator.types';

const internalClassName = 'becn-usage-indicator';

const UsageIndicator: React.FC<UsageIndicatorProps> = ({
  className = '',
  totalQuantity,
  usedQuantity,
  additionalQuantity,
  testId,
  usageIndicatorStyle,
  usedSegment = {
    label: 'Ordered',
    color: '#AECAEA',
    borderColor: '#93AECC',
  },
  additionalSegment = {
    label: 'Added to Order',
    color: '#A7F3D0',
    borderColor: '#6EE7B7',
  },
  remainingSegment = {
    label: 'Remaining',
    color: '#FDD28C',
    borderColor: '#FCBF5F',
  },
}) => {
  // Calculate remaining quantity
  const remainingQuantity = totalQuantity - usedQuantity - additionalQuantity;

  // Calculate width of each segment
  const usedWidth = (usedQuantity / totalQuantity) * 100;
  const additionalWidth = (additionalQuantity / totalQuantity) * 100;

  // Return the usage indicator
  return (
    <Box
      className={`${internalClassName} ${className}`}
      style={usageIndicatorStyle?.container}
    >
      <Box
        className={`${internalClassName}__upper-text`}
        style={usageIndicatorStyle?.upperText}
      >
        <Box
          className={`${internalClassName}__upper-text__container`}
          style={usageIndicatorStyle?.upperTextContainer}
        >
          <Typography
            variant="body1"
            component="div"
            children={usedSegment.label}
            className={`${internalClassName}__upper-text__label`}
            style={usageIndicatorStyle?.upperTextLabel}
          />
          <Typography
            variant="body1"
            component="div"
            children={usedQuantity}
            className={`${internalClassName}__upper-text__value ${internalClassName}__upper-text__used-value`}
            data-testid={testId ? `${testId}-used-value` : undefined}
            style={usageIndicatorStyle?.upperTextValue}
          />
        </Box>
        <Box
          className={`${internalClassName}__upper-text__container`}
          style={usageIndicatorStyle?.upperTextContainer}
        >
          <Typography
            variant="body1"
            component="div"
            children={remainingSegment.label}
            className={`${internalClassName}__upper-text__label`}
            style={usageIndicatorStyle?.upperTextLabel}
          />
          <Typography
            variant="body1"
            component="div"
            children={remainingQuantity}
            className={`${internalClassName}__upper-text__value ${internalClassName}__upper-text__remaining-value`}
            data-testid={testId ? `${testId}-remaining-value` : undefined}
            style={usageIndicatorStyle?.upperTextValue}
          />
        </Box>
      </Box>
      <Box
        className={`${internalClassName}__slider`}
        style={usageIndicatorStyle?.slider}
      >
        <Box
          className={`${internalClassName}__slider__segment`}
          style={{
            backgroundColor: remainingSegment.color,
            border: `1px solid ${remainingSegment.borderColor}`,
            clipPath: `polygon(${
              usedWidth + additionalWidth
            }% 0, 100% 0, 100% 100%, ${usedWidth + additionalWidth}% 100%)`,
            ...usageIndicatorStyle?.sliderSegment,
          }}
        />
        <Box
          className={`${internalClassName}__slider__segment`}
          style={{
            backgroundColor: additionalSegment.color,
            border: `1px solid ${additionalSegment.borderColor}`,
            clipPath: `polygon(${usedWidth}% 0, ${
              usedWidth + additionalWidth
            }% 0, ${usedWidth + additionalWidth}% 100%, ${usedWidth}% 100%)`,
            ...usageIndicatorStyle?.sliderSegment,
          }}
        />
        {usedQuantity > 0 && (
          <Box
            className={`${internalClassName}__slider__segment`}
            style={{
              backgroundColor: usedSegment.color,
              border: `1px solid ${usedSegment.borderColor}`,
              clipPath: `polygon(0 0, ${usedWidth}% 0, ${usedWidth}% 100%, 0% 100%)`,
              ...usageIndicatorStyle?.sliderSegment,
            }}
          />
        )}
      </Box>
      {additionalQuantity > 0 && (
        <Box
          className={`${internalClassName}__lower-text`}
          style={usageIndicatorStyle?.lowerText}
        >
          <Box
            className={`${internalClassName}__lower-text__container`}
            style={usageIndicatorStyle?.lowerTextContainer}
          >
            <Typography
              variant="body1"
              component="div"
              children={additionalSegment.label}
              className={`${internalClassName}__lower-text__label`}
              style={usageIndicatorStyle?.lowerTextLabel}
            />
            <Typography
              variant="body1"
              component="div"
              children={additionalQuantity}
              className={`${internalClassName}__lower-text__value ${internalClassName}__lower-text__additional-value`}
              data-testid={testId ? `${testId}-additional-value` : undefined}
              style={usageIndicatorStyle?.lowerTextValue}
            />
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default UsageIndicator;
