import React from 'react';
import { ComponentMeta } from '@storybook/react';
import { Grid, Link } from '@material-ui/core';
import ItemListCard from './ItemListCard';
import { ItemListCardProps } from './ItemListCard.types';

const meta: ComponentMeta<typeof ItemListCard> = {
  component: ItemListCard,
  title: 'Components/ItemListCard',
  argTypes: {},
};
export default meta;

const itemListCard = [
  {
    categoryName: 'Manufacturer',
    key: 'MANUFACTURER',
    imageURL: 'abc',
  },
  {
    categoryName: 'ISO',
    key: 'ISO',
  },
  {
    categoryName: 'Cover Board',
    key: 'COVER_BOARD',
    children: <Link>Go to that section +</Link>,
  },
  {
    categoryName: 'Vapor Barrier',
    key: 'VAPOR_BARRIER',
    productList: [
      {
        productName:
          'Carlisle SynTec SecurShield® RL™RapidLock Grade-II (20 psi) PolyisoInsulation',
        unitPrice: '$20.44',
        qty: '32124',
      },
      {
        productName: 'Test data SecurShield®n',
        unitPrice: '$80.44',
        qty: '99',
      },
    ],
    children: <></>,
  },
];

const handleChange = (val, index, key) => {
  console.log('handleChange---', val);
  console.log('handleChange---', index);
  console.log('key---', key);
};

const deleteProduct = (val, key) => {
  console.log('delete---', val);
  console.log('key---', key);
};

const mapItemList = () => itemListCard.map((obj, index) => {
  if (obj.categoryName.toLocaleLowerCase() !== 'manufacturer') {
    const itemListCardVal: ItemListCardProps = {
      itemCard: {
        cardHeading: obj.categoryName,
        productList: obj.productList || [],
      },
      children: obj.children,
      categoryKey: obj.key,
      handleChange,
      deleteProduct,
    };
    return <ItemListCard {...itemListCardVal} />;
  }
});

export const Default = () => (
  <Grid container spacing={2}>
    <Grid item xs={3}>
      {mapItemList()}
    </Grid>
  </Grid>
);
