export interface ItemListCardProps {
  itemCard?: ItemCard;
  handleChange?: Function;
  deleteProduct?: Function;
  categoryKey: string;
  children?: any;
}
export interface ItemCard {
  cardHeading?: string;
  productList: ItemProduct[];
  editButtonText?: string;
}
export interface ItemProduct {
  productName: string;
  qty: string;
  unitPrice: string;
}
