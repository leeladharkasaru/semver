import React from 'react';
import './ItemListCard.scss';
import { Box, Grid, Paper, TextField } from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import { ItemListCardProps } from './ItemListCard.types';

const ItemListCard: React.FC<ItemListCardProps> = ({
  itemCard,
  handleChange,
  categoryKey,
  deleteProduct,
  children,
}: ItemListCardProps) => (
  <Paper
    data-testid="item-list-card-container"
    className="item-list-card-container"
  >
    <Grid container className="item-list-card">
      <Grid container style={{ margin: 'auto' }}>
        <Grid
          item
          xs={12}
          style={{
            display: itemCard.productList.length > 0 ? 'contents' : 'initial',
          }}
        >
          <div
            data-testid={`item-list-category-${categoryKey}`}
            className="category-heading"
          >
            {itemCard.cardHeading}
          </div>
        </Grid>
      </Grid>
      {itemCard.productList.length
        ? itemCard.productList.map((itemCardObj, index) => (
            <>
              <Box
                className="item-product-box"
                data-testid="item-product-box"
                style={{
                  paddingTop: index !== 0 ? '8px' : 'none',
                  borderTop: index !== 0 ? '1px solid #BFC3C7' : 'none',
                  width: '100%',
                }}
              >
                <div className="item-product-grid" style={{ display: 'flex' }}>
                  <div
                    data-testid={`item-list-product-${index}`}
                    className="item-product"
                  >
                    {itemCardObj.productName}
                  </div>

                  <Delete
                    className="item-list-delete-product-icon"
                    data-testid={`item-list-delete-product-${index}`}
                    onClick={() => deleteProduct(index, categoryKey)}
                  />
                </div>
                <Grid container className="item-qty-grid">
                  <Grid item>
                    <TextField
                      className="item-quantity-field"
                      data-testid={`item-list-quantity-${index}`}
                      size="small"
                      margin="dense"
                      name="quantity"
                      fullWidth
                      variant="outlined"
                      required
                      InputProps={{
                        inputProps: { min: 1, max: 50 },
                      }}
                      value={itemCardObj.qty}
                      onChange={(e: any) =>
                        handleChange(e.target.value, index, categoryKey)
                      }
                    />
                  </Grid>
                  <Grid item className="unit-price-grid">
                    <span data-testid={`item-list-unitprice-${index}`}>
                      {itemCardObj.unitPrice}
                    </span>
                  </Grid>
                </Grid>

                {/* {children && children} */}
              </Box>
            </>
          ))
        : children}
    </Grid>
  </Paper>
);

export default ItemListCard;
