import React from 'react';
import { render } from '@testing-library/react';
import ItemListCard from './ItemListCard';
import { ItemListCardProps } from './ItemListCard.types';

describe('test item list card component', () => {
  // let props: AccordianCardProps = {
  const itemListCardVal: ItemListCardProps = {
    categoryKey: 'VAPOR_BARRIER',
    itemCard: {
      cardHeading: 'Vapor Barrier',
      productList: [
        {
          productName:
            'Carlisle SynTec SecurShield® RL™RapidLock Grade-II (20 psi) PolyisoInsulation',
          unitPrice: '$20.44',
          qty: '32',
        },
      ],
    },
    handleChange: () => null,
    deleteProduct: () => null,
  };

  it('it should render item list card', () => {
    const renderComponent = () => render(<ItemListCard {...itemListCardVal} />);
    const { getByText } = renderComponent();
    expect(getByText('$20.44')).toBeDefined();
  });

  it('it should render item list card heading only', () => {
    const itemListCardValModified: ItemListCardProps = {
      itemCard: {
        cardHeading: 'ISO',
        productList: [],
      },
      categoryKey: 'ISO',
      handleChange: () => null,
      deleteProduct: () => null,
    };

    const renderComponent = () => render(<ItemListCard {...itemListCardValModified} />);
    const { queryByText } = renderComponent();

    expect(queryByText('$20.44')).toBeNull();
  });
});
