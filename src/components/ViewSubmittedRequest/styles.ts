const ViewSubmittedStyles = {
  style: (isMobile?: boolean, isTab?: boolean) => ({
    position: 'absolute',
    padding: 4,
    top: '5%',
    left: isMobile ? '5%' : isTab ? '10%' : '20%',
    width: isMobile ? '90%' : isTab ? '80%' : '60%',
    height: '90%',
    overflowY: 'scroll',
    bgcolor: 'background.paper',
    borderRadius: 4,
    boxShadow: 24,
    p: 4
  }),
  printStyle: {
    border: '1px solid rgba(0,0,0,.12)',
    padding: '0 0.938rem',
    color: 'inherit',
    textTransform: 'none',
    ':hover': {
      background: 'lightgrey'
    },
    lineHeight: '34px'
  },
  closeStyle: {
    border: '1px solid rgba(0,0,0,.12)',
    padding: '0 0.938rem',
    color: 'inherit',
    textTransform: 'none',
    ':hover': {
      background: 'lightgrey'
    },
    lineHeight: '34px',
    float: 'right',
    marginTop: 2
  },
  quoteLabelStyle: {
    fontFamily: 'Proxima Nova,arial,sans-serif',
    fontSize: '0.875rem',
    fontWeight: 'bolder',
    width: '30%'
  },
  quoteContentStyle: {
    fontFamily: 'Proxima Nova,arial,sans-serif',
    fontSize: '0.875rem',
    fontWeight: 400,
    width: '70%'
  },
  titleStyle: {
    fontFamily: 'Proxima Nova,arial,sans-serif',
    fontSize: '20px',
    fontWeight: '700'
  }
};

export default ViewSubmittedStyles;
