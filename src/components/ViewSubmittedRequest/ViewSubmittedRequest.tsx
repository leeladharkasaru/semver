/* eslint-disable operator-linebreak */
/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable implicit-arrow-linebreak */

import React, { useRef } from 'react';
import ReactToPrint from 'react-to-print';

import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Typography,
} from '@material-ui/core';

import './ViewSubmittedRequest.css';
import { ViewSubmittedRequestProps } from './ViewSubmittedRequest.types';
import ViewSubmittedStyles from './styles';

import {
  ADDRESS_1,
  ADDRESS_2,
  CITY,
  CREATED,
  JOB_NAME,
  LAST_MODIFIED,
  PHONE_NUMBER,
  QUOTE,
  STATE,
  VIEW_SUBMMITED_LABELS,
  WORK_TYPE,
} from '../../common/constants';

const quotePrintArr = [
  QUOTE,
  PHONE_NUMBER,
  CREATED,
  LAST_MODIFIED,
  WORK_TYPE,
  JOB_NAME,
  ADDRESS_1,
  ADDRESS_2,
  CITY,
  STATE,
];
const ViewSubmittedRequestData: React.FC<ViewSubmittedRequestProps> = ({
  openPopup,
  handleClose,
  title,
  printLabel,
  quoteDetails,
  children,
  printRef,
  isMobile,
}) => {
  const { quoteLabelStyle, quoteContentStyle, titleStyle } =
    ViewSubmittedStyles;

  return (
    <Dialog
      // eslint-disable-next-line no-return-assign, no-param-reassign
      ref={(el: any) => (printRef = el)}
      open={openPopup}
      maxWidth="md"
      onClose={handleClose}
    >
      <DialogTitle className="title">
        <Grid container>
          <Grid item md={9} xs={9}>
            <Typography data-testid="quote-details-heading" style={titleStyle}>
              {title}
            </Typography>
          </Grid>
          <Grid item md={3} xs={3}>
            <ReactToPrint
              bodyClass="print"
              content={() => printRef}
              trigger={() => (
                <Button
                  data-testid="print-button"
                  className="printStyle"
                  id="printScreen"
                  style={{ float: 'right', marginRight: '16px' }}
                >
                  {printLabel}
                </Button>
              )}
            />
          </Grid>
        </Grid>
      </DialogTitle>
      <DialogContent style={{ height: isMobile ? '60vh' : 'none' }}>
        <Box>
          <Box marginBottom={3}>
            {Object.keys(quoteDetails) &&
              quotePrintArr.map(
                (label: string) =>
                  quoteDetails[label] && (
                    <Box className="quote-details" key={label}>
                      <Typography
                        data-testid={`${label}-label`}
                        style={quoteLabelStyle}
                      >
                        {label}:
                      </Typography>
                      <Typography
                        data-testid={`${label}-content`}
                        style={quoteContentStyle}
                      >
                        {quoteDetails[label]}
                      </Typography>
                    </Box>
                  )
              )}
          </Box>
          <hr />
          {children}
        </Box>
      </DialogContent>
      <DialogActions>
        <Grid container>
          <Grid item md={12} xs={12} className="close">
            <Button
              data-testid="print-close-button"
              className="printStyle"
              onClick={handleClose}
            >
              {VIEW_SUBMMITED_LABELS.CLOSE_LABEL}
            </Button>
          </Grid>
        </Grid>
      </DialogActions>
    </Dialog>
  );
};

const ViewSubmittedRequest: React.FC<ViewSubmittedRequestProps> = (props) => {
  const printRef = useRef(null);

  return <ViewSubmittedRequestData {...{ ...props, printRef }} />;
};
export default ViewSubmittedRequest;
