import ViewSubmittedRequest from './ViewSubmittedRequest';

export type {
  ViewSubmitQuoteDetails,
  ViewSubmittedRequestProps,
} from './ViewSubmittedRequest.types';

export { ViewSubmittedRequest };
