import React from "react";

export interface ViewSubmittedRequestProps {
  quoteDetails: ViewSubmitQuoteDetails;
  children: any;
  isMobile?: boolean;
  isTab?: boolean;
  printRef?: any;
  title: string;
  printLabel: string;
  modalStyle?: React.CSSProperties;
  openPopup: boolean;
  handleClose?: any;
  dialogStyle?: any;
}

export interface ViewSubmitQuoteDetails {
  [key: string]: string;
}

// keys of the IQuoteDetails
export type QuoteDetailsKeys = Array<keyof ViewSubmitQuoteDetails>;
