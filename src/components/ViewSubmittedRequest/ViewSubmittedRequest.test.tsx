import { render } from '@testing-library/react';
import React from 'react';
import ViewSubmittedRequest from './ViewSubmittedRequest';
import {
  ViewSubmitQuoteDetails,
  ViewSubmittedRequestProps,
} from './ViewSubmittedRequest.types';

describe('Test Component', () => {
  const quoteDetails: ViewSubmitQuoteDetails = {
    'Quote Name': '_AAXISBDD_408190',
    'Phone Number': '123-456-7890',
    Created: '12-04-2023 BDD User4 AAXIS',
    'Last Modified': '12-05-2023 Ankita.Bhandula-con@becn.com',
    'Work Type': 'Remodel',
    'Job Name': 'SHOP ACCOUNT',
    'Address 1': '801 University Boulevard',
    'Address 2': '801 University Boulevard',
    City: 'Tuscaloosa',
    State: 'Gujrat',
  };

  const props: ViewSubmittedRequestProps = {
    modalStyle: {},
    openPopup: true,
    title: 'Quote Details',
    printLabel: 'Print',
    quoteDetails,
    children: null,
  };

  const renderComponent = () => render(<ViewSubmittedRequest {...props} />);

  it('should render view submitted request.', () => {
    // Assign
    // Act
    const { getByText } = renderComponent();
    // Act
    expect(getByText('Quote Details')).toBeDefined();
  });
});
