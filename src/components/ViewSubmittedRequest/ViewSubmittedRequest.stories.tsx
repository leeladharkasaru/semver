import {
  Box,
  Button,
  Grid,
  Input,
  MenuItem,
  Select,
  TableCell,
  Typography,
} from '@material-ui/core';
import React, { useState } from 'react';
import BCNTable from '../Table/Table';
import ViewSubmittedRequest from './ViewSubmittedRequest';
import { ViewSubmitQuoteDetails } from './ViewSubmittedRequest.types';
import { standardQuoteItems } from '../../mocks/quoteItems';

export default {
  title: 'Components/ViewSubmittedRequest',
};

const quoteDetails: ViewSubmitQuoteDetails = {
  'Quote Name': '_AAXISBDD_408190',
  'Phone Number': '123-456-7890',
  Created: '12-04-2023 BDD User4 AAXIS',
  'Last Modified': '12-05-2023 Ankita.Bhandula-con@becn.com',
  'Work Type': 'Remodel',
  'Job Name': 'SHOP ACCOUNT',
  'Address 1': '801 University Boulevard',
  'Address 2': '801 University Boulevard',
  City: 'Tuscaloosa',
  State: 'Gujrat',
};

const SampleTable = {
  columns: [
    { header: 'Product', type: 'image' },
    { header: 'Details' },
    { header: 'Unit Price' },
    {
      header: 'Order Qty',
      columnType: 'select',
      options: ['Order Qty', 'Shipped Qty'],
    },
    { header: 'Sub Total' },
  ],
  meta: {
    cell: ({ value, style, type, options }: any) => {
      const [changeValue, setValue] = useState(value);
      const changehandler = (e: any) => {
        setValue(e.target.value);
      };
      return (
        <TableCell style={style}>
          {type === 'button' ? (
            <Button>{value}</Button>
          ) : type === 'text' ? (
            <Input value={changeValue} onChange={changehandler} />
          ) : type === 'image' ? (
            <img src={value} style={style} />
          ) : type === 'select' ? (
            <Select
              variant="outlined"
              style={{
                ...style,
                border: '1px solid #5F6369',
                '.MuiSelect-outlined': {
                  border: '2px solid red',
                },
                root: {
                  background: 'red',
                },
                '.MenuItem.selected': {
                  color: 'red',
                },
              }}
              value={changeValue}
              onChange={changehandler}
            >
              {options?.map((val, index) => (
                <MenuItem key={index} value={val}>
                  {val}
                </MenuItem>
              ))}
            </Select>
          ) : (
            value
          )}
        </TableCell>
      );
    },
  },
  rows: standardQuoteItems.map((item) => {
    const itemData = {
      Product: `https://beaconproplus.com${item.imageURL}`,
      Details: (
        <Box>
          <a
            style={{
              color: '#285D99',
              fontFamily: '',
              fontSize: '1rem',
              fontWeight: 400,
            }}
          >
            {item.displayName}
          </a>
          <Box display="flex">
            <Typography
              style={{
                fontFamily: 'Proxima Nova,arial,sans-serif',
                fontSize: '1rem',
                fontWeight: 400,
                color: '#000000',
              }}
            >
              Item#:&nbsp;
            </Typography>
            <Typography
              style={{
                fontFamily: 'Proxima Nova,arial,sans-serif',
                fontSize: '1rem',
                fontWeight: 400,
                color: '#000000',
              }}
            >
              {item.itemNumber}
            </Typography>
          </Box>
        </Box>
      ),
      'Unit Price': `$${item.unitPrice}/${item.unitOfMeasure}`,
      'Order Qty': item.quantity,
      'Sub Total': `$${item.formatItemTotalPrice}`,
    };
    const row = { cellData: itemData };
    return row;
  }),
  columnStyles: [
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      lineHeight: '1rem',
      fontWeight: 400,
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      lineHeight: '1rem',
      fontWeight: 400,
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      lineHeight: '1rem',
      fontWeight: 400,
      width: '15%',
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 400,
      width: '9.5rem',
      height: '3rem',
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 400,
      width: '15%',
    },
  ],
  rowStyle: [
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
  ],
};

const renderChildren = () => (
  <Grid item className="tab-container">
    <BCNTable {...SampleTable} />
  </Grid>
);

export const WithPopup = (args) => {
  const [open, setOpen] = useState(false);
  const props = {
    dialogStyle: { width: '100vw' },
    openPopup: open,
    title: 'Quote Details',
    printLabel: 'Print',
    isMobile: true,
    handleClose: () => setOpen(false),
    quoteDetails,
    children: renderChildren(),
  };
  return (
    <>
      <Button onClick={() => setOpen(true)}>View Submitted Request</Button>
      <ViewSubmittedRequest {...props} />
    </>
  );
};
