import Delete from '@material-ui/icons/Delete';
import { TableCell, TextField, makeStyles } from '@material-ui/core';
import { render } from '@testing-library/react';
import React, { useState } from 'react';
import CustomItem from './CustomItem';
import { CustomPanelProps } from './CustomItem.types';

const addtionalQuoteItems = [
  {
    unitPrice: null,
    itemNumber: '',
    itemType: 'E',
    mincronDisplayName: null,
    quantity: 34,
    color: null,
    productId: null,
    unitOfMeasure: 'EA',
    displayName: 'wer',
    itemTotalPrice: null,
    imageOnErrorUrl: '/images/default_not_found.jpg',
    currencySymbol: null,
    productNumber: null,
    formatItemTotalPrice: null,
    bodyComments: null,
    PDPUrl: null,
    deleteStatus: null,
    formatUnitPrice: null,
    imageURL: '/images/default_not_found.jpg',
    stickerImageURL: null,
    id: '220900001',
    itemDescription: 'wer',
    dimensions: null,
  },
  {
    unitPrice: null,
    itemNumber: '',
    itemType: 'E',
    mincronDisplayName: null,
    quantity: 9999,
    color: null,
    productId: null,
    unitOfMeasure: 'EA',
    displayName: 'item',
    itemTotalPrice: null,
    imageOnErrorUrl: '/images/default_not_found.jpg',
    currencySymbol: null,
    productNumber: null,
    formatItemTotalPrice: null,
    bodyComments: null,
    PDPUrl: null,
    deleteStatus: null,
    formatUnitPrice: null,
    imageURL: '/images/default_not_found.jpg',
    stickerImageURL: null,
    id: '220700004',
    itemDescription: 'item',
    dimensions: null,
  },
  {
    unitPrice: null,
    itemNumber: '',
    itemType: 'E',
    mincronDisplayName: null,
    quantity: 7777,
    color: null,
    productId: null,
    unitOfMeasure: 'EA',
    displayName: 'test 6',
    itemTotalPrice: null,
    imageOnErrorUrl: '/images/default_not_found.jpg',
    currencySymbol: null,
    productNumber: null,
    formatItemTotalPrice: null,
    bodyComments: null,
    PDPUrl: null,
    deleteStatus: null,
    formatUnitPrice: null,
    imageURL: '/images/default_not_found.jpg',
    stickerImageURL: null,
    id: '220800007',
    itemDescription: 'test 6',
    dimensions: null,
  },
  {
    unitPrice: null,
    itemNumber: '',
    itemType: 'E',
    mincronDisplayName: null,
    quantity: 5100,
    color: null,
    productId: null,
    unitOfMeasure: 'EA',
    displayName: 'item',
    itemTotalPrice: null,
    imageOnErrorUrl: '/images/default_not_found.jpg',
    currencySymbol: null,
    productNumber: null,
    formatItemTotalPrice: null,
    bodyComments: null,
    PDPUrl: null,
    deleteStatus: null,
    formatUnitPrice: null,
    imageURL: '/images/default_not_found.jpg',
    stickerImageURL: null,
    id: '220700003',
    itemDescription: 'item',
    dimensions: null,
  },
  {
    unitPrice: null,
    itemNumber: '',
    itemType: 'E',
    mincronDisplayName: null,
    quantity: 5600,
    color: null,
    productId: null,
    unitOfMeasure: 'EA',
    displayName: 'hgha',
    itemTotalPrice: null,
    imageOnErrorUrl: '/images/default_not_found.jpg',
    currencySymbol: null,
    productNumber: null,
    formatItemTotalPrice: null,
    bodyComments: null,
    PDPUrl: null,
    deleteStatus: null,
    formatUnitPrice: null,
    imageURL: '/images/default_not_found.jpg',
    stickerImageURL: null,
    id: '220700002',
    itemDescription: 'hgha',
    dimensions: null,
  },
  {
    unitPrice: null,
    itemNumber: '',
    itemType: 'E',
    mincronDisplayName: null,
    quantity: 1,
    color: null,
    productId: null,
    unitOfMeasure: 'EA',
    displayName: 'test 5',
    itemTotalPrice: null,
    imageOnErrorUrl: '/images/default_not_found.jpg',
    currencySymbol: null,
    productNumber: null,
    formatItemTotalPrice: null,
    bodyComments: null,
    PDPUrl: null,
    deleteStatus: null,
    formatUnitPrice: null,
    imageURL: '/images/default_not_found.jpg',
    stickerImageURL: null,
    id: '220800006',
    itemDescription: 'test 5',
    dimensions: null,
  },
  {
    unitPrice: null,
    itemNumber: '',
    itemType: 'E',
    mincronDisplayName: null,
    quantity: 5600,
    color: null,
    productId: null,
    unitOfMeasure: 'EA',
    displayName: 'test4',
    itemTotalPrice: null,
    imageOnErrorUrl: '/images/default_not_found.jpg',
    currencySymbol: null,
    productNumber: null,
    formatItemTotalPrice: null,
    bodyComments: null,
    PDPUrl: null,
    deleteStatus: null,
    formatUnitPrice: null,
    imageURL: '/images/default_not_found.jpg',
    stickerImageURL: null,
    id: '220800005',
    itemDescription: 'test4',
    dimensions: null,
  },
  {
    unitPrice: null,
    itemNumber: '',
    itemType: 'E',
    mincronDisplayName: null,
    quantity: 5600,
    color: null,
    productId: null,
    unitOfMeasure: 'EA',
    displayName: 'test3',
    itemTotalPrice: null,
    imageOnErrorUrl: '/images/default_not_found.jpg',
    currencySymbol: null,
    productNumber: null,
    formatItemTotalPrice: null,
    bodyComments: null,
    PDPUrl: null,
    deleteStatus: null,
    formatUnitPrice: null,
    imageURL: '/images/default_not_found.jpg',
    stickerImageURL: null,
    id: '220800004',
    itemDescription: 'test3',
    dimensions: null,
  },
  {
    unitPrice: null,
    itemNumber: '',
    itemType: 'E',
    mincronDisplayName: null,
    quantity: 1,
    color: null,
    productId: null,
    unitOfMeasure: 'EA',
    displayName: 'test two',
    itemTotalPrice: null,
    imageOnErrorUrl: '/images/default_not_found.jpg',
    currencySymbol: null,
    productNumber: null,
    formatItemTotalPrice: null,
    bodyComments: null,
    PDPUrl: null,
    deleteStatus: null,
    formatUnitPrice: null,
    imageURL: '/images/default_not_found.jpg',
    stickerImageURL: null,
    id: '220800003',
    itemDescription: 'test two',
    dimensions: null,
  },
  {
    unitPrice: null,
    itemNumber: '',
    itemType: 'E',
    mincronDisplayName: null,
    quantity: 10000,
    color: null,
    productId: null,
    unitOfMeasure: 'EA',
    displayName: 'test one',
    itemTotalPrice: null,
    imageOnErrorUrl: '/images/default_not_found.jpg',
    currencySymbol: null,
    productNumber: null,
    formatItemTotalPrice: null,
    bodyComments: null,
    PDPUrl: null,
    deleteStatus: null,
    formatUnitPrice: null,
    imageURL: '/images/default_not_found.jpg',
    stickerImageURL: null,
    id: '220800002',
    itemDescription: 'test one',
    dimensions: null,
  },
  {
    unitPrice: null,
    itemNumber: '',
    itemType: 'E',
    mincronDisplayName: null,
    quantity: 56,
    color: null,
    productId: null,
    unitOfMeasure: 'EA',
    displayName: 'Ankita Bhandula',
    itemTotalPrice: null,
    imageOnErrorUrl: '/images/default_not_found.jpg',
    currencySymbol: null,
    productNumber: null,
    formatItemTotalPrice: null,
    bodyComments: null,
    PDPUrl: null,
    deleteStatus: null,
    formatUnitPrice: null,
    imageURL: '/images/default_not_found.jpg',
    stickerImageURL: null,
    id: '220700001',
    itemDescription: 'Ankita Bhandula',
    dimensions: null,
  },
];

describe('Test Custom Item Component', () => {
  let props: CustomPanelProps;

  beforeEach(() => {
    props = {
      message: 'Your Custom Items:',
      tableProps: {
        meta: {
          cell: ({
            value, style, type, index,
          }: any) => {
            const [changeValue, setValue] = useState(value);
            const changehandler = (e: any, index: number) => {
              setValue(e.target.value);
            };
            return (
              <TableCell style={style}>
                {type === 'button' ? (
                  <Delete />
                ) : type === 'text' ? (
                  <TextField
                    margin="dense"
                    name="quantity"
                    type="number"
                    size="small"
                    InputProps={{ inputProps: { min: 1, max: 99999 } }}
                    fullWidth
                    variant="outlined"
                    required
                    value={changeValue}
                    onChange={(e) => changehandler(e, index)}
                  />
                ) : (
                  value
                )}
              </TableCell>
            );
          },
        },
        columns: [
          { header: 'Details' },
          { header: 'Qty', type: 'text' },
          { header: 'Actions', type: 'button' },
        ],
        columnStyles: [
          {
            width: '50%',
          },
          {
            width: '20%',
          },
          {
            width: '10%',
          },
        ],
        rows: addtionalQuoteItems.map((item) => {
          const data = {
            Details: item?.displayName,
            Qty: item.quantity,
            Action: '',
          };
          const row = { cellData: data };
          return row;
        }),
      },
    };
  });

  const renderComponent = () => render(<CustomItem {...props} />);

  it('should render beacon text correctly', () => {
    // Assign
    const { getByTestId } = renderComponent();
    // Act
    const component = getByTestId('customitem');
    // Assert
    expect(component).toBeDefined();
  });
});
