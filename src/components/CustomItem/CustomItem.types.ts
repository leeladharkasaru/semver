import { TableProps } from '../Table';

export interface CustomPanelProps {
  message: string;
  tableProps: TableProps;
}

export interface CustomItemProps {
  itemName: IItem;
  quantity: IQuantity;
}
interface IItem {
  value: string;
  valid: boolean;
}
interface IQuantity {
  value: number;
  valid: boolean;
}
