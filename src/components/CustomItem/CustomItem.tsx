import { Grid, Typography } from '@material-ui/core';
import React from 'react';
import { BCNTable } from '../Table';
import './CustomItem.scss';
import { CustomPanelProps } from './CustomItem.types';

const CustomItem: React.FC<CustomPanelProps> = ({ message, tableProps }) => (
  <Grid container data-testid="customitem">
    <Grid item sm={12} xs={12} md={12} lg={12}>
      <Typography data-testid="message-heading" className="message">
        {message}
      </Typography>
    </Grid>
    <Grid item sm={12} xs={12} md={12} lg={12}>
      <BCNTable {...tableProps} />
    </Grid>
  </Grid>
);

export default CustomItem;
