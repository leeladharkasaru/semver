import React, { useState } from 'react';

import {
  Box,
  Button,
  Input,
  MenuItem,
  Select,
  TableCell,
  Typography,
} from '@material-ui/core';
import { ComponentStoryObj, Meta } from '@storybook/react';
import BCNItemsList from '../BCNItemsList/BCNItemsList';
import Summary from '../Summary/Summary';
import BCNTable from '../Table/Table';
import TabList from './TabList';
import { TabItem } from './TabList.types';
import { standardQuoteItems } from '../../mocks/quoteItems';

const meta: Meta<typeof TabList> = {
  component: TabList,
  title: 'Components/TabList',
  argTypes: {},
};
export default meta;

const DesktopSummary = {
  summary: {
    'Sub Total': '$120.54',
    'Other Charges': '$5.66',
    Tax: '$6.46',
    Total: '$9.08',
  },
  label: 'Order Summary',
  summaryStyle: {
    summaryContainer: {
      marginLeft: '30.875rem',
      marginTop: '3.844rem',
    },
    headingStyle: {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      marginLeft: '1.5rem',
      fontSize: '1.25rem',
      fontWeight: 600,
      lineHeight: '1.5rem',
      letterSpacing: 0,
      textAlign: 'left',
      color: '#242424',
      marginBottom: '0.406rem',
    },
    infoIconStyle: {
      width: '17px',
      height: '17px',
      alignContent: 'center',
    },
    listWapper: {
      borderBottom: '1px solid #DDD',
      borderTop: '1px solid #DDD',
      width: '27.813rem',
    },
    listBoxStyle: {
      display: 'flex',
      borderBottom: '1px solid #DDD',
    },

    summaryListLabelStyle: {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '0.875rem',
      marginTop: '0.594rem',
      fontWeight: 700,
      lineHeight: '1.313rem',
      letterSpacing: 0,
      textAlign: 'left',
    },
    summaryListHeaderStyle: {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '0.875rem',
      marginLeft: '1.5rem',
      marginTop: '0.594rem',
      fontWeight: 700,
      width: '13.438rem',
      lineHeight: '1.313rem',
      letterSpacing: '0em',
      textAlign: 'left',
    },
  },
};
interface ItemProps {
  Product?: any;
  Details?: any;
  Unit?: any;
  Quantity?: any;
  Action?: any;
}

const SampleTable = {
  columns: [
    { header: 'Product', type: 'image' },
    { header: 'Details' },
    { header: 'Unit Price' },
    {
      header: 'Order Qty',
      columnType: 'select',
      options: ['Order Qty', 'Shipped Qty'],
    },
    { header: 'Sub Total' },
  ],
  meta: {
    cell: ({ value, style, type, options }: any) => {
      const [changeValue, setValue] = useState(value);
      const changehandler = (e: any) => {
        setValue(e.target.value);
      };
      return (
        <TableCell style={style}>
          {type === 'button' ? (
            <Button>{value}</Button>
          ) : type === 'text' ? (
            <Input value={changeValue} onChange={changehandler} />
          ) : type === 'image' ? (
            <img src={value} style={style} />
          ) : type === 'select' ? (
            <Select
              variant="outlined"
              style={{
                ...style,
                border: '1px solid #5F6369',
                '.MuiSelect-outlined': {
                  border: '2px solid red',
                },
                root: {
                  background: 'red',
                },
                '.MenuItem.selected': {
                  color: 'red',
                },
              }}
              value={changeValue}
              onChange={changehandler}
            >
              {options?.map((val, index) => (
                <MenuItem key={index} value={val}>
                  {val}
                </MenuItem>
              ))}
            </Select>
          ) : (
            value
          )}
        </TableCell>
      );
    },
  },
  rows: standardQuoteItems.map((item, index) => {
    const itemData = {
      Product: `https://beaconproplus.com${item.imageURL}`,
      Details: (
        <Box>
          <a
            style={{
              color: '#285D99',
              fontFamily: '',
              fontSize: '1rem',
              fontWeight: 400,
            }}
          >
            {item.displayName}
          </a>
          <Box display="flex">
            <Typography
              style={{
                fontFamily: 'Proxima Nova,arial,sans-serif',
                fontSize: '1rem',
                fontWeight: 400,
                color: '#000000',
              }}
            >
              Item#:&nbsp;
            </Typography>
            <Typography
              style={{
                fontFamily: 'Proxima Nova,arial,sans-serif',
                fontSize: '1rem',
                fontWeight: 400,
                color: '#000000',
              }}
            >
              {item.itemNumber}
            </Typography>
          </Box>
        </Box>
      ),
      'Unit Price': `$${item.unitPrice}/${item.unitOfMeasure}`,
      'Order Qty': item.quantity,
      'Sub Total': `$${item.formatItemTotalPrice}`,
    };
    const row = { cellData: itemData };
    return row;
  }),
  columnStyles: [
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      lineHeight: '1rem',
      fontWeight: 400,
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      lineHeight: '1rem',
      fontWeight: 400,
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      lineHeight: '1rem',
      fontWeight: 400,
      width: '15%',
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 400,
      width: '9.5rem',
      height: '3rem',
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 400,
      width: '15%',
    },
  ],
  rowStyle: [
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
  ],
};

const itemListProps = {
  children: (
    <>
      <BCNTable {...SampleTable} />
      <Summary {...DesktopSummary} />
    </>
  ),
};

const itemListComponent = <BCNItemsList {...itemListProps} />;

type ComponentStory = ComponentStoryObj<typeof TabList>;

const Panel = {
  Item: 'Item List',
  CustomItem: 'Custom Item List',
  Product: 'Product Documents',
  Warranty: 'Warranty Submission Information',
};

let value = 0;
const handleClick = (e: React.ChangeEvent<{}>, val: number) => {
  value = val;
  console.log('value ', value);
};

const tabStyle = {
  width: '20vw',
  height: '4rem',
  padding: '0.625rem',
  border: '1px solid #C7C9C8',
  textAlign: 'center',
  backgroundColor: 'white',
  color: 'black',
  textTransform: 'none',
  fontFamily: 'Proxima Nova,arial,sans-serif',
};
const selectedTabStyle = {
  ...tabStyle,
  backgroundColor: '#285D99',
  color: '#FAFAFA',
};

const tabsArr: TabItem[] = [
  {
    label: Panel.Item,
    children: itemListComponent,
  },
  {
    label: Panel.CustomItem,
    children: <h1>Panel.CustomItem</h1>,
  },
  {
    label: Panel.Product,
    children: <h1>Panel.Product</h1>,
  },
  {
    label: Panel.Warranty,
    children: <h1>Panel.warranty</h1>,
  },
];

const tabIndicatorStyle = {
  backgroundColor: '#1F4B8F',
};
const isMobileview = false;

export const Order: ComponentStory = (args) => <TabList {...args} />;

Order.args = {
  value: 0,
  tabArr: tabsArr,
  selectedTabStyle,
  tabStyle,
  tabIndicatorStyle,
  mobileView: isMobileview,
  handleClick,
};
