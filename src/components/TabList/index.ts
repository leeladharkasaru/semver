import TabList from './TabList';

export type { TabItem, TabListProps } from './TabList.types';
export { TabList };
