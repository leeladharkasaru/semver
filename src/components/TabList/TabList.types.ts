import React from 'react';

export interface TabItem {
  label: string;
  children: React.ReactNode;
}

export interface TabListProps {
  value: number;
  mobileView?: boolean;
  selectedTabStyle: Object;
  tabStyle: Object;
  tabIndicatorStyle: Object;
  tabArr: TabItem[];
  handleClick: (event: React.ChangeEvent<{}>, value: number) => void;
}
