import React from 'react';
import { render } from '@testing-library/react';
import TabList from './TabList';
import { TabItem, TabListProps } from './TabList.types';

const Panel = {
  Item: 'ITEM_LIST',
  CustomItem: 'CUSTOM_ITEM_LIST',
  Product: 'PRODUCT_DOCUMENT',
  Warranty: 'WARRANTY_INFO',
};

const tabsArr: TabItem[] = [
  {
    label: Panel.Item,
    children: <h1>Panel.Item</h1>,
  },
  {
    label: Panel.CustomItem,
    children: <h1>Panel.CustomItem</h1>,
  },
  {
    label: Panel.Product,
    children: <h1>Panel.Product</h1>,
  },
  {
    label: Panel.Warranty,
    children: <h1>Panel.warranty</h1>,
  },
];
describe('it should render TabList Component', () => {
  const props: TabListProps = {
    value: 0,
    tabArr: tabsArr,
    selectedTabStyle: {},
    tabStyle: {},
    tabIndicatorStyle: {},
    mobileView: false,
    handleClick: () => null,
  };

  const renderComponent = () => render(<TabList {...props} />);

  it('should display tab list', () => {
    // Assign
    const { getAllByRole } = renderComponent();

    // Assert
    expect(getAllByRole('tab').length).toEqual(4);
  });
});
