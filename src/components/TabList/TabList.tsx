/* eslint-disable react/no-array-index-key */
import { Tab, Tabs } from '@material-ui/core';
import React from 'react';
import './TabList.scss';
import { TabItem, TabListProps } from './TabList.types';

const BecnTabList: React.FC<TabListProps> = ({
  tabIndicatorStyle,
  tabStyle,
  selectedTabStyle,
  value,
  tabArr,
  handleClick,
}) => (
  <>
    <Tabs
      TabIndicatorProps={{
        style: tabIndicatorStyle || {},
      }}
      data-testid="items-tab-panel"
      value={value}
      onChange={handleClick}
      aria-label="items-tab-panel"
    >
      {tabArr.map((tabObj: TabItem, id: number) => (
        <Tab
          key={`tab-${id}`}
          value={id}
          style={id === value ? selectedTabStyle : tabStyle}
          label={tabObj.label}
        />
      ))}
    </Tabs>
    {tabArr.map((tabObj: TabItem, index: number) => (
      <div
        role="tabpanel"
        hidden={value !== index}
        data-testid={`items-tab-${index}`}
        key={`items-tab-content-${index}`}
        id={`items-tab-content-${index}`}
        aria-labelledby={`items-tab-content-${index}`}
      >
        {value === index && tabObj.children}
      </div>
    ))}
  </>
);

export default BecnTabList;
