import './Subheading.scss';
import React from 'react';
import { Typography } from '@material-ui/core';
import { SubheadingProps } from './Subheading.types';

const internalClassName = 'becn-subheading';

const Subheading: React.FC<SubheadingProps> = ({
  children,
  className = '',
  color = 'default',
  variant = 'div',
  component = 'div',
  testId,
  subheadingStyle,
}) => (
  <Typography
    variant="body1"
    className={`${internalClassName} ${internalClassName}--${variant} ${internalClassName}--${color} ${className}`}
    component={component}
    data-testid={testId}
    style={subheadingStyle?.container}
  >
    {children}
  </Typography>
);

export default Subheading;
