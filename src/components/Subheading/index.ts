import Subheading from './Subheading';

export type {
  SubheadingProps,
  SubheadingVariant,
  SubheadingComponent,
  SubheadingColor,
  SubheadingStyle,
} from './Subheading.types';
export { Subheading };
