export interface SubheadingProps {
    className?: string;
    variant?: SubheadingVariant;
    component?: SubheadingComponent;
    color?: SubheadingColor;
    testId?: string;
    subheadingStyle?: SubheadingStyle;
}

export type SubheadingVariant = 'default';

export type SubheadingComponent = 'div' | 'p' | 'span';

export type SubheadingColor = 'default' | 'brandOnWhite';

export type SubheadingStyle = {
    container?: Object;
}
