import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import Subheading from './Subheading';

const meta: Meta<typeof Subheading> = {
  component: Subheading,
  title: 'Components/Subheading',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Subheading>;

export const Default: ComponentStory = (args: any) => (
  <Subheading {...args} />
);
Default.args = {
  children: 'Subheading',
  testId: 'test-subheading',
};

export const BrandOnWhite: ComponentStory = (args: any) => (
  <Subheading {...args} />
);
BrandOnWhite.args = {
  children: 'Subheading',
  color: 'brandOnWhite',
  testId: 'test-subheading',
};

export const OverrideStyle: ComponentStory = (args: any) => (
  <Subheading {...args} />
);
OverrideStyle.args = {
  children: 'Subheading',
  component: 'p',
  testId: 'test-subheading',
  subheadingStyle: {
    container: {
      color: 'red',
    },
  },
};
