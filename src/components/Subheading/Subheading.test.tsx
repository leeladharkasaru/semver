import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import React from 'react';
import Subheading from './Subheading';
import { SubheadingProps } from './Subheading.types';

describe('Subheading Component', () => {
  let props: SubheadingProps;

  beforeEach(() => {
    props = {
      variant: 'default',
      component: 'div',
      color: 'default',
      testId: 'test-subheading',
      subheadingStyle: {
        container: {
          color: 'red',
        },
      },
    };
  });

  const renderComponent = () => render(<Subheading {...props}>Test Subheading</Subheading>);

  it('should render the Subheading text', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-subheading')).toBeInTheDocument();
  });

  it('should render with the default component', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-subheading').tagName).toBe('DIV');
  });

  it('should render with the p component', () => {
    // Arrange
    props.component = 'p';

    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-subheading').tagName).toBe('P');
  });

  it('should render with the span component', () => {
    // Arrange
    props.component = 'span';

    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-subheading').tagName).toBe('SPAN');
  });

  it('should render with the correct color class', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Arrange
    expect(getByTestId('test-subheading')).toHaveClass('becn-subheading--default');
  });

  it('should apply the SubheadingStyle prop to the container', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-subheading')).toHaveStyle({ color: 'red' });
  });
});
