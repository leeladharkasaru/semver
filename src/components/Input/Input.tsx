import './Input.scss';
import React, { FC } from 'react';
import { FormHelperText, InputLabel, TextField } from '@material-ui/core';
import { InputProps } from './Input.types';

const internalClassName = 'becn-input';

const Input: FC<InputProps> = ({
  id,
  testId,
  disabled,
  label,
  message,
  error,
  value,
  defaultValue,
  placeholder,
  variant = 'standard',
  ref,
  startAdornment,
  endAdornment,
  onChange,
  onEnter,
  onBlur,
}) => {
  const isVariantOutlined = variant === 'outlined';

  // eslint-disable-next-line max-len
  const handleKeyDown: React.KeyboardEventHandler<
    HTMLTextAreaElement | HTMLInputElement
  > = (event) => {
    // Halt if no onEnter callback is provided
    if (!onEnter) return;

    // Trigger onEnter callback if the user presses the Enter key
    const key = event.key.toUpperCase();
    if (key === 'ENTER') {
      onEnter(event.currentTarget.value);
    }
  };

  return (
    <>
      {!isVariantOutlined && (
        <InputLabel className={`${internalClassName}__label`}>
          {label}
        </InputLabel>
      )}
      <TextField
        id={id}
        data-testid={testId}
        type="text"
        disabled={disabled}
        error={error}
        placeholder={placeholder}
        value={value}
        defaultValue={defaultValue}
        ref={ref}
        variant={variant}
        label={isVariantOutlined ? label : undefined}
        className={`${internalClassName}__text-field`}
        onChange={onChange}
        onBlur={onBlur}
        InputProps={{
          startAdornment,
          endAdornment,
          onKeyDown: handleKeyDown,
        }}
      />
      {message && (
        <FormHelperText
          error={error}
          className={`${internalClassName}__helper-text`}
        >
          {message}
        </FormHelperText>
      )}
    </>
  );
};

export default Input;
