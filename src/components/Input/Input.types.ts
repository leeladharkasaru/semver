import { ChangeEventHandler, MutableRefObject, ReactPropTypes } from 'react';
import { InputProps as StandardInputProps } from '@material-ui/core';

export interface InputProps {
  id?: string;
  testId?: string;
  label?: string;
  error?: boolean;
  message?: string;
  success?: boolean;
  disabled?: boolean;
  value?: string;
  defaultValue?: string;
  placeholder?: string;
  variant?: InputVariant;
  ref?: React.Ref<HTMLInputElement> | MutableRefObject<HTMLInputElement | undefined>;
  startAdornment?: React.ReactNode;
  endAdornment?: React.ReactNode;
  onChange?: ChangeEventHandler<HTMLInputElement>;
  onEnter?: (value: string) => void;
  onBlur?: StandardInputProps['onBlur'];
}

export type InputVariant = "standard" | "outlined";
