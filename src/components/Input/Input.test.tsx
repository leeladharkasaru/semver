import "@testing-library/jest-dom";
import { fireEvent, render } from "@testing-library/react";
import React from "react";
import Input from "./Input";
import { InputProps } from "./Input.types";

describe("Input Component", () => {
    let props: InputProps;

    beforeEach(() => {
        props = {
            id: "test-input",
            testId: "test-input",
            label: "Test Label",
            message: "Test Message",
            error: false,
            disabled: false,
            value: "Test Value",
            onEnter: jest.fn(),
        };
    });

    const renderComponent = () => render(<Input {...props} />);

    it("renders without crashing", () => {
        // Act
        const { container } = renderComponent();

        // Assert
        expect(container).toBeInTheDocument();
    });

    it("displays correct label", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId(props.testId!)).toBeInTheDocument();
    });

    it("displays correct message", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId(props.testId!)).toBeInTheDocument();
    });

    it("handles disabled prop correctly", () => {
        // Arrange
        props.disabled = true;

        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId(props.testId!).querySelector('input')).toBeDisabled();
    });

    it("handles error prop correctly", () => {
        // Arrange
        props.error = true;

        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId(props.testId!).querySelector('.Mui-error')).toBeInTheDocument();
    });

    it("displays correct value", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId(props.testId!).querySelector('input')).toHaveValue(props.value);
    });

    it("handles onEnter prop correctly", () => {
        // Arrange
        const value = "Test Value";

        // Act
        const { getByTestId } = renderComponent();
        fireEvent.keyDown(getByTestId(props.testId!).querySelector('input')!, { key: "Enter", code: "13" });

        // Assert
        expect(props.onEnter).toHaveBeenCalledWith(value);
    });
});