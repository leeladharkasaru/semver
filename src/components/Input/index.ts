import Input from './Input';

export type { InputProps, InputVariant } from './Input.types';
export { Input };
