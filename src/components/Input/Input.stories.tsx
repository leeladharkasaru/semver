import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import { IconButton, InputAdornment } from '@material-ui/core';
import { Search } from '@material-ui/icons';
import Input from './Input';

const meta: Meta<typeof Input> = {
  component: Input,
  title: 'Components/InputField',
  argTypes: {},
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Input>;

export const Primary: ComponentStory = (args) => (
  <Input data-testid="InputField-id" {...args} />
);
Primary.args = {
  error: false,
  disabled: false,
  label: 'Primary',
  testId: 'test-input',
  onEnter: (value) => console.log(value),
  onChange: (e) => console.log(e.target.value),
  onBlur: (e) => console.log(e.target.value),
};

export const Success: ComponentStory = (args) => (
  <Input data-testid="InputField-id" {...args} />
);
Success.args = {
  error: false,
  success: true,
  disabled: false,
  label: 'Success',
  testId: 'test-input',
  onEnter: (value) => console.log(value),
  onChange: (e) => console.log(e.target.value),
  onBlur: (e) => console.log(e.target.value),
};

export const Error: ComponentStory = (args) => (
  <Input data-testid="InputField-id" {...args} />
);
Error.args = {
  error: true,
  disabled: false,
  message: 'Error',
  testId: 'test-input',
  onEnter: (value) => console.log(value),
  onChange: (e) => console.log(e.target.value),
  onBlur: (e) => console.log(e.target.value),
};

export const Disabled: ComponentStory = (args) => (
  <Input data-testid="InputField-id" {...args} />
);
Disabled.args = {
  disabled: true,
  label: 'Disabled',
  testId: 'test-input',
  onEnter: (value) => console.log(value),
  onChange: (e) => console.log(e.target.value),
  onBlur: (e) => console.log(e.target.value),
};

export const DefaultValue: ComponentStory = (args) => (
  <Input data-testid="InputField-id" {...args} />
);
DefaultValue.args = {
  error: false,
  disabled: false,
  label: 'Primary',
  defaultValue: 'Default Value',
  testId: 'test-input',
  onEnter: (value) => console.log(value),
  onChange: (e) => console.log(e.target.value),
  onBlur: (e) => console.log(e.target.value),
};

export const OutlinedAndLabel: ComponentStory = (args) => (
  <Input data-testid="InputField-id" {...args} />
);
OutlinedAndLabel.args = {
  error: false,
  disabled: false,
  label: 'Email address',
  testId: 'test-input',
  variant: 'outlined',
  onEnter: (value) => console.log(value),
  onChange: (e) => console.log(e.target.value),
  onBlur: (e) => console.log(e.target.value),
};

export const OutlinedAndPlaceholder: ComponentStory = (args) => (
  <Input data-testid="InputField-id" {...args} />
);
OutlinedAndPlaceholder.args = {
  error: false,
  disabled: false,
  placeholder: 'Enter email address',
  testId: 'test-input',
  variant: 'outlined',
  onEnter: (value) => console.log(value),
  onChange: (e) => console.log(e.target.value),
  onBlur: (e) => console.log(e.target.value),
};

export const OutlinedAndError: ComponentStory = (args) => (
  <Input data-testid="InputField-id" {...args} />
);
OutlinedAndError.args = {
  error: true,
  disabled: false,
  label: 'Email address',
  testId: 'test-input',
  variant: 'outlined',
  message: 'Error',
  onEnter: (value) => console.log(value),
  onChange: (e) => console.log(e.target.value),
  onBlur: (e) => console.log(e.target.value),
};

export const OutlinedAndDisabled: ComponentStory = (args) => (
  <Input data-testid="InputField-id" {...args} />
);
OutlinedAndDisabled.args = {
  error: false,
  disabled: true,
  label: 'Email address',
  testId: 'test-input',
  variant: 'outlined',
  onEnter: (value) => console.log(value),
  onChange: (e) => console.log(e.target.value),
  onBlur: (e) => console.log(e.target.value),
};

export const OutlinedAndStartAdornment: ComponentStory = (args) => (
  <Input data-testid="InputField-id" {...args} />
);
OutlinedAndStartAdornment.args = {
  error: false,
  disabled: false,
  label: 'Amount',
  testId: 'test-input',
  variant: 'outlined',
  startAdornment: <InputAdornment position="start">$</InputAdornment>,
  onEnter: (value) => console.log(value),
  onChange: (e) => console.log(e.target.value),
  onBlur: (e) => console.log(e.target.value),
};

export const OutlinedAndEndAdornment: ComponentStory = (args) => (
  <Input data-testid="InputField-id" {...args} />
);
OutlinedAndEndAdornment.args = {
  error: false,
  disabled: false,
  label: 'Search Query',
  testId: 'test-input',
  variant: 'outlined',
  endAdornment: (
    <InputAdornment position="end">
      <IconButton
        onClick={() => console.log('Search clicked')}
        style={{ padding: 0 }}
      >
        <Search />
      </IconButton>
    </InputAdornment>
  ),
  onEnter: (value) => console.log(value),
  onChange: (e) => console.log(e.target.value),
  onBlur: (e) => console.log(e.target.value),
};
