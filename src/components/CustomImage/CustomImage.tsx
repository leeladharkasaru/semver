/* eslint-disable no-unused-expressions */
/* eslint-disable no-unneeded-ternary */
import { Favorite, FavoriteBorder } from '@material-ui/icons';
import React from 'react';
import { Image, ImageProps } from '../Image';
import { CustomImageProps } from './CustomImage.types';

const CustomImage: React.FC<CustomImageProps> = ({
  src,
  styleClasses,
  favoritesProps,
  fallbackSrc,
  alt,
  showFavoriteButton = true,
  testId,
}) => {
  const props: ImageProps = {
    src,
    alt,
    className: styleClasses?.imageClass,
    fallbackSrc,
    testId,
  };
  const onFavoriteChange = favoritesProps?.onFavoriteChange;
  const handleFavoriteClick = () => {
    onFavoriteChange();
  };
  return (
    <div
      className={styleClasses?.imageContainer}
      data-testid={`custom-image-${testId ? testId : '0'}`}
    >
      {/* <img
        onError={() => errorHandler && errorHandler()}
        className={styleClasses?.imageClass}
        src={src}
      /> */}
      <Image {...props} />
      {showFavoriteButton && (
        <div
          className={styleClasses?.favContainer}
          data-testid={`fav-container-${testId ? testId : '0'}`}
        >
          {favoritesProps?.isFavorite ? (
            <Favorite
              className={styleClasses?.favIconFilled}
              onClick={handleFavoriteClick}
            />
          ) : (
            <FavoriteBorder
              className={styleClasses?.favIconOutline}
              onClick={handleFavoriteClick}
            />
          )}
        </div>
      )}
    </div>
  );
};

export default CustomImage;
