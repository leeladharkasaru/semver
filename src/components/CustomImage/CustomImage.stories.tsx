import { ComponentStoryObj, Meta } from "@storybook/react";
import React from "react";
import CustomImage from "./CustomImage";
import "./CustomImage.scss";
const meta: Meta<typeof CustomImage> = {
  component: CustomImage,
  title: "Components/CustomImage",
  argTypes: {},
};
export default meta;
let isFav = false;
type ComponentStory = ComponentStoryObj<typeof CustomImage>;
export const Custom_Image: ComponentStory = (args) => <CustomImage {...args} />;

Custom_Image.args = {
  src: "https://dev-static.becn.digital/insecure/plain/images/large/invalid_image.jpg",
  styleClasses: {
    imageContainer: "image-container",
    favContainer: "fab-icon-container",
    favIconFilled: "fab-icon-filled",
    favIconOutline: "fab-icon-outline",
    imageClass: "product-image",
  },
  showFavoriteButton: true,
  favoritesProps: {
    isFavorite: isFav,
    onFavoriteChange: () => {
      isFav = true;
    },
  },
  fallbackSrc:
    "https://dev-static.becn.digital/insecure/plain/images/large/C-635001_default_small.jpg",
};
