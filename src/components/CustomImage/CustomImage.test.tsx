import { render } from '@testing-library/react';
import React from 'react';
import CustomImage from './CustomImage';
import { CustomImageProps } from './CustomImage.types';

describe('Test Component', () => {
  let props: CustomImageProps = {
    src: '',
    alt: '',
  };

  beforeEach(() => {
    props = {
      src: '',
      alt: '',
      testId: '0',
    };
  });

  const renderComponent = () => render(<CustomImage {...props} />);

  it('should render beacon text correctly', () => {
    // Assign
    props.src = 'Test component';
    const { getByTestId } = renderComponent();
    // Act
    const component = getByTestId('custom-image-0');
    // Assert
    expect(component).toBeDefined();
  });
});
