import CustomImage from "./CustomImage";
export type {
    CustomImageProps,
    FavoritesProps,
    StyleClasses
} from "./CustomImage.types";
export { CustomImage };

