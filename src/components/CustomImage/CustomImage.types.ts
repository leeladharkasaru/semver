export interface CustomImageProps {
  src: string;
  styleClasses?: StyleClasses;
  favoritesProps?: FavoritesProps;
  alt: string;
  fallbackSrc?: string;
  testId?: string;
  showFavoriteButton?: boolean;
}
export interface FavoritesProps {
  isFavorite: boolean;
  onFavoriteChange?: () => void;
}

export interface StyleClasses {
  imageClass?: any;
  imageContainer?: any;
  favContainer?: any;
  favIconOutline?: any;
  favIconFilled?: any;
}
