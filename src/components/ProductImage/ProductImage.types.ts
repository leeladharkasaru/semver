export type ProductImageVariant = 'thumbnail';

export type ProductImageStyle = {
  container?: Object;
  image?: Object;
  favoriteContainer?: Object;
  favoriteButton?: Object;
  favoriteIcon?: Object;
};

export interface ProductImageProps {
  src: string;
  alt: string;
  fallbackSrc?: string;
  variant?: ProductImageVariant;
  isFavorite: boolean;
  className?: string;
  testId?: string;
  showFavoriteButton?: boolean;
  productImageStyle?: ProductImageStyle;
  onFavoriteChange?: (isFavorite: boolean) => void;
}
