import './ProductImage.scss';
import React from 'react';
import { Box, IconButton } from '@material-ui/core';
import { Favorite, FavoriteBorder } from '@material-ui/icons';
import { ProductImageProps } from './ProductImage.types';
import Image from '../Image/Image';

const internalClassName = 'becn-product-image';

const ProductImage: React.FC<ProductImageProps> = ({
  src,
  alt,
  fallbackSrc,
  variant = 'thumbnail',
  isFavorite,
  className = '',
  testId,
  showFavoriteButton = true,
  productImageStyle,
  onFavoriteChange,
}) => {
  // Handle the favorite button click and trigger the external event
  const handleFavoriteClick = () => {
    const newIsFavorite = !isFavorite;
    onFavoriteChange(newIsFavorite);
  };

  return (
    <Box
      className={`${internalClassName} ${internalClassName}--${variant} ${className}`}
      style={productImageStyle?.container}
    >
      <Image
        src={src}
        alt={alt}
        fallbackSrc={fallbackSrc}
        imageStyle={productImageStyle?.image}
        testId={testId ? `${testId}-image` : undefined}
      />
      {showFavoriteButton && (
        <Box
          className={`${internalClassName}--favorite-container`}
          style={productImageStyle?.favoriteContainer}
        >
          <IconButton
            className={`${internalClassName}--favorite-button`}
            data-testid={testId ? `${testId}-favorite-button` : undefined}
            style={productImageStyle?.favoriteButton}
            onClick={handleFavoriteClick}
          >
            {isFavorite ? (
              <Favorite
                className={`${internalClassName}--favorite-icon`}
                style={productImageStyle?.favoriteIcon}
              />
            ) : (
              <FavoriteBorder
                className={`${internalClassName}--favorite-icon`}
                style={productImageStyle?.favoriteIcon}
              />
            )}
          </IconButton>
        </Box>
      )}
    </Box>
  );
};

export default ProductImage;
