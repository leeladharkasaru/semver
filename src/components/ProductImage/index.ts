import ProductImage from './ProductImage';

export type { ProductImageProps, ProductImageStyle, ProductImageVariant } from './ProductImage.types';
export { ProductImage };
