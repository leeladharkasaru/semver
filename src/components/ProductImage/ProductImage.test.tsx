import '@testing-library/jest-dom';
import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ProductImage from './ProductImage';
import { ProductImageProps } from './ProductImage.types';

describe('ProductImage Component', () => {
  let props: ProductImageProps;

  beforeEach(() => {
    props = {
      src: 'test.jpg',
      alt: 'Test Image',
      variant: 'thumbnail',
      isFavorite: false,
      testId: 'test-product-image',
      productImageStyle: {
        container: {
          backgroundColor: 'red',
        },
      },
      onFavoriteChange: jest.fn(),
    };
  });

  const renderComponent = () => render(<ProductImage {...props} />);

  it('should render the image', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-product-image-image')).toBeInTheDocument();
  });

  it('should render with the correct variant', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-product-image-image').parentElement).toHaveClass('becn-product-image--thumbnail');
  });

  it('should apply the productImageStyle prop to the container', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-product-image-image').parentElement).toHaveStyle({ backgroundColor: 'red' });
  });

  it('should call onFavoriteChange when the favorite button is clicked', async () => {
    // Act
    const { getByTestId } = renderComponent();
    fireEvent.click(getByTestId('test-product-image-favorite-button'));

    // Assert
    expect(props.onFavoriteChange).toHaveBeenCalledWith(true);
  });

  it('should render the Favorite icon when isFavorite is true', () => {
    // Arrange
    props.isFavorite = true;

    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-product-image-favorite-button').querySelector('svg')).toHaveClass('becn-product-image--favorite-icon');
  });

  it('should render the FavoriteBorder icon when isFavorite is false', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-product-image-favorite-button').querySelector('svg')).toHaveClass('becn-product-image--favorite-icon');
  });

  it('should not render the favorite button when showFavoriteButton is false', () => {
    // Arrange
    props.showFavoriteButton = false;

    // Act
    const { queryByTestId } = renderComponent();

    // Assert
    expect(queryByTestId('test-product-image-favorite-button')).toBeNull();
  });
});
