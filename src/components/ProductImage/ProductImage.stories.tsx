import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import ProductImage from './ProductImage';

const meta: Meta<typeof ProductImage> = {
  component: ProductImage,
  title: 'Components/ProductImage',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof ProductImage>;

export const Default: ComponentStory = (args: any) => (
  <ProductImage {...args} />
);
Default.args = {
  src: 'https://dev-static.becn.digital/insecure/plain/images/large/C-635001_default_small.jpg',
  alt: 'Product Image',
  isFavorite: false,
  testId: 'test-product-image',
  onFavoriteChange: (isFavorite: boolean) => {
    console.log(`isFavorite: ${isFavorite}`);
  },
};

export const Favorite: ComponentStory = (args: any) => (
  <ProductImage {...args} />
);
Favorite.args = {
  src: 'https://dev-static.becn.digital/insecure/plain/images/large/C-635001_default_small.jpg',
  alt: 'Product Image',
  isFavorite: true,
  testId: 'test-product-image',
  onFavoriteChange: (isFavorite: boolean) => {
    console.log(`isFavorite: ${isFavorite}`);
  },
};

export const HideFavoriteButton: ComponentStory = (args: any) => (
  <ProductImage {...args} />
);
HideFavoriteButton.args = {
  src: 'https://dev-static.becn.digital/insecure/plain/images/large/C-635001_default_small.jpg',
  alt: 'Product Image',
  isFavorite: false,
  showFavoriteButton: false,
  testId: 'test-product-image',
  onFavoriteChange: (isFavorite: boolean) => {
    console.log(`isFavorite: ${isFavorite}`);
  },
};

export const ErrorWithFallback: ComponentStory = (args: any) => (
  <ProductImage {...args} />
);
ErrorWithFallback.args = {
  src: 'https://dev-static.becn.digital/insecure/plain/images/large/invalid_image.jpg',
  fallbackSrc: 'https://dev-static.becn.digital/insecure/plain/images/large/C-635001_default_small.jpg',
  alt: 'Product Image',
  isFavorite: false,
  testId: 'test-product-image',
  onFavoriteChange: (isFavorite: boolean) => {
    console.log(`isFavorite: ${isFavorite}`);
  },
};

export const ErrorWithoutFallback: ComponentStory = (args: any) => (
  <ProductImage {...args} />
);
ErrorWithoutFallback.args = {
  src: 'https://dev-static.becn.digital/insecure/plain/images/large/invalid_image.jpg',
  alt: 'Product Image',
  isFavorite: false,
  testId: 'test-product-image',
  onFavoriteChange: (isFavorite: boolean) => {
    console.log(`isFavorite: ${isFavorite}`);
  },
};

export const OverrideStyle: ComponentStory = (args: any) => (
  <ProductImage {...args} />
);
OverrideStyle.args = {
  src: 'https://dev-static.becn.digital/insecure/plain/images/large/C-635001_default_small.jpg',
  alt: 'Product Image',
  isFavorite: false,
  testId: 'test-product-image',
  onFavoriteChange: (isFavorite: boolean) => {
    console.log(`isFavorite: ${isFavorite}`);
  },
  productImageStyle: {
    container: {
      border: '1px solid red',
    },
    favoriteButton: {
      backgroundColor: 'blue',
    },
    favoriteContainer: {
      backgroundColor: 'green',
    },
    favoriteIcon: {
      color: 'yellow',
    },
    image: {
      border: '1px solid blue',
    },
  },
};
