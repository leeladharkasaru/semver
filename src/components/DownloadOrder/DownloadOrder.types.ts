export interface DownloadOrderProps {
  open: boolean;
  type: string;
  pricing?: boolean;
  handleChange: Function;
  anchorEl: any;
  handleClose: Function;
  handleClick: Function;
  exportDownload: Function;
  labels: DownloadLabels;
  includePricing?: boolean;
}

export interface DownloadLabels {
  downloadInlabel: string;
  downloadOutlabel: string;
}
