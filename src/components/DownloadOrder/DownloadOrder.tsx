import {
  Box,
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  Menu,
  MenuItem,
  Select,
} from '@material-ui/core';
import GetAppIcon from '@material-ui/icons/GetApp';
import SelectDropDownIcon from '@material-ui/icons/ExpandMore';
import React from 'react';
import {
  CSV,
  EXCEL,
  FILE_TYPE,
  INCLUDE_PRICE,
  PDF,
} from '../../common/constants';
import './DownloadOrder.scss';
import { DownloadOrderProps } from './DownloadOrder.types';

export const fileType = {
  width: '120px',
};

const DownloadOrder: React.FC<DownloadOrderProps> = ({
  open,
  type,
  pricing,
  handleChange,
  anchorEl,
  handleClose,
  handleClick,
  exportDownload,
  labels,
  includePricing,
}) => (
  <Box>
    <Button
      data-testid="btn-download"
      className="download-out"
      onClick={(e) => handleClick(e)}
    >
      {labels.downloadOutlabel}
      <GetAppIcon className="download-icon" />
    </Button>
    <Menu anchorEl={anchorEl} open={open} onClose={() => handleClose()}>
      <FormControl className="download-form">
        <label>
          {' '}
          {FILE_TYPE}
          <></>
        </label>
        <FormGroup>
          <Select
            defaultValue="excel"
            data-testid="menu-download"
            name="type"
            id="type"
            variant="standard"
            disableUnderline
            style={{ ...fileType }}
            className="MuiSelect-selectMenu"
            IconComponent={SelectDropDownIcon}
            onChange={(e) => handleChange(e)}
            value={type}
          >
            <MenuItem value="excel">{EXCEL}</MenuItem>
            <MenuItem value="csv">{CSV}</MenuItem>
            <MenuItem value="pdf">{PDF}</MenuItem>
          </Select>
        </FormGroup>
        {includePricing && (
          <FormGroup style={{ marginTop: 1 }}>
            <FormControlLabel
              control={
                <Checkbox
                  data-testid="checkbox-pricing"
                  name="pricing"
                  checked={pricing}
                  onChange={(e) => handleChange(e)}
                />
              }
              label={INCLUDE_PRICE}
            />
          </FormGroup>
        )}
        <FormGroup
          className={
            pricing ? 'download-form-with-price' : 'download-form-without-price'
          }
        >
          <Button
            onClick={() => {
              // server download
              exportDownload(type, pricing);
              handleClose();
              // Local download to run storybook
              // exportDownload();
            }}
            data-testid="btn-popup-download"
            className="download-in"
          >
            {labels.downloadInlabel}
          </Button>
        </FormGroup>
      </FormControl>
    </Menu>
  </Box>
);

export default DownloadOrder;
