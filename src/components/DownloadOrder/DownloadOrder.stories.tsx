import { ComponentStoryObj, Meta } from '@storybook/react';
import React from 'react';
import DownloadOrder from './DownloadOrder';

const meta: Meta<typeof DownloadOrder> = {
  component: DownloadOrder,
  title: 'Components/DownloadOrder',
  argTypes: {},
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof DownloadOrder>;

export const DownloadOrderDefault: ComponentStory = (args) => (
  <DownloadOrder {...args} />
);

DownloadOrderDefault.args = {
  open: false,
  type: '',
  pricing: true,
  handleChange: () => {},
  anchorEl: true,
  handleClose: () => {},
  handleClick: () => {},
  exportDownload: () => {},
  labels: {
    downloadInlabel: 'Download Quote',
    downloadOutlabel: 'Download As',
  },
};

export const DownloadOrderNoPrice: ComponentStory = (args) => (
  <DownloadOrder {...args} />
);

DownloadOrderNoPrice.args = {
  open: false,
  labels: {
    downloadInlabel: 'Download Quote',
    downloadOutlabel: 'Download As',
  },
  type: '',
  pricing: false,
  handleChange: () => {},
  anchorEl: true,
  handleClose: () => {},
  handleClick: () => {},
  exportDownload: () => {},
};

export const DownloadQuoteAsReceived: ComponentStory = (args) => (
  <DownloadOrder {...args} />
);

DownloadQuoteAsReceived.args = {
  open: false,
  type: 'csv',
  pricing: true,
  handleChange: () => {},
  anchorEl: true,
  handleClose: () => {},
  handleClick: () => {},
  labels: {
    downloadInlabel: 'Download Quote',
    downloadOutlabel: 'Download As',
  },
  exportDownload: () => {},
};
