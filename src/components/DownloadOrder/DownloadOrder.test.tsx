import { render } from '@testing-library/react';
import React from 'react';
import { INVOICE_DOWNLOAD_AS_BUTTON } from '../../common/constants';
import DownloadOrder from './DownloadOrder';
import { DownloadOrderProps } from './DownloadOrder.types';

describe('Download Order Component', () => {
  const props: DownloadOrderProps = {
    open: false,
    type: '',
    pricing: true,
    handleChange: () => {},
    anchorEl: true,
    handleClose: () => {},
    handleClick: () => {},
    exportDownload: () => {},
    labels: {
      downloadInlabel: 'Download',
      downloadOutlabel: 'Download As',
    },
  };

  const renderComponent = () => render(<DownloadOrder {...props} />);

  it('Renders the Download AS button for desktop', () => {
    const { getByText } = renderComponent();
    // Act
    expect(getByText(INVOICE_DOWNLOAD_AS_BUTTON));
  });

  const renderMobileComponent = () => render(<DownloadOrder {...props} />);
  it('Renders the Download AS button for mobile', () => {
    const { getByText } = renderMobileComponent();
    // Act
    expect(getByText(INVOICE_DOWNLOAD_AS_BUTTON));
  });

  const renderComponentHidePrice = () => render(<DownloadOrder {...props} />);

  it('Renders the Download AS button with Price Hided', () => {
    const { getByText } = renderComponentHidePrice();
    // Act
    expect(getByText(INVOICE_DOWNLOAD_AS_BUTTON));
  });
});
