import { render } from '@testing-library/react';
import React from 'react';
import Summary from './Summary';
import { SummaryProps } from './Summary.types';

describe('Test Component', () => {
  const props: SummaryProps = {
    summary: {
      'Sub Total': '120',
      'Other Charges': '5',
      Tax: 5,
      Total: 20,
    },
    label: 'ORDER SUMMARY',
    summaryStyle: {
      summaryContainer: {
        marginLeft: '50%',
        marginTop: '2.25rem',
      },
      headingStyle: {
        //   height: "3.5rem",
        fontFamily: 'Inter',
        fontSize: '1.25rem',
        fontWeight: 600,
        lineHeight: '1.5rem',
        letterSpacing: 0,
        textAlign: 'left',
        color: '#242424',
      },
      listWapper: {
        borderBottom: '1px solid #DDD',
        borderTop: '1px solid #DDD',
        width: '27.813rem',
      },
      listBoxStyle: {
        display: 'flex',
        borderBottom: '1px solid #DDD',
      },

      summaryListLabelStyle: {
        fontFamily: 'Proxima Nova',
        fontSize: '0.875rem',
        marginTop: '0.594rem',

        fontWeight: 700,
        lineHeight: '1.313rem',
        letterSpacing: 0,
        textAlign: 'left',
      },
      summaryListHeaderStyle: {
        fontFamily: 'Proxima Nova',
        fontSize: '0.875rem',
        marginTop: '0.594rem',
        fontWeight: 700,
        width: '13.438rem',
        lineHeight: '1.313rem',
        letterSpacing: '0em',
        textAlign: 'left',
      },
    },
  };

  const renderComponent = () => render(<Summary {...props} />);

  it('should render beacon text correctly', () => {
    // Assign
    const { getByTestId } = renderComponent();
    // Act
    const component = getByTestId('Summary');
    // Assert
    expect(component).toBeDefined();
  });
});
