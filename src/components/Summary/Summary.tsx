import InfoIcon from '@material-ui/icons/Info';
import { Box, Typography } from '@material-ui/core';
import React from 'react';
import { SummaryProps } from './Summary.types';

const Summary: React.FC<SummaryProps> = ({ label, summary, summaryStyle }) => (
  <Box style={summaryStyle?.summaryContainer} data-testid="Summary">
    <Typography style={summaryStyle?.headingStyle}>{label}</Typography>
    <Box style={summaryStyle?.listWapper}>
      {Object.keys(summary).map((sum: any, index: number) => (
        <Box key={index} style={summaryStyle?.listBoxStyle}>
          <Typography style={summaryStyle?.summaryListHeaderStyle}>
            {sum}
            {sum === 'Other Charges' && (
              <InfoIcon style={summaryStyle?.infoIconStyle} />
            )}
          </Typography>

          <Typography style={summaryStyle?.summaryListLabelStyle}>
            {summary[sum]}
          </Typography>
        </Box>
      ))}
    </Box>
  </Box>
);

export default Summary;
