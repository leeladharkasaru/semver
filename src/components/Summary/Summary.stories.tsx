import { ComponentMeta, ComponentStory } from '@storybook/react';
import React from 'react';
import { ORDER_SUMMARY, PRICE_CALCULATED_TEXT } from '../../common/constants';
import Summary from './Summary';

const meta: ComponentMeta<typeof Summary> = {
  component: Summary,
  title: 'Components/Summary',
  argTypes: {},
};
export default meta;

type Story = ComponentStory<typeof Summary>;

export const DesktopSummary: Story = (args) => <Summary {...args} />;

DesktopSummary.args = {
  summary: {
    'Other Charges': '$5.35',
    'Sub Total': '$120.00',
    Tax: '$5.19',
    Total: '$130.54',
  },
  label: ORDER_SUMMARY,
  summaryStyle: {
    summaryContainer: {
      marginLeft: '50%',
      marginTop: '2.25rem',
    },
    headingStyle: {
      //   height: "3.5rem",
      fontFamily: 'Inter',
      fontSize: '1.25rem',
      fontWeight: 600,
      lineHeight: '1.5rem',
      letterSpacing: 0,
      textAlign: 'left',
      color: '#242424',
    },
    listWapper: {
      borderBottom: '1px solid #DDD',
      borderTop: '1px solid #DDD',
      width: '27.813rem',
    },
    listBoxStyle: {
      display: 'flex',
      borderBottom: '1px solid #DDD',
    },

    summaryListLabelStyle: {
      fontFamily: 'Proxima Nova',
      fontSize: '0.875rem',
      marginTop: '0.594rem',

      fontWeight: 700,
      lineHeight: '1.313rem',
      letterSpacing: 0,
      textAlign: 'left',
    },
    summaryListHeaderStyle: {
      fontFamily: 'Proxima Nova',
      fontSize: '0.875rem',
      marginTop: '0.594rem',
      fontWeight: 700,
      width: '13.438rem',
      lineHeight: '1.313rem',
      letterSpacing: '0em',
      textAlign: 'left',
    },
  },
};

export const DesktopSummaryPendingOrder: Story = (args) => (
  <Summary {...args} />
);

DesktopSummaryPendingOrder.args = {
  summary: {
    'Other Charges': PRICE_CALCULATED_TEXT,
    Tax: '$3.90',
    'Sub Total': '$45.93',
    Total: '$49.83',
  },
  label: ORDER_SUMMARY,
  summaryStyle: {
    summaryContainer: {
      marginLeft: '50%',
      marginTop: '2.25rem',
    },
    headingStyle: {
      //   height: "3.5rem",
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1.25rem',
      fontWeight: 600,
      lineHeight: '1.5rem',
      letterSpacing: 0,
      textAlign: 'left',
      color: '#242424',
    },
    listWapper: {
      borderBottom: '1px solid #DDD',
      borderTop: '1px solid #DDD',
      width: '27.813rem',
    },
    listBoxStyle: {
      display: 'flex',
      borderBottom: '1px solid #DDD',
    },

    summaryListLabelStyle: {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '0.875rem',
      color: '#000000',
      marginTop: '0.594rem',
      fontWeight: 700,
      lineHeight: '1.313rem',
      letterSpacing: 0,
      textAlign: 'left',
    },
    summaryListHeaderStyle: {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '0.875rem',
      color: '#000000',
      fontWeight: 700,
      marginTop: '0.594rem',
      width: '13.438rem',
      lineHeight: '1.313rem',
      letterSpacing: '0em',
      textAlign: 'left',
    },
  },
};
