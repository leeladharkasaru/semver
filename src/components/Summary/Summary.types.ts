export type SummaryProps = {
  summary: any;
  label: string;
  summaryStyle?: SummaryStyle;
};

export type SummaryStyle = {
  summaryContainer?: Object;
  headingStyle?: Object;
  summaryListHeaderStyle?: Object;
  summaryListLabelStyle?: Object;
  listBoxStyle?: Object;
  listWapper?: Object;
  infoIconStyle?: Object;
};
