import { ComponentMeta, ComponentStory } from '@storybook/react';
import * as React from 'react';
import { default as ConfirmDialog } from './ConfirmDialog';

const meta: ComponentMeta<typeof ConfirmDialog> = {
  component: ConfirmDialog,
  title: 'Components/ConfirmDialog',
  argTypes: {},
};
export default meta;

type Story = ComponentStory<typeof ConfirmDialog>;
export const SimpleDialog: Story = (args) => <ConfirmDialog {...args} />;

SimpleDialog.args = {
  dialogOpen: false,
  dialogTitle: 'Change Manufacturer?',
  dialogContent:
    'Manufacturer product lines are ordered separately. Changing the manufacturer will restart your order. Do you wish to change the selected manufacturer?',
  showDividers: true,
  dialogStyles: {
    dialogStyle: {
      fontFamily: 'Proxima-nova, sans-serif',
      width: '420px',
    },
    dialogTitleStyle: {
      fontFamily: 'Proxima-nova, sans-serif',
      fontSize: '18px',
      fontWeight: '400',
      lineHeight: '24px',
    },
    dialogContentStyle: {
      fontFamily: 'Proxima-nova, sans-serif',
      fontSize: '16px',
      fontWeight: '400',
      lineHeight: '24px',
    },
    dialogActionStyle: {
      primary: {
        background: '#025F9E',
        color: '#FAFAFA',
        textTransform: 'none',
        width: '50%',
        borderRadius: '4px',
      },
      secondry: {
        textTransform: 'none',
        width: '50%',
        borderRadius: '4px',
        color: '#025F9E',
        background: '#FAFAFA',
        border: '1px solid',
      },
      dialogActionRoot: {
        padding: '26px',
        alignItems: 'center',
        marginLeft: '6%',
        marginRight: '6%',
        justifyContent: 'flex-start',
      },
    },
  },
  showCloseIcon: true,
  dialogActionLabel: {
    primaryLabel: 'Done',
    secondryLabel: 'Close',
  },
  primaryButtonActionHandler: () => {
    console.log('clicked ok button');
  },
};
