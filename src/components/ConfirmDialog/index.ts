import ConfirmDialog from './ConfirmDialog';

export type {
  ConfirmDialogProps,
  DialogActionLabel,
  DialogStyles,
  DialogActionStyles,
} from './ConfirmDialog.types';
export {
  ConfirmDialog,
};
