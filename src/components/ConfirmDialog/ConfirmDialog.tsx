import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import './ConfirmDialog.scss';
import { ConfirmDialogProps } from './ConfirmDialog.types';

const ConfirmDialog: React.FC<ConfirmDialogProps> = ({
  dialogOpen,
  backDropProps,
  paperProps,
  handleClose,
  showDividers,
  dialogContent,
  dialogTitle,
  dialogStyles,
  dialogActionLabel,
  primaryButtonActionHandler,
  showCloseIcon,
}) => {
  const handleClick = () => {
    primaryButtonActionHandler();
  };
  return (
    <Dialog
      open={dialogOpen}
      keepMounted
      onClose={handleClose}
      className="main-dialog-container"
      data-testid="confirmation-dialog"
      aria-labelledby="confirmation-dialog-slide-title"
      aria-describedby="confirmation-dialog-slide-description"
      style={dialogStyles?.dialogStyle}
      PaperProps={paperProps}
      BackdropProps={backDropProps}
    >
      {dialogTitle && (
        <DialogTitle
          id="alert-dialog-slide-title"
          className="confirmation-dialog-title"
          style={dialogStyles?.dialogTitleStyle}
        >
          <Box style={dialogStyles.titleContainer} className="header-box">
            {dialogTitle}
            {showCloseIcon && (
              <CloseIcon
                className="close-icon"
                data-testid="close-icon"
                style={dialogStyles?.closeIconStyle}
                onClick={handleClose}
              />
            )}
          </Box>
        </DialogTitle>
      )}
      {dialogContent && (
        <DialogContent
          className="confirmation-dialog-content"
          data-testid="confirmation-dialog-content"
          dividers={showDividers ?? false}
          style={dialogStyles?.dialogContentStyle}
        >
          {dialogContent}
        </DialogContent>
      )}
      <DialogActions style={dialogStyles?.dialogActionStyle?.dialogActionRoot}>
        <Button
          onClick={handleClose}
          data-testid="secondry-button"
          className="secondry-button"
          style={dialogStyles?.dialogActionStyle?.secondry}
        >
          {dialogActionLabel ? dialogActionLabel.secondryLabel : 'Cancel'}
        </Button>
        <Button
          onClick={handleClick}
          data-testid="primary-button"
          className="primary-button"
          style={dialogStyles?.dialogActionStyle?.primary}
        >
          {dialogActionLabel ? dialogActionLabel.primaryLabel : 'Ok'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDialog;
