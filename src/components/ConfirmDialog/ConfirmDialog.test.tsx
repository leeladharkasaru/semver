import { render } from '@testing-library/react';
import React from 'react';
import ConfirmDialog from './ConfirmDialog';

describe('Test Component', () => {
  const customisedProps = {
    dialogOpen: true,
    dialogContent: 'Do You Want To Delete this Item',
    dialogTitle: 'Delete Item',
    dialogStyles: {
      dialogStyle: {
        fontFamily: 'Proxima Nova',
      },
      dialogContentStyle: {
        fontFamily: 'Proxima Nova',
      },
      dialogActionStyle: {
        primary: {
          background: 'blue',
          color: 'white',
          textTransform: 'none',
        },
        secondry: {
          textTransform: 'none',
        },
      },
    },
    dialogActionLabel: {
      primaryLabel: 'Done',
      secondryLabel: 'Close',
    },
    primaryButtonActionHandler: () => {},
  };
  const renderComponent = () => render(<ConfirmDialog {...customisedProps} />);

  it('should render Confirm Dialog correctly', () => {
    const { getByTestId } = renderComponent();
    // Act
    const component = getByTestId('confirmation-dialog');
    // Assert
    expect(component).toBeDefined();
    const primaryButton = getByTestId('primary-button');
    primaryButton.click();
  });
});
