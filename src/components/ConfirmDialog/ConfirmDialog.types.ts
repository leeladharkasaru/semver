import { BackdropProps, PaperProps } from '@material-ui/core';

export type ConfirmDialogProps = {
  dialogOpen: boolean;
  dialogContent: any;
  showDividers?: boolean;
  dialogTitle?: string;
  dialogStyles?: DialogStyles;
  dialogActionLabel?: DialogActionLabel;
  primaryButtonActionHandler?: Function;
  handleClose?: any;
  showCloseIcon?: boolean;
  backDropProps?:BackdropProps;
  paperProps?:PaperProps;
};

export type DialogActionLabel = {
  primaryLabel?: string;
  secondryLabel?: string;
};

export type DialogStyles = {
  dialogStyle?: Object;
  dialogTitleStyle?: Object;
  dialogContentStyle?: Object;
  closeIconStyle?: Object;
  titleContainer?:Object;
  dialogActionStyle?: DialogActionStyles;

};

export type DialogActionStyles = {
  primary?: Object;
  secondry?: Object;
  dialogActionRoot?: Object;
};
