import {
  Backdrop, Box, Button, Fade, Modal,
} from '@material-ui/core';
import React from 'react';
import './Popup.scss';
import { PopupProps } from './Popup.types';

const Popup: React.FC<PopupProps> = ({
  open,
  children,
  closeButtonProps,
  setOpen,
  closeHandle,
}) => {
  const handleClose = () => {
    setOpen(false);
    closeHandle();
  };
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      data-testid="PopupBox"
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
      }}
    >
      <Fade in={open}>
        <Box className="popup">
          {children}
          {closeButtonProps && (
            <Button
              data-testid="close"
              onClick={handleClose}
              style={closeButtonProps && closeButtonProps.closeStyle}
            >
              {closeButtonProps ? closeButtonProps.label : 'Close'}
            </Button>
          )}
        </Box>
      </Fade>
    </Modal>
  );
};

export default Popup;
