export type PopupProps = {
  open: boolean;
  children?: any;
  closeButtonProps?: CloseButton;
  setOpen: Function;
  closeHandle: Function;
};

export type CloseButton = {
  label?: string;
  closeStyle?: Object;
};
