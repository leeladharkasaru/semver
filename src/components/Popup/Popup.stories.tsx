import { Button, Input, TableCell } from '@material-ui/core';
import { ComponentMeta, ComponentStory } from '@storybook/react';
import * as React from 'react';
import { BCNTable } from '../Table/index';
import { default as Popup } from './Popup';

const standardQuoteItems = [
  {
    unitPrice: 67.75,
    itemNumber: '407262',
    itemType: 'I',
    mincronDisplayName: '',
    quantity: 1,
    color: null,
    productId: 'C-349425',
    unitOfMeasure: 'BDL',
    displayName:
      'Atlas Roofing StormMaster&reg; Slate Shingles with Scotchgard&trade; Protector Weathered Slate',
    itemTotalPrice: 67.75,
    imageOnErrorUrl: '/images/large/brand/atlas_roofing_brand_thumb.jpg',
    currencySymbol: '$',
    productNumber: 'ATLSMSLSPWE',
    formatItemTotalPrice: '67.75',
    bodyComments: null,
    PDPUrl: '/productDetail/C-349425?skuId=407262&Color=Weathered+Slate',
    deleteStatus: null,
    formatUnitPrice: '67.75',
    imageURL: '/images/large/407262_default_thumb.jpg',
    stickerImageURL: null,
    id: null,
    itemDescription: 'ATL AR SMASTER SLATE WEATHERED',
    dimensions: null,
  },
  {
    unitPrice: 26.66,
    itemNumber: '428681',
    itemType: 'I',
    mincronDisplayName: '',
    quantity: 1,
    color: null,
    productId: 'C-428681',
    unitOfMeasure: 'BDL',
    displayName: 'IKO Cambridge&reg; Shingles Aged Redwood',
    itemTotalPrice: 26.66,
    imageOnErrorUrl: '/images/large/brand/iko_brand_thumb.jpg',
    currencySymbol: '$',
    productNumber: 'IKOCAMAR',
    formatItemTotalPrice: '26.66',
    bodyComments: null,
    PDPUrl: '/productDetail/C-428681?skuId=428681&Color=Aged+Redwood',
    deleteStatus: null,
    formatUnitPrice: '26.66',
    imageURL: '/images/large/428681_default_thumb.jpg',
    stickerImageURL: null,
    id: null,
    itemDescription: 'IKO+CAMBRIDGE AR AGED REDWOD',
    dimensions: null,
  },
];
const meta: ComponentMeta<typeof Popup> = {
  component: Popup,
  title: 'Components/Popup',
  argTypes: {},
};
export default meta;
type Story = ComponentStory<typeof Popup>;
export const DesktopView: Story = (args) => <Popup {...args} />;

const tableProps = {
  columns: [
    { header: 'Product', type: 'image' },
    { header: 'Details' },
    { header: 'Unit' },
    { header: 'Quantity', type: 'text' },
    { header: 'Action', type: 'button' },
  ],
  meta: {
    cell: ({ value, style, type }: any) => {
      const [changeValue, setValue] = React.useState(value);
      const changehandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value);
      };
      return (
        <TableCell style={style}>
          {type === 'button' ? (
            <Button>{value}</Button>
          ) : type === 'text' ? (
            <Input value={changeValue} onChange={changehandler} />
          ) : type === 'image' ? (
            <img src={value} style={style} />
          ) : (
            value
          )}
        </TableCell>
      );
    },
  },
  rows: standardQuoteItems.map((item, index) => {
    const itemData = {
      Product: `https://beaconproplus.com${item.imageURL}`,
      Details: item.displayName,
      Unit: item.unitOfMeasure,
      Quantity: item.quantity,
      Action: 'Delete',
    };
    const row = { cellData: itemData };
    return row;
  }),
  columnStyles: [
    {
      color: 'rgba(0,0,0,.54)',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      width: 'fit-content',
      fontSize: '1rem',
      fontWeight: 600,
    },
    {
      color: 'rgba(0,0,0,.54)',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 600,
    },
    {
      color: 'rgba(0,0,0,.54)',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 600,
    },
    {
      color: 'rgba(0,0,0,.54)',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 600,
    },
    {
      color: 'rgba(0,0,0,.54)',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 600,
    },
  ],
  rowStyle: [
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      // background: "red",
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
    },
  ],
};

const popupProps = {
  open: true,

  children: <BCNTable {...tableProps} />,
  closeButtonProps: {
    label: 'Del',
  },
  setOpen: () => {
    popupProps.open = true;
  },
  closeHandle: () => {
    popupProps.open = false;
  },
};

DesktopView.args = {
  ...popupProps,
};
