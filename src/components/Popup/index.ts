import Popup from './Popup';

export type { PopupProps, CloseButton } from './Popup.types';
export { Popup };
