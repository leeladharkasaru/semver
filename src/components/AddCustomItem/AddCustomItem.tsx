/* eslint-disable react/destructuring-assignment */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-param-reassign */

import {
  Box,
  Button,
  Grid,
  Popover,
  TextField,
  Typography,
  useMediaQuery,
  useTheme
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import React, { useState } from 'react';
import { CustomItemProps } from '../CustomItem/CustomItem.types';
import { Popup } from '../Popup';

import './AddCustomItem.scss';
import {
  AddCustomItemProps,
  CustomItemPopupClassNamesProps
} from './AddCustomItem.types';
import ADD_CUSTOM_ITEM from './Constants';

const initialCustomItem: CustomItemProps = {
  itemName: { value: '', valid: true },
  quantity: { value: 1, valid: true }
};

const AddCustomItemRow = (
  customItems: Array<CustomItemProps>,
  setCustomItems: Function,
  closeHandle: any,
  addToQuoteHandle: any,
  popupClassNames?: CustomItemPopupClassNamesProps
) => {
  const {
    ACTION,
    ADD_CUSTOM_ITEM_ROW,
    ADD_TO_QUOTE,
    CANCEL,
    HEADING,
    ITEM_NAME_VALIDATION,
    ITEM_NAME,
    ITEM_NAME_LABEL,
    MAX_ROW,
    MAX_ROW_VALIDATION,
    QUANTITY,
    QUANTITY_LABEL,
    QTY_VALIDATION,
    ITEM_NAME_VALID,
    QTY_VALID,
    WHOLE_NUMBER_VALIDATION
  } = ADD_CUSTOM_ITEM;
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.between(300, 600));
  const handleAddCustomItemRow = (e: any) => {
    if (customItems.length >= MAX_ROW) {
      setAnchorEl(e.currentTarget);
      setTimeout(() => {
        setAnchorEl(null);
      }, 2000);
    }

    if (customItems.length < 10) {
      setCustomItems([...customItems, initialCustomItem]);
    }
  };
  const checkInputValidation = (name: string, inputValue: any) => {
    if (
      name === ITEM_NAME_VALID &&
      typeof inputValue === 'string' &&
      !inputValue.match(/^\s*$/)
    ) {
      return true;
    }

    const valueCheck: boolean = Number.isInteger(parseFloat(inputValue));

    if (
      name === QTY_VALID &&
      !Number.isNaN(inputValue) &&
      valueCheck &&
      parseInt(inputValue, 10) >= 1 &&
      parseInt(inputValue, 10) <= 5000
    ) {
      return true;
    }
    return false;
  };
  const handleClickAddToQuote = async () => {
    const items = JSON.parse(JSON.stringify(customItems));
    let isInvalid: boolean = false;
    items.forEach((customItem: CustomItemProps) => {
      if (checkInputValidation(ITEM_NAME_VALID, customItem.itemName.value)) {
        customItem.itemName.valid = true;
      } else {
        customItem.itemName.valid = false;
        isInvalid = true;
      }
      if (checkInputValidation(QTY_VALID, customItem.quantity.value)) {
        customItem.quantity.valid = true;
      } else {
        customItem.quantity.valid = false;
        isInvalid = true;
      }
    });
    setCustomItems([...items]);

    if (!isInvalid) {
      if (addToQuoteHandle) addToQuoteHandle(items);
      closeHandle();
    }
  };
  const handleChangeCustomItem = (e: any, index: number) => {
    const items = JSON.parse(JSON.stringify(customItems));
    const {
      target: { value, name }
    } = e;
    const customItem = items[index];
    if (name === ITEM_NAME) {
      customItem.itemName.value = value;
      items[index] = customItem;
      setCustomItems([...items]);
    } else if (value >= 1 || value === '') {
      customItem.quantity.value = value ? parseInt(value, 10) : '';
      items[index] = customItem;
      setCustomItems([...items]);
    }
  };
  const handleClickDelete = (index: number) => {
    const items = customItems.filter((item, itemIndex) => itemIndex !== index);
    setCustomItems([...items]);
  };
  return (
    <>
      <Grid container className="title-section">
        <Grid item xs={9} sm={9} md={6} lg={6}>
          <Typography className="add-custom-item-heading">{HEADING}</Typography>
        </Grid>
        <Grid item xs={3} sm={3} md={6} lg={6}>
          <CloseIcon className="close-icon" onClick={closeHandle} />
        </Grid>
      </Grid>
      <Grid container className="content-section">
        <Grid item xs={12} sm={12} lg={12} md={12}>
          <Box className="add-row-container">
            {customItems.map((customItem, index) => (
              <Box key={index} className="add-row-layout">
                <Box>
                  <TextField
                    className="item-name-layout"
                    InputLabelProps={{ shrink: true }}
                    inputProps={{
                      'data-testid': `item-name-${index}`
                    }}
                    label={ITEM_NAME_LABEL}
                    margin="dense"
                    placeholder="Item Name"
                    name={ITEM_NAME}
                    variant="standard"
                    value={customItem?.itemName?.value}
                    type="text"
                    onChange={(e) => handleChangeCustomItem(e, index)}
                  />
                  <Typography
                    data-testid={`item-name-validation-${index}`}
                    className={
                      !customItem?.itemName?.valid
                        ? 'validation-feilds'
                        : 'hide'
                    }>
                    {ITEM_NAME_VALIDATION}
                  </Typography>
                </Box>
                <Box>
                  <Box display="flex">
                    <TextField
                      InputLabelProps={{ shrink: true }}
                      label={QUANTITY_LABEL}
                      // type='number'
                      inputProps={{
                        'data-testid': `quantity-${index}`
                        // step: 1,
                      }}
                      margin="dense"
                      placeholder="Quantity"
                      name={QUANTITY}
                      variant="standard"
                      className="quantity-layout"
                      value={customItem?.quantity?.value}
                      onChange={(e) => handleChangeCustomItem(e, index)}
                    />

                    <Box>
                      <Typography
                        data-testid="action-label"
                        className={
                          customItems.length > 1
                            ? index === 0 && !isMobile
                              ? 'action-text'
                              : 'hide'
                            : 'hide'
                        }>
                        {ACTION}
                      </Typography>
                      <Button
                        disableRipple
                        data-testid={`action-delete${index}`}
                        className={
                          customItems.length > 1
                            ? index === 0 && !isMobile
                              ? 'action-delete'
                              : 'action-delete-wa'
                            : 'hide'
                        }
                        onClick={() => handleClickDelete(index)}>
                        <DeleteIcon />
                      </Button>
                    </Box>
                  </Box>
                  <Typography
                    data-testid={`quantity-validation-${index}`}
                    className={
                      !customItem?.quantity?.valid
                        ? 'validation-feilds quantity'
                        : 'hide'
                    }>
                    {customItem.quantity &&
                    Number.isInteger(
                      parseFloat(customItem.quantity.value.toString())
                    )
                      ? QTY_VALIDATION
                      : WHOLE_NUMBER_VALIDATION}
                  </Typography>
                </Box>
              </Box>
            ))}

            <Box className="btn-tab-container">
              <Button
                disableRipple
                variant="outlined"
                className={popupClassNames?.btnAddRow || 'add-custom-item-btn'}
                onClick={(e) => handleAddCustomItemRow(e)}
                data-testid="action-btn-add-row">
                {ADD_CUSTOM_ITEM_ROW}
              </Button>
              <Popover
                data-testid="pop-over-container"
                elevation={4}
                open={Boolean(anchorEl)}
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: isMobile ? 'top' : 'center',
                  horizontal: 'left'
                }}
                transformOrigin={{
                  vertical: isMobile ? 'bottom' : 'center',
                  horizontal: isMobile ? 'center' : 'right'
                }}
                className="pop-over-container">
                <Typography className="popOverText" data-testid="pop-over-text">
                  {MAX_ROW_VALIDATION}
                </Typography>
              </Popover>
              <Box className="btn-duo-container">
                <Button
                  className={popupClassNames?.btnCancel || 'cancel-btn-default'}
                  onClick={closeHandle}
                  data-testid="cancel-btn-default">
                  <label>{CANCEL}</label>
                </Button>
                <Button
                  disableRipple
                  className={popupClassNames?.btnSave || 'add-to-quote-btn'}
                  variant="outlined"
                  data-testid="add-to-quote-btn"
                  onClick={handleClickAddToQuote}>
                  {ADD_TO_QUOTE}
                </Button>
              </Box>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </>
  );
};

const AddCustomItem: React.FC<AddCustomItemProps> = ({
  addBtnLabel,
  addToQuoteHandle,
  btnAddClassName,
  popupClassNames
}) => {
  const [customItems, setCustomItems] = useState<Array<CustomItemProps>>([
    initialCustomItem
  ]);
  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const closeHandle = () => {
    setCustomItems([initialCustomItem]);
    setOpen(false);
  };
  return (
    <Grid container>
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <Button
          className={btnAddClassName || 'add-button'}
          data-testid="AddCustomItem"
          onClick={handleOpen}>
          {addBtnLabel}
        </Button>
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={12} data-testid="add-items-popup">
        <Popup
          open={open}
          // eslint-disable-next-line react/no-children-prop
          children={AddCustomItemRow(
            customItems,
            setCustomItems,
            closeHandle,
            addToQuoteHandle,
            popupClassNames
          )}
          setOpen={setOpen}
          closeHandle={closeHandle}
        />
      </Grid>
    </Grid>
  );
};

export default AddCustomItem;
