import { fireEvent, render } from '@testing-library/react';
import React from 'react';

import AddCustomItem from './AddCustomItem';
import { AddCustomItemProps } from './AddCustomItem.types';

describe('Test Component', () => {
  let props: AddCustomItemProps;
  beforeEach(() => {
    props = {
      addBtnLabel: 'Add Custom Item',
    };
  });

  const renderComponent = () => render(<AddCustomItem {...props} />);
  it('should render beacon text correctly', async () => {
    // Assign
    const { getByTestId } = renderComponent();
    // Act
    const addCustomBtn = getByTestId('AddCustomItem');
    // Assert
    expect(addCustomBtn).toBeDefined();
    await addCustomBtn.click();
    const popup = getByTestId('add-items-popup');
    expect(popup).toBeDefined();
    const addRow = getByTestId('action-btn-add-row');
    await addRow.click();
    await addRow.click();
    await addRow.click();
    await addRow.click();
    await addRow.click();
    await addRow.click();
    await addRow.click();
    await addRow.click();
    await addRow.click();
    await addRow.click();
    const item1 = getByTestId('item-name-1');
    fireEvent.change(item1, {
      target: {
        value: 'item1',
      },
    });
    const deleteRow = await getByTestId('action-delete1');
    await deleteRow.click();
    const cancel = getByTestId('cancel-btn-default');
    await cancel.click();
    await addCustomBtn.click();
    const addQuote = getByTestId('add-to-quote-btn');
    await addQuote.click();
  });
});
