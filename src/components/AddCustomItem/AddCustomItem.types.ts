export interface AddCustomItemProps {
  addBtnLabel: string;
  addToQuoteHandle?: Function;
  btnAddClassName?: string;
  popupClassNames?: CustomItemPopupClassNamesProps;
}
export interface CustomItemPopupClassNamesProps {
  btnAddRow: string;
  btnCancel: string;
  btnSave: string;
}
