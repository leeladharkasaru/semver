const ADD_CUSTOM_ITEM = {
  HEADING: 'Add Custom Items',
  ITEM_NAME_VALIDATION: '*Please enter item name',
  ACTION: 'Action',
  QTY_VALIDATION: '*Max quantity 5000',
  ADD_CUSTOM_ITEM_ROW: 'Add Custom Item +',
  MAX_ROW_VALIDATION: 'Max 10 items at a time',
  CANCEL: 'Cancel',
  ADD_TO_QUOTE: 'Add To Quote',
  MAX_ROW: 10,
  ITEM_NAME_LABEL: 'Item Name*',
  ITEM_NAME: 'itemName',
  QUANTITY_LABEL: 'Quantity*',
  QUANTITY: 'quantity',
  ITEM_NAME_VALID: 'Item Name',
  QTY_VALID: 'Quantity',
  WHOLE_NUMBER_VALIDATION: '*Please enter a whole number',
};

export { ADD_CUSTOM_ITEM as default };
