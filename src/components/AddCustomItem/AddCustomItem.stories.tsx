import { ComponentStoryObj, Meta } from '@storybook/react';
import React from 'react';
import AddCustomItem from './AddCustomItem';

const meta: Meta<typeof AddCustomItem> = {
  component: AddCustomItem,
  title: 'Components/AddCustomItem',
  argTypes: {},
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof AddCustomItem>;
export const Default: ComponentStory = (args) => <AddCustomItem {...args} />;

Default.args = {
  addBtnLabel: 'Add Custom Item',
  addToQuoteHandle: (items) => {
    console.log('adding items to quote', items);
  },
};
