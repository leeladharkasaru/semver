import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import React from 'react';
import Heading from './Heading';
import { HeadingProps } from './Heading.types';

describe('Heading Component', () => {
  let props: HeadingProps;

  beforeEach(() => {
    props = {
      variant: 'h1',
      component: 'h1',
      color: 'default',
      testId: 'test-heading',
      headingStyle: {
        container: {
          color: 'red',
        },
      },
    };
  });

  const renderComponent = () => render(<Heading {...props}>Test Heading</Heading>);

  it('should render the heading text', () => {
    const { getByTestId } = renderComponent();
    expect(getByTestId('test-heading')).toBeInTheDocument();
  });

  it('should render with the default variant', () => {
    const { getByTestId } = renderComponent();
    expect(getByTestId('test-heading').tagName).toBe('H1');
  });

  it('should render with the h2 variant', () => {
    // Arrange
    props.variant = 'h2';
    props.component = undefined;

    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-heading').tagName).toBe('H2');
  });

  it('should render with the h3 variant', () => {
    // Arrange
    props.variant = 'h3';
    props.component = undefined;

    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-heading').tagName).toBe('H3');
  });

  it('should render with the correct color class', () => {
    const { getByTestId } = renderComponent();
    expect(getByTestId('test-heading')).toHaveClass('becn-heading--default');
  });

  it('should apply the headingStyle prop to the container', () => {
    const { getByTestId } = renderComponent();
    expect(getByTestId('test-heading')).toHaveStyle({ color: 'red' });
  });
});
