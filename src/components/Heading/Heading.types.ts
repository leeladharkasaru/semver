export interface HeadingProps {
    className?: string;
    variant?: HeadingVariant;
    component?: HeadingComponent;
    color?: HeadingColor;
    testId?: string;
    headingStyle?: HeadingStyle;
}

export type HeadingVariant = 'h1' | 'h2' | 'h3';

export type HeadingComponent = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';

export type HeadingColor = 'default' | 'brandOnWhite' | 'subtle';

export type HeadingStyle = {
    container?: Object;
}
