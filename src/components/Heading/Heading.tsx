import './Heading.scss';
import React from 'react';
import { Typography } from '@material-ui/core';
import { HeadingProps } from './Heading.types';

const internalClassName = 'becn-heading';

const Heading: React.FC<HeadingProps> = ({
  children,
  className = '',
  color = 'default',
  variant = 'h1',
  component = variant,
  testId,
  headingStyle,
}) => (
  <Typography
    className={`${internalClassName} ${internalClassName}--${variant} ${internalClassName}--${color} ${className}`}
    variant={variant}
    component={component}
    data-testid={testId}
    style={headingStyle?.container}
  >
    {children}
  </Typography>
);

export default Heading;
