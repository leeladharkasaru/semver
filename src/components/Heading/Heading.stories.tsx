import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import Heading from './Heading';

const meta: Meta<typeof Heading> = {
  component: Heading,
  title: 'Components/Heading',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Heading>;

export const Default: ComponentStory = (args: any) => (
  <Heading {...args} />
);
Default.args = {
  children: 'Heading',
};

export const BrandOnWhite: ComponentStory = (args: any) => (
  <Heading {...args} />
);
BrandOnWhite.args = {
  children: 'Heading',
  color: 'brandOnWhite',
};

export const H2: ComponentStory = (args: any) => (
  <Heading {...args} />
);
H2.args = {
  children: 'Heading',
  variant: 'h2',
};

export const H3: ComponentStory = (args: any) => (
  <Heading {...args} />
);
H3.args = {
  children: 'Heading',
  variant: 'h3',
};

export const OverrideStyle: ComponentStory = (args: any) => (
  <Heading {...args} />
);
OverrideStyle.args = {
  children: 'Heading',
  variant: 'h1',
  component: 'h2',
  headingStyle: {
    container: {
      color: 'red',
    },
  },
};
