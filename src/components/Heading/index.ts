import Heading from './Heading';

export type {
  HeadingProps,
  HeadingVariant,
  HeadingComponent,
  HeadingColor,
  HeadingStyle,
} from './Heading.types';
export { Heading };
