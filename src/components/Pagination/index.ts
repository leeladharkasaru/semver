import Pagination from './Pagination';

export type {
  PaginationProps,
  PaginationStyle,
} from './Pagination.types';
export { Pagination };
