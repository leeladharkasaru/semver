import './Pagination.scss';
import React from 'react';
import { Box, Grid, Link, Typography, useMediaQuery } from '@material-ui/core';
import {
  ChevronLeft,
  ChevronRight,
  FirstPage,
  LastPage
} from '@material-ui/icons';
import { PaginationProps } from './Pagination.types';
import DropDownList from '../DropDownList/DropDownList';
import { Text } from '../Text';
import Button2 from '../Button2/Button2';

const internalClassName = 'bcn-pagination';

function Pagination({
  numberOfItems,
  page = 1,
  itemsPerPage = 15,
  itemsPerPageOptions = [5, 10, 15, 20, 100],
  itemLabel = 'items',
  className = '',
  testId,
  paginationStyle,
  onPageChange
}: PaginationProps) {
  // Check if the screen is mobile
  const isMobile = useMediaQuery('(max-width:768px)');
  const isSmallMobile = useMediaQuery('(max-width:410px)');

  // Calculate the number of pages
  const numberOfPages = Math.ceil(numberOfItems / itemsPerPage);

  // Calculate the range of items being displayed
  const startItem = (page - 1) * itemsPerPage + 1;
  const endItem = Math.min(page * itemsPerPage, numberOfItems);

  // Calculate the page links
  const pageOffset = 9;
  const beginningPages = [];
  const middlePages = [];
  const endingPages = [];
  let beginningOffset = page - pageOffset;
  if (beginningOffset < 1) {
    beginningOffset = 1;
  }
  let endingOffset = page + pageOffset;
  if (endingOffset > numberOfPages) {
    endingOffset = numberOfPages;
  }
  // eslint-disable-next-line no-plusplus
  for (let i = 1; i <= numberOfPages; i++) {
    let middleOffset = 1;
    if (page === 1 || page === numberOfPages) {
      middleOffset = 2;
    }
    if (i === beginningOffset && page > 2) {
      beginningPages.push(i);
    } else if (i > page + 1 && i === endingOffset) {
      endingPages.push(i);
    } else if (i >= page - middleOffset && i <= page + middleOffset) {
      middlePages.push(i);
    }
  }

  // Handle the change of the page
  const handlePageChange = (newPage: number) => {
    // Halt if the page is out of range
    if (newPage < 1 || newPage > numberOfPages) {
      return;
    }

    // Halt if the page is the same as the current page
    if (newPage === page) {
      return;
    }

    // Trigger the callback
    onPageChange(newPage, itemsPerPage);
  };

  // Handle the change of the items per page
  const handleItemsPerPageChange = (value: number) => {
    // Trigger the callback
    const newItemsPerPage = value;
    const resetPage = 1;
    onPageChange(resetPage, newItemsPerPage);
  };

  // Render pagination info
  const Info = () => (
    <Box
      className={`${internalClassName}--info`}
      style={paginationStyle?.info}
      data-testid={testId ? `${testId}-info` : undefined}>
      <Typography
        className={`${internalClassName}--text`}
        data-testid={testId ? `${testId}-text` : undefined}>
        {startItem}-{endItem} of
        {numberOfItems} {itemLabel}
      </Typography>
    </Box>
  );

  // Render items per page dropdown
  const ItemsPerPage = () => (
    <Box
      className={`${internalClassName}--items-per-page`}
      style={paginationStyle?.itemsPerPage}>
      <DropDownList
        radius="sharp"
        defaultValue={itemsPerPage}
        items={itemsPerPageOptions.map((option) => ({
          label: option.toString(),
          value: option.toString()
        }))}
        testId={testId ? `${testId}-items-per-page` : undefined}
        onChange={(value) => handleItemsPerPageChange(parseInt(value, 10))}
        renderValue={(value) => (
          <>
            <Text
              variant="bold"
              component="span"
              className={`${internalClassName}--show-label`}>
              Show:
            </Text>
            <Text
              component="span"
              className={`${internalClassName}--show-value`}>
              {value}
            </Text>
          </>
        )}
      />
    </Box>
  );

  // Render previous buttons
  const PreviousButtons = () => (
    <Box
      className={`${internalClassName}--buttons ${internalClassName}--prev-buttons`}
      style={paginationStyle?.prevButtons}>
      <Button2
        size="small"
        className={`${internalClassName}--button ${internalClassName}--prev-button`}
        testId={testId ? `${testId}-first-page-button` : undefined}
        onClick={() => handlePageChange(1)}>
        <FirstPage />
      </Button2>
      <Button2
        size="small"
        className={`${internalClassName}--button ${internalClassName}--prev-button`}
        testId={testId ? `${testId}-prev-page-button` : undefined}
        onClick={() => handlePageChange(page - 1)}>
        <ChevronLeft />
      </Button2>
    </Box>
  );

  // Render next buttons
  const NextButtons = () => (
    <Box
      className={`${internalClassName}--buttons ${internalClassName}--next-buttons`}
      style={paginationStyle?.nextButtons}>
      <Button2
        size="small"
        className={`${internalClassName}--button ${internalClassName}--next-button`}
        testId={testId ? `${testId}-next-page-button` : undefined}
        onClick={() => handlePageChange(page + 1)}>
        <ChevronRight />
      </Button2>
      <Button2
        size="small"
        className={`${internalClassName}--button ${internalClassName}--next-button`}
        testId={testId ? `${testId}-last-page-button` : undefined}
        onClick={() => handlePageChange(numberOfPages)}>
        <LastPage />
      </Button2>
    </Box>
  );

  // Render page links
  const PageLinks = () => (
    <Box
      className={`${internalClassName}--page-links`}
      style={paginationStyle?.pageLinks}>
      {beginningPages.map((beginningPage) => (
        <Link
          key={beginningPage}
          className={`${internalClassName}--page-link`}
          data-testid={
            testId ? `${testId}-page-link-${beginningPage}` : undefined
          }
          style={paginationStyle?.pageLink}
          onClick={() => handlePageChange(beginningPage)}>
          {beginningPage}
        </Link>
      ))}
      {beginningPages.length > 0 && (
        <Typography
          component="span"
          className={`${internalClassName}--page-link-disabled`}
          style={paginationStyle?.pageLinkDisabled}>
          ...
        </Typography>
      )}
      {middlePages.map((middlePage) => (
        <Link
          key={middlePage}
          className={`${internalClassName}--page-link ${
            middlePage === page ? `${internalClassName}--page-link-active` : ''
          }`}
          data-testid={testId ? `${testId}-page-link-${middlePage}` : undefined}
          style={paginationStyle?.pageLink}
          onClick={() => handlePageChange(middlePage)}>
          {middlePage}
        </Link>
      ))}
      {endingPages.length > 0 && (
        <Typography
          component="span"
          className={`${internalClassName}--page-link-disabled`}
          style={paginationStyle?.pageLinkDisabled}>
          ...
        </Typography>
      )}
      {endingPages.map((endingPage) => (
        <Link
          key={endingPage}
          className={`${internalClassName}--page-link`}
          data-testid={testId ? `${testId}-page-link-${endingPage}` : undefined}
          style={paginationStyle?.pageLink}
          onClick={() => handlePageChange(endingPage)}>
          {endingPage}
        </Link>
      ))}
    </Box>
  );

  return (
    <Box
      className={`${internalClassName}--container ${
        isMobile && !isSmallMobile ? `${internalClassName}--mobile` : ''
      } ${
        isSmallMobile ? `${internalClassName}--small-mobile` : ''
      } ${className}`}
      style={paginationStyle?.container}>
      {isMobile ? (
        <Grid
          container
          spacing={0}
          direction="row"
          justifyContent="space-between"
          alignItems="center">
          <Grid
            container
            item
            xs={7}
            justifyContent="flex-start"
            alignItems="center">
            <Info />
          </Grid>
          <Grid
            container
            item
            xs={5}
            direction="row"
            justifyContent="flex-end"
            alignItems="center">
            <ItemsPerPage />
          </Grid>
          {isSmallMobile ? (
            <>
              <Grid
                container
                item
                xs={12}
                justifyContent="center"
                alignItems="center">
                <PreviousButtons />
                <NextButtons />
              </Grid>
              <Grid
                container
                item
                xs={12}
                justifyContent="center"
                alignItems="center">
                <PageLinks />
              </Grid>
            </>
          ) : (
            <Grid
              container
              item
              xs={12}
              justifyContent="center"
              alignItems="center">
              <PreviousButtons />
              <PageLinks />
              <NextButtons />
            </Grid>
          )}
        </Grid>
      ) : (
        <Grid
          container
          spacing={0}
          direction="row"
          justifyContent="space-between"
          alignItems="center">
          <Grid
            container
            item
            xs={2}
            justifyContent="flex-start"
            alignItems="center">
            <Info />
          </Grid>
          <Grid
            container
            item
            xs={8}
            justifyContent="center"
            alignItems="center">
            <PreviousButtons />
            <PageLinks />
            <NextButtons />
          </Grid>
          <Grid
            container
            item
            xs={2}
            direction="row"
            justifyContent="flex-end"
            alignItems="center">
            <ItemsPerPage />
          </Grid>
        </Grid>
      )}
    </Box>
  );
}

export default Pagination;
