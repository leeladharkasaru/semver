import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import Pagination from './Pagination';

const meta: Meta<typeof Pagination> = {
  component: Pagination,
  title: 'Components/Pagination',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Pagination>;

export const Default: ComponentStory = (args: any) => (
  <Pagination {...args} />
);
Default.args = {
  numberOfItems: 1000,
  page: 1,
  itemsPerPage: 10,
  itemLabel: 'items',
  testId: 'test-pagination',
  onPageChange: (page: number, itemsPerPage: number) => {
    console.log(`Page: ${page}, Items per page: ${itemsPerPage}`);
  },
};

export const OneItem: ComponentStory = (args: any) => (
  <Pagination {...args} />
);
OneItem.args = {
  numberOfItems: 1,
  page: 1,
  itemsPerPage: 10,
  itemLabel: 'items',
  testId: 'test-pagination',
  onPageChange: (page: number, itemsPerPage: number) => {
    console.log(`Page: ${page}, Items per page: ${itemsPerPage}`);
  },
};

export const PaginationOffset: ComponentStory = (args: any) => (
  <Pagination {...args} />
);
PaginationOffset.args = {
  numberOfItems: 1000,
  page: 53,
  itemsPerPage: 10,
  itemLabel: 'items',
  testId: 'test-pagination',
  onPageChange: (page: number, itemsPerPage: number) => {
    console.log(`Page: ${page}, Items per page: ${itemsPerPage}`);
  },
};

export const PaginationEnd: ComponentStory = (args: any) => (
  <Pagination {...args} />
);
PaginationEnd.args = {
  numberOfItems: 1000,
  page: 100,
  itemsPerPage: 10,
  itemLabel: 'items',
  testId: 'test-pagination',
  onPageChange: (page: number, itemsPerPage: number) => {
    console.log(`Page: ${page}, Items per page: ${itemsPerPage}`);
  },
};

export const OverrideStyles: ComponentStory = (args: any) => (
  <Pagination {...args} />
);
OverrideStyles.args = {
  numberOfItems: 1000,
  page: 1,
  itemsPerPage: 10,
  itemLabel: 'items',
  testId: 'test-pagination',
  paginationStyle: {
    container: {
      backgroundColor: 'lightgray',
      padding: '10px',
    },
    info: {
      color: 'blue',
    },
    itemsPerPage: {
      color: 'green',
    },
    nextButtons: {
      border: '1px solid black',
    },
    prevButtons: {
      border: '1px solid black',
    },
    pageLinks: {
      border: '1px solid black',
    },
    pageLink: {
      color: 'black',
    },
    pageLinkDisabled: {
      color: 'gray',
    },
  },
  onPageChange: (page: number, itemsPerPage: number) => {
    console.log(`Page: ${page}, Items per page: ${itemsPerPage}`);
  },
};
