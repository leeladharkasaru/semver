export type PaginationStyle = {
  container?: Object;
  info?: Object;
  itemsPerPage?: Object;
  prevButtons?: Object;
  nextButtons?: Object;
  pageLinks?: Object;
  pageLink?: Object;
  pageLinkDisabled?: Object;
};

export interface PaginationProps {
  numberOfItems: number;
  page: number;
  itemsPerPage: number;
  itemsPerPageOptions?: number[];
  itemLabel?: string;
  className?: string;
  testId?: string;
  paginationStyle?: PaginationStyle;
  onPageChange: (page: number, itemsPerPage: number) => void;
}
