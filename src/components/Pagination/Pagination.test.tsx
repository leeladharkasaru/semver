import "@testing-library/jest-dom";
import { render, fireEvent } from "@testing-library/react";
import React from "react";
import Pagination from "./Pagination";
import { PaginationProps } from "./Pagination.types";
import { useMediaQuery } from "@material-ui/core";

jest.mock('@material-ui/core', () => ({
    ...jest.requireActual('@material-ui/core'),
    useMediaQuery: jest.fn().mockReturnValue(false),
}));

describe("Pagination Component", () => {
    let props: PaginationProps;
    let onPageChange: jest.Mock;

    const renderComponent = () => render(<Pagination {...props} />);

    beforeEach(() => {
        onPageChange = jest.fn();
        props = {
            numberOfItems: 100,
            page: 1,
            itemsPerPage: 10,
            itemsPerPageOptions: [5, 10, 15, 20, 100],
            itemLabel: "items",
            testId: "test-pagination",
            onPageChange,
        };
    });

    it("renders text", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-pagination-text")).toHaveTextContent("1-10 of100 items");
    });

    it("calls onPageChange when changing page", () => {
        // Act
        const { getByTestId } = renderComponent();
        fireEvent.click(getByTestId("test-pagination-page-link-2"));

        // Assert
        expect(onPageChange).toHaveBeenCalledWith(2, 10);
    });

    it("calls onPageChange when changing items per page", () => {
        // Act
        const { getByTestId } = renderComponent();
        fireEvent.change(getByTestId("test-pagination-items-per-page").querySelector('input') as HTMLInputElement, { target: { value: "15" } });

        // Assert
        expect(onPageChange).toHaveBeenCalledWith(1, 15);
    });

    it("does not call onPageChange when clicking on the current page", () => {
        // Act
        const { getByTestId } = renderComponent();
        fireEvent.click(getByTestId("test-pagination-page-link-1"));

        // Assert
        expect(onPageChange).not.toHaveBeenCalled();
    });

    it("renders beginning pages", () => {
        // Arrange
        props.page = 5;

        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-pagination-page-link-1")).toBeInTheDocument();
    });

    it("renders mobile view", () => {
        // Arrange
        ((useMediaQuery as unknown) as jest.Mock).mockImplementation((query: string) => {
            return query === "(max-width:768px)";
        });

        // Act
        const { container } = renderComponent();

        // Assert
        expect(container.querySelector('.bcn-pagination--mobile')).toBeInTheDocument();
    });

    it("renders small mobile view", () => {
        // Arrange
        ((useMediaQuery as unknown) as jest.Mock).mockResolvedValue(true);

        // Act
        const { container } = renderComponent();

        // Assert
        expect(container.querySelector('.bcn-pagination--small-mobile')).toBeInTheDocument();
    });

    it("does not overflow the page range when clicking previous page button", () => {
        // Arrange
        props.numberOfItems = 10;

        // Act
        const { getByTestId } = renderComponent();
        fireEvent.click(getByTestId("test-pagination-prev-page-button"));

        // Assert
        expect(onPageChange).not.toHaveBeenCalled();
    });

    it("does not overflow the page range when clicking next page button", () => {
        // Arrange
        props.numberOfItems = 10;

        // Act
        const { getByTestId } = renderComponent();
        fireEvent.click(getByTestId("test-pagination-next-page-button"));

        // Assert
        expect(onPageChange).not.toHaveBeenCalled();
    });

    it("should show active page", () => {
        // Arrange
        props.page = 2;

        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-pagination-page-link-2")).toHaveClass("bcn-pagination--page-link-active");
    });

    it("should not show active page for the non-current pages", () => {
        // Arrange
        props.page = 2;

        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-pagination-page-link-1")).not.toHaveClass("bcn-pagination--page-link-active");
        expect(getByTestId("test-pagination-page-link-3")).not.toHaveClass("bcn-pagination--page-link-active");
    });
});
