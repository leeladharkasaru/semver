import '@testing-library/jest-dom';
import { fireEvent, render } from '@testing-library/react';
import React from 'react';
import Image from './Image';
import { ImageProps } from './Image.types';

describe('Image Component', () => {
  let props: ImageProps;

  beforeEach(() => {
    props = {
      src: 'test.jpg',
      alt: 'Test Image',
      fallbackSrc: 'fallback.jpg',
      className: 'test-class',
      testId: 'test-image',
      imageStyle: {
        backgroundColor: 'red',
      },
    };
  });

  const renderComponent = () => render(<Image {...props} />);

  it('should render the Image component', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-image')).toBeInTheDocument();
  });

  it('should render the image with the correct src', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-image')).toHaveAttribute('src', 'test.jpg');
  });

  it('should render the image with the correct alt', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-image')).toHaveAttribute('alt', 'Test Image');
  });

  it('should render the image with the correct class', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-image')).toHaveClass('test-class');
  });

  it('should render the image with the correct imageStyle', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-image')).toHaveStyle({ backgroundColor: 'red' });
  });

  it('should render the image with the correct testId', () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId('test-image')).toHaveAttribute('data-testid', 'test-image');
  });

  it('should not render the image with the testId attribute if not provided', () => {
    // Arrange
    props.testId = undefined;

    // Act
    const { getByRole } = renderComponent();

    // Assert
    expect(getByRole('img')).not.toHaveAttribute('data-testid');
  });

  it('should render the image with the correct fallback image', () => {
    // Act
    const { getByTestId } = renderComponent();
    fireEvent.error(getByTestId('test-image'));

    // Assert
    expect(getByTestId('test-image')).toHaveAttribute('src', 'fallback.jpg');
  });

  it('should handle on error only once', () => {
    // Act
    const { getByTestId } = renderComponent();
    fireEvent.error(getByTestId('test-image'));
    fireEvent.error(getByTestId('test-image'));

    // Assert
    expect(getByTestId('test-image')).toHaveAttribute('src', 'fallback.jpg');
  });
});
