import React from 'react';
import { ImageProps, ImageState } from './Image.types';

const Image: React.FC<ImageProps> = ({
  src,
  alt,
  fallbackSrc,
  className,
  testId,
  imageStyle,
}) => {
  // Save the image state
  const [state, setState] = React.useState<ImageState>({
    src,
    errored: false,
  });

  // Handle the image error by using the fallback image
  const handleError: React.ReactEventHandler<HTMLImageElement> = () => {
    // Halt if the image has already errored
    if (state.errored || !fallbackSrc) {
      return;
    }

    // Use fallback image if provided
    if (fallbackSrc) {
      setState({
        src: fallbackSrc,
        errored: true,
      });
    }
  };

  // Render the image
  return (
    <img
      src={state.src}
      alt={alt}
      className={className}
      data-testid={testId || undefined}
      style={imageStyle}
      onError={handleError}
    />
  );
};

export default Image;
