import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import Image from './Image';

const meta: Meta<typeof Image> = {
  component: Image,
  title: 'Components/Image',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Image>;

export const Default: ComponentStory = (args: any) => (
  <Image {...args} />
);
Default.args = {
  src: 'https://dev-static.becn.digital/insecure/plain/images/large/C-635001_default_small.jpg',
  alt: 'Product Image',
  testId: 'test-product-image',
};

export const ErrorWithFallback: ComponentStory = (args: any) => (
  <Image {...args} />
);
ErrorWithFallback.args = {
  src: 'https://dev-static.becn.digital/insecure/plain/images/large/invalid_image.jpg',
  fallbackSrc: 'https://dev-static.becn.digital/insecure/plain/images/large/C-635001_default_small.jpg',
  alt: 'Product Image',
  testId: 'test-product-image',
};

export const ErrorWithoutFallback: ComponentStory = (args: any) => (
  <Image {...args} />
);
ErrorWithoutFallback.args = {
  src: 'https://dev-static.becn.digital/insecure/plain/images/large/invalid_image.jpg',
  alt: 'Product Image',
  testId: 'test-product-image',
};
