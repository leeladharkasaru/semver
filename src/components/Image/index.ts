import Image from './Image';

export type {
  ImageProps,
} from './Image.types';
export { Image };
