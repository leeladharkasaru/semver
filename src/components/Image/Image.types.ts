export interface ImageProps {
    src: string;
    alt: string;
    fallbackSrc?: string;
    className?: string;
    testId?: string;
    imageStyle?: Object;
}

export interface ImageState {
    src: string;
    errored: boolean;
}
