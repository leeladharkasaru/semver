import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import Link from './Link';

const meta: Meta<typeof Link> = {
  component: Link,
  title: 'Components/Link',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Link>;

export const Default: ComponentStory = (args: any) => (
  <Link {...args} />
);
Default.args = {
  children: 'Link',
  href: '#',
  className: 'test',
  variant: 'default',
  color: 'primary',
  testId: 'test-link',
  onClick: (e) => {
    e.preventDefault();
    console.log('Link clicked', e);
  },
};

export const Bold: ComponentStory = (args: any) => (
  <Link {...args} />
);
Bold.args = {
  children: 'Link',
  href: '#',
  className: 'test',
  variant: 'bold',
  color: 'primary',
  testId: 'test-link',
  onClick: (e) => {
    e.preventDefault();
    console.log('Link clicked', e);
  },
};

export const OverrideStyles: ComponentStory = (args: any) => (
  <Link {...args} />
);
OverrideStyles.args = {
  children: 'Link',
  href: '#',
  className: 'test',
  variant: 'default',
  color: 'primary',
  testId: 'test-link',
  onClick: (e) => {
    e.preventDefault();
    console.log('Link clicked', e);
  },
  linkStyle: {
    container: {
      color: 'red',
    },
  },
};
