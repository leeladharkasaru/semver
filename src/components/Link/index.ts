import Link from './Link';

export type {
  LinkProps,
  LinkVariant,
  LinkColor,
  LinkStyle,
} from './Link.types';
export { Link };
