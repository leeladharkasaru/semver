import "@testing-library/jest-dom";
import { render, fireEvent } from "@testing-library/react";
import React from "react";
import Link from "./Link";
import { LinkProps } from "./Link.types";

describe("Link Component", () => {
  let props: LinkProps;

  const renderComponent = () => render(<Link {...props} />);

  beforeEach(() => {
    props = {
      children: "Test Link",
      href: "http://test.com",
      className: "test-class",
      color: "primary",
      testId: "test-id",
      linkStyle: {
        container: {
          backgroundColor: "red",
        },
      },
      onClick: jest.fn(),
    } as any;
  });

  it("should render the link with the correct text", () => {
    // Act
    const { getByText } = renderComponent();

    // Assert
    expect(getByText("Test Link")).toBeInTheDocument();
  });

  it("should have the correct class", () => {
    // Act
    const { container } = renderComponent();

    // Assert
    expect(container.firstChild).toHaveClass("bcn-link");
    expect(container.firstChild).toHaveClass("bcn-link--primary");
    expect(container.firstChild).toHaveClass("test-class");
  });

  it("should have the correct style", () => {
    // Act
    const { container } = renderComponent();

    // Assert
    expect(container.firstChild).toHaveStyle("background-color: red");
  });

  it("should call onClick when clicked", () => {
    // Act
    const { getByTestId } = renderComponent();
    fireEvent.click(getByTestId("test-id"));

    // Assert
    expect(props.onClick).toHaveBeenCalled();
  });

  it("should have the correct href", () => {
    // Act
    const { getByTestId } = renderComponent();

    // Assert
    expect(getByTestId("test-id")).toHaveAttribute("href", "http://test.com");
  });
});
