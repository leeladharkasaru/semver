import './Link.scss';
import React from 'react';
import { Link } from '@material-ui/core';
import { LinkProps } from './Link.types';

const interalClassName = 'bcn-link';

const BeaconLink: React.FC<LinkProps> = ({
  children,
  href = '#',
  className = '',
  variant = 'default',
  color = 'primary',
  testId,
  linkStyle,
  onClick,
}) => (
  <Link
    href={href}
    className={`${interalClassName} ${interalClassName}--${variant} ${interalClassName}--${color} ${className}`}
    style={linkStyle?.container}
    data-testid={testId}
    onClick={onClick}
  >
    {children}
  </Link>
);

export default BeaconLink;
