import { MouseEventHandler } from 'react';

export type LinkVariant = 'default' | 'bold';

export type LinkColor = 'primary';

export type LinkStyle = {
    container?: Object;
};
export interface LinkProps {
    href?: string;
    className?: string;
    variant?: LinkVariant;
    color?: LinkColor;
    testId?: string;
    linkStyle?: LinkStyle;
    onClick?: MouseEventHandler<HTMLAnchorElement>;
}
