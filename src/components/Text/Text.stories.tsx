import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import Text from './Text';

const meta: Meta<typeof Text> = {
  component: Text,
  title: 'Components/Text',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Text>;

export const Default: ComponentStory = (args: any) => (
  <Text {...args} />
);
Default.args = {
  children: 'Text',
  className: 'custom-class',
  variant: 'default',
  testId: 'test-text',
};

export const Bold: ComponentStory = (args: any) => (
  <Text {...args} />
);
Bold.args = {
  children: 'Text',
  className: 'custom-class',
  variant: 'bold',
  testId: 'test-text',
};

export const OverrideStyle: ComponentStory = (args: any) => (
  <Text {...args} />
);
OverrideStyle.args = {
  children: 'Text',
  className: 'custom-class',
  variant: 'default',
  component: 'span',
  testId: 'test-text',
  textStyles: {
    container: {
      color: 'red',
    },
  },
};
