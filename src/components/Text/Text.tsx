import './Text.scss';
import React from 'react';
import { Typography } from '@material-ui/core';
import { TextProps } from './Text.types';

const internalClassName = 'becn-text';

const Text: React.FC<TextProps> = ({
  className = '',
  children,
  variant = 'default',
  component = 'p',
  testId,
  textStyles,
}) => (
  <Typography
    variant="body1"
    className={`${internalClassName} ${internalClassName}--${variant} ${className}`}
    component={component}
    data-testid={testId}
    style={textStyles?.container}
  >
    {children}
  </Typography>
);

export default Text;
