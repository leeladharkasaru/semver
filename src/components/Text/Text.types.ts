export interface TextProps {
    className?: string;
    variant?: TextVariant;
    component?: TextComponent;
    testId?: string;
    textStyles?: TextStyles;
}

export type TextVariant = 'default' | 'bold' | 'caption';

export type TextComponent = 'p' | 'span' | 'div';

export type TextStyles = {
    container?: Object;
}
