import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import React from "react";
import Text from "./Text";
import { TextProps } from "./Text.types";

describe("Text Component", () => {
    let props: TextProps;

    beforeEach(() => {
        props = {
            children: "Test Text",
            variant: "default",
            component: "p",
            testId: "test-text",
            textStyles: {
                container: {
                    color: "red",
                },
            },
        } as any;
    });

    const renderComponent = () => render(<Text {...props} />);

    it("should render the text", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-text")).toBeInTheDocument();
    });

    it("should render with the correct variant", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-text")).toHaveClass("becn-text--default");
    });

    it("should apply the textStyles prop to the container", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("test-text")).toHaveStyle({ color: "red" });
    });

    it("should render with the correct component", () => {
        // Act
        const { container } = renderComponent();

        // Assert
        expect(container.querySelector("p")).toBeInTheDocument();
    });
});
