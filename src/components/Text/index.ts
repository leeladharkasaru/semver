import Text from './Text';

export type {
  TextComponent, TextProps, TextStyles, TextVariant,
} from './Text.types';
export { Text };
