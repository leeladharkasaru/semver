import { render } from '@testing-library/react';
import React from 'react';
import DatePicker from './DatePicker';
import { DatePickerProps } from './DatePicker.types';

describe('Test Component', () => {
  let props: DatePickerProps;

  beforeEach(() => {
    props = {
      dateValue: '',
      changeHandler: () => {}
    };
  });

  const renderComponent = () => render(<DatePicker {...props} />);

  it('should render beacon text correctly', () => {
    // Assign
    const { getByTestId } = renderComponent();
    // Act
    const component = getByTestId('DatePicker');
    // Assert
    expect(component).toBeDefined();
  });
});
