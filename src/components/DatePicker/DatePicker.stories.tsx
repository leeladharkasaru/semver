import { ComponentStoryObj, Meta } from '@storybook/react';
import React from 'react';
import DatePicker from './DatePicker';
const meta: Meta<typeof DatePicker> = {
  component: DatePicker,
  title: 'Components/DatePicker',
  argTypes: {}
};
export default meta;
type ComponentStory = ComponentStoryObj<typeof DatePicker>;
export const Date_Picker: ComponentStory = (args) => <DatePicker {...args} />;
export const Date_Picker_Error: ComponentStory = (args) => (
  <DatePicker {...args} />
);

Date_Picker.args = {
  // error:true,
  helperText: 'MMDDYYYY',
  format: 'YYYY/DD/MM',
  label: 'Common Date Picker',
  changeHandler: () => {}
};
Date_Picker_Error.args = {
  error: true
};
