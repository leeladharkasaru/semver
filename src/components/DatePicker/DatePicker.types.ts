export interface DatePickerProps {
  label?: string;
  error?: boolean;
  helperText?: string;
  variant?: 'filled' | 'outlined' | 'standard';
  dataTestId?: string;
  fullWidth?: boolean;
  format?: string;
  dateValue: any;
  inputClass?: any;
  inputIconClass?: any;
  changeHandler: (changeValue: any) => void;
  focusLabelColor?:string
}
