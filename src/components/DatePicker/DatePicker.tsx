import { FormControl, InputAdornment, TextField } from '@material-ui/core';
import moment from 'moment';
import React, { useState } from 'react';
import { DATE_FORMATS } from '../../common/constants';
import './DatePicker.scss';
import { DatePickerProps } from './DatePicker.types';

const DatePicker: React.FC<DatePickerProps> = ({
  label,
  error,
  helperText,
  variant,
  dataTestId,
  format,
  fullWidth,
  dateValue,
  inputClass = '',
  inputIconClass = '',
  focusLabelColor,
  changeHandler
}) => {
  const [date, setDate] = useState<any>(dateValue);
  const [iserror, setError] = useState<any>(error);
  const handleChange = (e: any) => {
    let selectedDate = '';
    selectedDate = moment(new Date(e.target.value)).format(
      format || DATE_FORMATS[0]
    );
    setDate(selectedDate);
    if (selectedDate) setError(false);
    changeHandler(selectedDate);
  };
  const handleBlur = (e: any) => {
    if (!e.target.value.length) setError(true);
  };
  return (
    <div data-testid="DatePicker" className="date-picker-container">
      <FormControl fullWidth={fullWidth} error={iserror}>
        <TextField
          data-testid={`${dataTestId}-container`}
          variant={variant || 'outlined'}
          value={date}
          InputLabelProps={{
            shrink: date?.length,
            style: { color: focusLabelColor }
          }}
          label={label}
          className={inputClass}
          InputProps={{
            endAdornment: (
              <InputAdornment position="start">
                <input
                  className={`background-date ${inputIconClass}`}
                  type="date"
                  data-testid={`${dataTestId}-icon`}
                  onChange={handleChange}
                />
              </InputAdornment>
            )
          }}
          helperText={helperText}
          error={iserror}
          size="small"
          onBlur={handleBlur}
        />
      </FormControl>
    </div>
  );
};

export default DatePicker;
