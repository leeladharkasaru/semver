import './InputNumber.scss';
import React from 'react';
import { Box, IconButton, Input } from '@material-ui/core';
import { Add, Remove } from '@material-ui/icons';
import { InputNumberProps } from './InputNumber.types';

const internalClassName = 'becn-input-number';

const InputNumber: React.FC<InputNumberProps> = ({
  className = '',
  value: initialValue = 0,
  minValue = 0,
  maxValue = 9999,
  testId,
  inputNumberStyle,
  onChange
}) => {
  // Save component state
  const [value, setValue] = React.useState(initialValue);

  // Clean the input value and limit it to the min and max
  const getCleanValue = (dirtyValue: number | string) => {
    // Parse the string value to a number
    let cleanValue = 0;
    if (typeof dirtyValue === 'string') {
      const numericValue = dirtyValue.replace(/[^0-9]/g, '');
      cleanValue = parseInt(numericValue, 10) || 0;
    } else {
      cleanValue = dirtyValue;
    }

    // Limit the value to the min and max
    if (cleanValue < minValue) {
      cleanValue = minValue;
    } else if (cleanValue > maxValue) {
      cleanValue = maxValue;
    }

    return cleanValue;
  };

  // Update the value and trigger the onChange callback
  const updateValue = (newValue: number) => {
    // Update the value
    setValue(newValue);

    // Trigger the onChange callback
    onChange(newValue);
  };

  // Handle increment button click
  const onIncrement = () => {
    const newValue = getCleanValue(value + 1);
    updateValue(newValue);
  };

  // Handle decrement button click
  const onDecrement = () => {
    const newValue = getCleanValue(value - 1);
    updateValue(newValue);
  };

  // Handle Enter key press
  const handleKeyDown: React.KeyboardEventHandler<
    HTMLTextAreaElement | HTMLInputElement
  > = (event) => {
    const key = event.key.toUpperCase();
    if (key === 'ENTER') {
      const newValue = getCleanValue(event.currentTarget.value);
      updateValue(newValue);
    }
  };

  // Handle blur event on the input
  const handleBlur: React.FocusEventHandler<
    HTMLInputElement | HTMLTextAreaElement
  > = (event) => {
    const newValue = getCleanValue(event.currentTarget.value);
    updateValue(newValue);
  };

  // Handle change event on the input
  const handleChange: React.ChangeEventHandler<
    HTMLTextAreaElement | HTMLInputElement
  > = (event) => {
    const newValue = getCleanValue(event.target.value);
    setValue(newValue);
  };

  return (
    <Box
      className={`${internalClassName}--container ${className}`}
      style={inputNumberStyle?.container}>
      <IconButton
        aria-label="decrement"
        onClick={onDecrement}
        className={`${internalClassName}--icon-button`}
        style={inputNumberStyle?.iconButton}
        data-testid={testId ? `${testId}-decrement` : undefined}>
        <Remove
          className={`${internalClassName}--icon`}
          style={inputNumberStyle?.icon}
        />
      </IconButton>
      <Input
        className={`${internalClassName}--input`}
        value={value}
        onKeyDown={handleKeyDown}
        onBlur={handleBlur}
        onChange={handleChange}
        style={inputNumberStyle?.input}
        data-testid={testId ? `${testId}-input` : undefined}
      />
      <IconButton
        aria-label="increment"
        onClick={onIncrement}
        className={`${internalClassName}--icon-button`}
        style={inputNumberStyle?.iconButton}
        data-testid={testId ? `${testId}-increment` : undefined}>
        <Add
          className={`${internalClassName}--icon`}
          style={inputNumberStyle?.icon}
        />
      </IconButton>
    </Box>
  );
};

export default InputNumber;
