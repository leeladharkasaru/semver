import { render, fireEvent } from "@testing-library/react";
import React from "react";
import InputNumber from "./InputNumber";
import { InputNumberProps } from "./InputNumber.types";

describe("Input Number Component", () => {
    let props: InputNumberProps;

    const renderComponent = () => render(<InputNumber {...props} />);

    beforeEach(() => {
        props = {
            value: 123,
            testId: "test-input-number",
            onChange: jest.fn(),
        };
    });

    it("should render the component", () => {
        // Act
        const { container } = renderComponent();

        // Assert
        expect(container).toBeDefined();
    });

    it("should render the value", () => {
        // Act
        const { getByDisplayValue } = renderComponent();

        // Assert
        expect(getByDisplayValue("123")).toBeDefined();
    });

    it("should render the increment button", () => {
        // Act
        const { getByLabelText } = renderComponent();

        // Assert
        expect(getByLabelText("increment")).toBeDefined();
    });

    it("should render the decrement button", () => {
        // Act
        const { getByLabelText } = renderComponent();

        // Assert
        expect(getByLabelText("decrement")).toBeDefined();
    });

    it("should not go below minValue", () => {
        // Arrange
        props.value = 0;

        // Act
        const { getByTestId } = renderComponent();
        fireEvent.click(getByTestId("test-input-number-decrement"));

        // Assert
        const input = getByTestId("test-input-number-input").querySelector("input") as HTMLInputElement;
        expect(input.value).toBe("0");
    });

    it("should not go above maxValue", () => {
        // Arrange
        props.value = 9999;

        // Act
        const { getByTestId } = renderComponent();
        fireEvent.click(getByTestId("test-input-number-increment"));

        // Assert
        const input = getByTestId("test-input-number-input").querySelector("input") as HTMLInputElement;
        expect(input.value).toBe("9999");
    });

    it("should call onChange with the correct value when increment button clicked", () => {
        // Act
        const { getByTestId } = renderComponent();
        fireEvent.click(getByTestId("test-input-number-increment"));

        // Assert
        expect(props.onChange).toHaveBeenCalledWith(124);
    });

    it("should call onChange with the correct value when increment button clicked", () => {
        // Act
        const { getByTestId } = renderComponent();
        fireEvent.click(getByTestId("test-input-number-decrement"));

        // Assert
        expect(props.onChange).toHaveBeenCalledWith(122);
    });

    it("should call onChange with the correct value when input changes with blur", () => {
        // Act
        const { getByTestId } = renderComponent();
        const input = getByTestId("test-input-number-input").querySelector("input") as HTMLInputElement;
        fireEvent.change(input, { target: { value: "125" } });
        fireEvent.blur(input);

        // Assert
        expect(props.onChange).toHaveBeenCalledWith(125);
    });

    it("should call onChange with the correct value when input changes with enter", () => {
        // Act
        const { getByTestId } = renderComponent();
        const input = getByTestId("test-input-number-input").querySelector("input") as HTMLInputElement;
        fireEvent.change(input, { target: { value: "125" } });
        fireEvent.keyDown(input, { key: "Enter", code: "13" });

        // Assert
        expect(props.onChange).toHaveBeenCalledWith(125);
    });
});
