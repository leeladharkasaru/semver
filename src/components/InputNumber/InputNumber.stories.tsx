import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import InputNumber from './InputNumber';

const meta: Meta<typeof InputNumber> = {
  component: InputNumber,
  title: 'Components/InputNumber',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof InputNumber>;

export const Default: ComponentStory = (args: any) => (
  <InputNumber {...args} />
);
Default.args = {
  value: 100,
  testId: 'test-input-number',
  onChange: (value: number) => console.log(value),
};

export const OverrideStyles: ComponentStory = (args: any) => (
  <InputNumber {...args} />
);
OverrideStyles.args = {
  value: 100,
  testId: 'test-input-number',
  inputNumberStyle: {
    container: {
      'border-color': 'red',
    },
    input: {
      border: 'solid 1px red',
    },
    icon: {
      color: 'red',
    },
    iconButton: {
      color: 'red',
    },
  },
};
