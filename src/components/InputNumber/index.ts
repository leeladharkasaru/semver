import InputNumber from './InputNumber';

export type { InputNumberProps, InputNumberStyle } from './InputNumber.types';
export { InputNumber };
