export interface InputNumberProps {
    className?: string;
    value?: number;
    minValue?: number;
    maxValue?: number;
    testId?: string;
    inputNumberStyle?: InputNumberStyle;
    onChange?: (value: number) => void;
}

export type InputNumberStyle = {
    container?: Object;
    input?: Object;
    iconButton?: Object;
    icon?: Object;
};
