/* eslint-disable react/jsx-one-expression-per-line */
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Link,
  Typography,
  createTheme,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import React from 'react';
import './AccordianCard.scss';
import {
  AccordianCardProps,
  Document,
  ProductItem,
} from './AccordianCard.types';

export const Paneltheme = createTheme({
  palette: {
    type: 'light',
  },

  /* MuiTypography: {
            styleOverrides: {
                root: {
                    margin: 0,
                    padding: 0,
                },
            },
        },
        MuiAccordion: {
            styleOverrides: {
                root: {
                    ":before": {
                        opacity: 0,
                        margin: 0,
                        padding: 0,
                    },
                    borderBottom: "2px solid black",
                    width: "fit-content",
                },
            },
        },
        MuiAccordionSummary: {
            styleOverrides: {
                root: {
                    margin: 0,
                    padding: 0,
                    width: "fit-content",
                },
            },
        },
        MuiAccordionDetails: {
            styleOverrides: {
                root: {
                    width: "inherit",
                    wordBreak: "break-word",
                },
            },
        },
        MuiTab: {
            styleOverrides: {
                root: {
                    border: "1px solid black",
                    textTransform: "none",
                },
            },
        }, */
});

const DocumentIcon = () => (
  <Typography>
    <svg
      width="20"
      height="22"
      viewBox="0 0 22 27"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M5.66732 21.5001H16.334V18.8334H5.66732V21.5001ZM5.66732 16.1667H16.334V13.5001H5.66732V16.1667ZM3.00065 26.8334C2.26732 26.8334 1.63954 26.5723 1.11732 26.0501C0.595096 25.5279 0.333984 24.9001 0.333984 24.1667V2.83341C0.333984 2.10008 0.595096 1.4723 1.11732 0.950081C1.63954 0.427859 2.26732 0.166748 3.00065 0.166748H13.6673L21.6673 8.16675V24.1667C21.6673 24.9001 21.4062 25.5279 20.884 26.0501C20.3618 26.5723 19.734 26.8334 19.0007 26.8334H3.00065ZM12.334 9.50008V2.83341H3.00065V24.1667H19.0007V9.50008H12.334Z"
        fill="#1C1B1F"
      />
    </svg>
  </Typography>
);

const OpenIcon = (): any => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 24 24"
    width="24px"
    height="24px"
  >
    <path
      fill="#285D99"
      d="M 19.980469 2.9902344 A 1.0001 1.0001 0 0 0 19.869141 3 L 15 3 A 1.0001 1.0001 0 1 0 15 5 L 17.585938 5 L 8.2929688 14.292969 A 1.0001 1.0001 0 1 0 9.7070312 15.707031 L 19 6.4140625 L 19 9 A 1.0001 1.0001 0 1 0 21 9 L 21 4.1269531 A 1.0001 1.0001 0 0 0 19.980469 2.9902344 z M 5 3 C 3.9069372 3 3 3.9069372 3 5 L 3 19 C 3 20.093063 3.9069372 21 5 21 L 19 21 C 20.093063 21 21 20.093063 21 19 L 21 13 A 1.0001 1.0001 0 1 0 19 13 L 19 19 L 5 19 L 5 5 L 11 5 A 1.0001 1.0001 0 1 0 11 3 L 5 3 z"
    />
  </svg>
);

export const WARRANTY_MESSAGE = {
  ORDER:
    'Warranty information is not provided for the products listed in your order. For questions, please reach out to your local branch.',
  QUOTE:
    'Warranty information is not available for products listed in this quote. For questions, please reach out to your local branch.',
};

export const GroupHeading = ({ group, groupStyle }: any) => (
  <Typography data-testid="group-header" variant="h6" style={groupStyle}>
    {group}
  </Typography>
);

const BecnAccordianCard: React.FC<AccordianCardProps> = ({
  itemList,
  groupStyle,
  documentStyle,
}) => (
  <Box style={{ marginTop: 7.5 }}>
    {itemList.length ? (
      itemList.map((item: ProductItem, index: number) => (
        <>
          <GroupHeading group={item?.groupHeading} groupStyle={groupStyle} />
          <Box className="accordian-box">
            <Accordion data-testid={`accordian-header-${index}`} elevation={0}>
              <AccordionSummary
                data-testid={`accordian-message-${index}`}
                aria-controls="panel1a-content"
                expandIcon={<ExpandMoreIcon />}
                id="panel1a-header"
              >
                <Box
                  className="document-box"
                  data-testid={`accordian-icon-${index}`}
                >
                  <DocumentIcon />
                </Box>
                <Box style={documentStyle.aBoxSummaryStyle}>
                  {item.productName}
                  <Typography data-testid={`accordian-item-${index}`}>
                    Item#: {item.itemNumber}
                  </Typography>
                </Box>
              </AccordionSummary>

              <AccordionDetails>
                <Typography>
                  <Box>
                    {item?.docs?.map((doc: Document) => (
                      <Link
                        className="doc-link"
                        key={doc.label}
                        target="_blank"
                        href={doc.value || ''}
                      >
                        <Typography style={documentStyle}>
                          {doc.label}
                        </Typography>
                        <OpenIcon />
                      </Link>
                    ))}
                  </Box>
                </Typography>
              </AccordionDetails>
            </Accordion>
          </Box>
        </>
      ))
    ) : (
      <Box>
        <Typography
          data-testid="no-warranty-message"
          variant="body2"
          gutterBottom
          style={documentStyle.noDocumentSummaryStyle}
        >
          {WARRANTY_MESSAGE.ORDER}
        </Typography>
      </Box>
    )}
  </Box>
);

export default BecnAccordianCard;
