import { ComponentMeta } from '@storybook/react';
import React from 'react';
import AccordianCard from './AccordianCard';
import { DocumentPanel, ProductItem } from './AccordianCard.types';

const meta: ComponentMeta<typeof AccordianCard> = {
  component: AccordianCard,
  title: 'Components/AccordianCard',
  argTypes: {},
};
export default meta;

const groupStyle: React.CSSProperties = {
  fontWeight: '900',
  textTransform: 'uppercase',
  fontFamily: 'titling-gothic-fb-compressed-bold,arial,sans-serif',
  fontSize: '1.75rem',
  color: '#2424242',
  letterSpacing: '0.15px',
};

const documentStyle = {
  docIconStyle: {
    m: 0,
    mt: 1,
    p: 0,
  },
  aBoxSummaryStyle: {
    margin: '05.vw',
    marginLeft: '.5vw',
    fontWeight: 600,
    fontSize: '1.25rem',
    fontFamily: 'Proxima Nova,arial,sans-serif',
    color: '#2424242',
  },
  aTypoSummaryStyle: {
    color: '#5F6369',
    fontSize: '1rem',
    fontFamily: 'Proxima Nova,arial,sans-serif',
  },
  aTypoDetailsStyle: {
    color: '#285D99',
    textDecoration: 'underline',
    marginTop: 8,
    cursor: 'pointer',
    display: 'flex',
    fontFamily: 'Proxima Nova,arial,sans-serif',
  },
  noDocumentSummaryStyle: {
    margin: '05.vw',
    marginLeft: '.5vw',
    fontWeight: 600,
    fontSize: '1.25rem',
    fontFamily: 'Proxima Nova,arial,sans-serif',
    color: '#2424242',
    marginTop: '4%',
    marginBottom: '4%',
  },
  noDocumentAvailableBox: {
    borderBottom: '2px solid #13151A',
  },
  noDocumentAvailable: {
    fontWeight: '600',
    width: '90vw',
  },
  noDocumentAvailableDesktop: {
    fontWeight: '600',
    width: '50vw',
  },
  noDocumentAvailableTab: {
    fontWeight: '600',
    width: '75vw',
  },
};

const itemList: ProductItem[] = [
  {
    productName:
      'GAF 13-1/4" x 39-3/8" Timberline HDZ&trade; Shingles with StainGuard Protection Charcoal',
    itemNumber: '693302',
    docs: [
      {
        label: 'Warranty Brochure',
        value:
          'https://www.gaf.com/en-us/document-library/documents/Warranty__GAF_Shingle__Accessory_Limited_RESWT160I_International.pdf',
      },
    ],
    groupHeading: 'Building location',
  },
  {
    productName: 'GTRI-BUILT 1" Coil Roofing Nails Carton of 7,200rcoal',
    itemNumber: '693302',
    docs: [
      {
        label: 'Product document',
        value:
          'https://www.gaf.com/en-us/document-library/documents/Warranty__GAF_Shingle__Accessory_Limited_RESWT160I_International.pdf',
      },
    ],
    groupHeading: 'Membrane',
  },
  {
    productName: 'TRI-BUILT 1" Coil Roofing Nails Carton of 7,200',
    itemNumber: '169330',
    docs: [
      {
        label: 'Warranty Brochure',
        value:
          'https://www.gaf.com/en-us/document-library/documents/Warranty__GAF_Shingle__Accessory_Limited_RESWT160I_International.pdf',
      },
    ],
    groupHeading: 'Air/Vapor used',
  },
];

export const Default = (args) => <AccordianCard {...args} />;

Default.args = {
  itemList,
  productType: DocumentPanel.PRODUCT,
  groupStyle,
  documentStyle,
};

export const NoWarranty = (args) => <AccordianCard {...args} />;

NoWarranty.args = {
  itemList: {},
  productType: DocumentPanel.WARRANTY,
  groupStyle,
  documentStyle,
};
