/* eslint-disable no-unused-vars */
import { CSSProperties } from 'react';

export enum DocumentPanel {
  PRODUCT = 'PRODUCT',
  WARRANTY = 'WARRANTY',
}

export interface Document {
  label: string;
  value: string;
}
export interface ProductItem {
  productName: string;
  itemNumber: string;
  docs: Document[];
  groupHeading?: string;
}
export interface AccordianCardProps {
  itemList: ProductItem[];
  productType: DocumentPanel;
  documentStyle: any;
  groupStyle: CSSProperties;
}
