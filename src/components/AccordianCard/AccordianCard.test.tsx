import { render } from '@testing-library/react';
import React from 'react';
import AccordianCard from './AccordianCard';
import {
  AccordianCardProps,
  DocumentPanel,
  ProductItem,
} from './AccordianCard.types';

const itemList: ProductItem[] = [
  {
    groupHeading: 'Test Beacon',
    productName:
      'GAF 13-1/4" x 39-3/8" Timberline HDZ&trade; Shingles with StainGuard Protection Charcoal',
    itemNumber: '693302',
    docs: [
      {
        label: 'Warranty Brochure',
        value:
          'https://www.gaf.com/en-us/document-library/documents/Warranty__GAF_Shingle__Accessory_Limited_RESWT160I_International.pdf',
      },
    ],
  },
];

describe('Accordian Component', () => {
  const props: AccordianCardProps = {
    itemList,
    productType: DocumentPanel.PRODUCT,
    groupStyle: {},
    documentStyle: {},
  };

  const renderComponent = () => render(<AccordianCard {...props} />);

  it('it should render accordian', () => {
    const { getByText } = renderComponent();
    expect(getByText('Item#: 693302')).toBeDefined();
  });
});
