import { FilterSourceDataProps } from './FilterSection.types';

// Filter data to generate the different filters as per the key defined in the data.
export const FILTER_DATA: FilterSourceDataProps = {
  Thickness: [
    {
      facetName: '.015"',
      facetId: '4294800738',
      recordCount: 1,
      selected: false,
    },
    {
      facetName: '.03"',
      facetId: '4294801302',
      recordCount: 2,
      selected: false,
    },
    {
      facetName: '.06"',
      facetId: '4294801234',
      recordCount: 6,
      selected: false,
    },
    {
      facetName: '.065"',
      facetId: '4294778638',
      recordCount: 1,
      selected: false,
    },
    {
      facetName: '.5"',
      facetId: '4294800834',
      recordCount: 1,
      selected: false,
    },
    {
      facetName: '0.5"',
      facetId: '4294801109',
      recordCount: 3,
      selected: false,
    },
    {
      facetName: '1 (.5" to .75")',
      facetId: '4294801106',
      recordCount: 1,
      selected: false,
    },
  ],
  Length: [
    {
      facetName: '1.25"',
      facetId: '4294778265',
      recordCount: 2,
      selected: false,
    },
    {
      facetName: '1.625"',
      facetId: '4294778242',
      recordCount: 1,
      selected: false,
    },
    {
      facetName: '1.8"',
      facetId: '4294828811',
      recordCount: 1,
      selected: false,
    },
    {
      facetName: '10"',
      facetId: '4294831399',
      recordCount: 2,
      selected: false,
    },
    {
      facetName: "10'",
      facetId: '4294831786',
      recordCount: 6,
      selected: false,
    },
    {
      facetName: "100'",
      facetId: '4294831754',
      recordCount: 39,
      selected: false,
    },
    {
      facetName: '11"',
      facetId: '4294830445',
      recordCount: 2,
      selected: false,
    },
  ],
};

/*
Filter data with pre selected filters to generate the
different filters as per the key defined in the data.
*/
export const PRE_SELECTED_FILTERS_DATA: FilterSourceDataProps = {
  Thickness: [
    {
      facetName: '.015"',
      facetId: '4294800738',
      recordCount: 1,
      selected: false,
    },
    {
      facetName: '.03"',
      facetId: '4294801302',
      recordCount: 2,
      selected: true,
    },
    {
      facetName: '.06"',
      facetId: '4294801234',
      recordCount: 6,
      selected: false,
    },
    {
      facetName: '.065"',
      facetId: '4294778638',
      recordCount: 1,
      selected: true,
    },
    {
      facetName: '.5"',
      facetId: '4294800834',
      recordCount: 1,
      selected: false,
    },
    {
      facetName: '0.5"',
      facetId: '4294801109',
      recordCount: 3,
      selected: false,
    },
    {
      facetName: '1 (.5" to .75")',
      facetId: '4294801106',
      recordCount: 1,
      selected: true,
    },
  ],
  Length: [
    {
      facetName: '1.25"',
      facetId: '4294778265',
      recordCount: 2,
      selected: false,
    },
    {
      facetName: '1.625"',
      facetId: '4294778242',
      recordCount: 1,
      selected: false,
    },
    {
      facetName: '1.8"',
      facetId: '4294828811',
      recordCount: 1,
      selected: true,
    },
    {
      facetName: '10"',
      facetId: '4294831399',
      recordCount: 2,
      selected: false,
    },
    {
      facetName: "10'",
      facetId: '4294831786',
      recordCount: 6,
      selected: true,
    },
    {
      facetName: "100'",
      facetId: '4294831754',
      recordCount: 39,
      selected: false,
    },
    {
      facetName: '11"',
      facetId: '4294830445',
      recordCount: 2,
      selected: true,
    },
  ],
};
