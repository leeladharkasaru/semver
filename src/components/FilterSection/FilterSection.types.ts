import { DropDownListRadius } from '../DropDownList/DropDownList.types';
import { MultiSelectDropDownListStyle } from '../MultiSelectDropdown/MultiSelectDropdown.types';

export interface FilterSectionProps {
  data: FilterSourceDataProps;
  valueSeparator?: string;
  getCombinedFilters?: boolean;
  onChange: (filterPayload: FilterPayloadProps | string) => void;
  multiSelectFilterProps?: MultiSelectFilterProps;
}

export interface MultiSelectFilterProps {
  itemTextKey?: string;
  itemValueKey?: string;
  itemSelectedKey?: string;
  formControlClassName?: string;
  inputLableClassName?: string;
  selectClassName?: string;
  menuItemClassName?: string;
  checkboxClassName?: string;
  listItemClassName?: string;
  valueSeparator?: string;
  separatorPreFix?: string;
  separatorPostFix?: string;
  ellipsisPostSelectedItems?: number;
  radius?: DropDownListRadius;
  dropDownListStyle?: MultiSelectDropDownListStyle;
  variant?: "standard" | "outlined" | "filled";
}

export interface FilterSourceDataProps {
  [key: string]: any[];
}

export interface FilterPayloadProps {
  [key: string]: string;
}
