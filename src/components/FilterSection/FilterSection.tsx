import { Grid } from '@material-ui/core';
import React, { useState } from 'react';
import MultiSelectDropdown from '../MultiSelectDropdown/MultiSelectDropdown';
import { FilterPayloadProps, FilterSectionProps } from './FilterSection.types';

const FilterSection: React.FC<FilterSectionProps> = ({
  data,
  getCombinedFilters,
  onChange,
  multiSelectFilterProps = {
    valueSeparator: ',',
  },
}) => {
  const [filterData, setFilterData] = useState<FilterPayloadProps>({});
  const { valueSeparator } = multiSelectFilterProps;
  const handleChange = (selectedValues: string[], name: string) => {
    const updatedFilterData = {
      ...filterData,
      [name]: selectedValues.join(valueSeparator),
    };
    setFilterData(updatedFilterData);

    if (getCombinedFilters) {
      const filtersArray = Object.keys(updatedFilterData).map(
        (key: string) => updatedFilterData[key],
      );
      const filtersString = filtersArray.join(valueSeparator);
      onChange(filtersString);
    } else {
      onChange(updatedFilterData);
    }
  };

  return (
    <Grid container spacing={2}>
      {Object.keys(data).map((filterName: string) => {
        const filterOptions: any[] = data[filterName];
        const fieldName = filterName
          .substring(0, 1)
          .toLowerCase()
          .concat(filterName.substring(1));

        return (
          <Grid key={filterName} item xs={6} sm={6} md={4} lg={3}>
            <MultiSelectDropdown
              name={fieldName}
              label={filterName}
              items={filterOptions}
              onChange={handleChange}
              fullWidth
              {...multiSelectFilterProps}
            />
          </Grid>
        );
      })}
    </Grid>
  );
};

export default FilterSection;
