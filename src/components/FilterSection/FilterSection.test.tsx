import { render } from "@testing-library/react";
import React from "react";
import FilterSection from "./FilterSection";
import { FilterSectionProps } from "./FilterSection.types";
import { FILTER_DATA } from "./data";

describe("FilterSection Component", () => {
  const props: FilterSectionProps = {
    data: FILTER_DATA,
    onChange: jest.fn(),
    multiSelectFilterProps: {
      itemTextKey: "facetName",
      itemValueKey: "facetId",
      variant: "outlined",
    },
  };

  const renderComponent = () => render(<FilterSection {...props} />);
  const firstFilterName = Object.keys(props.data).find((key) => key);

  it("it should render FilterSection", () => {
    const { getAllByText } = renderComponent();
    expect(getAllByText(firstFilterName as string)[0]).toBeDefined();
  });
});
