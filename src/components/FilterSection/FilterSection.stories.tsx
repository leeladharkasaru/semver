import { ComponentMeta } from '@storybook/react';
import React from 'react';
import FilterSection from './FilterSection';
import { FILTER_DATA, PRE_SELECTED_FILTERS_DATA } from './data.ts';
import {
  FilterPayloadProps,
  FilterSectionProps,
} from './FilterSection.types.ts';

const meta: ComponentMeta<typeof FilterSection> = {
  component: FilterSection,
  title: 'Components/FilterSection',
  argTypes: {},
};
export default meta;

// Handle the on change to get the selected values and the field name
const handleChange = (filterPayload: FilterPayloadProps | string) => {
  console.log('filterPayload', filterPayload);
};

// Show with default features
export const Default = (args: FilterSectionProps) => (
  <FilterSection {...args} />
);
const defaultProps: FilterSectionProps = {
  data: FILTER_DATA,
  onChange: handleChange,
  multiSelectFilterProps: {
    itemTextKey: 'facetName',
    itemValueKey: 'facetId',
    variant: 'outlined',
  },
};
Default.args = defaultProps;

// Get combined filter values of all the filters.
export const GetCombinedFilters = (args: FilterSectionProps) => (
  <FilterSection {...args} />
);
const combinedFiltersProps: FilterSectionProps = {
  data: FILTER_DATA,
  getCombinedFilters: true,
  onChange: handleChange,
  multiSelectFilterProps: {
    itemTextKey: 'facetName',
    itemValueKey: 'facetId',
    variant: 'outlined',
  },
};
GetCombinedFilters.args = combinedFiltersProps;

// Show with pre selected filters.
export const PreSelectedFilters = (args: FilterSectionProps) => (
  <FilterSection {...args} />
);
const preSelectedFilterProps: FilterSectionProps = {
  data: PRE_SELECTED_FILTERS_DATA,
  onChange: handleChange,
  multiSelectFilterProps: {
    itemTextKey: 'facetName',
    itemValueKey: 'facetId',
    itemSelectedKey: 'selected',
    variant: 'outlined',
  },
};
PreSelectedFilters.args = preSelectedFilterProps;

// Show with ellipsis after the provided numbers of the selected data.
export const ShowEllipsis = (args: FilterSectionProps) => (
  <FilterSection {...args} />
);
const ellipsisProps: FilterSectionProps = {
  data: FILTER_DATA,
  onChange: handleChange,
  multiSelectFilterProps: {
    itemTextKey: 'facetName',
    itemValueKey: 'facetId',
    variant: 'outlined',
    ellipsisPostSelectedItems: 2,
  },
};
ShowEllipsis.args = ellipsisProps;

// Show with Pipe sign ("|") as a value separator that will be used to join
// the selected values and the display text relative to the selected values
export const PipeSignAsSeparator = (args: FilterSectionProps) => (
  <FilterSection {...args} />
);
const pieSignProps: FilterSectionProps = {
  data: FILTER_DATA,
  onChange: handleChange,
  multiSelectFilterProps: {
    itemTextKey: 'facetName',
    itemValueKey: 'facetId',
    variant: 'outlined',
    valueSeparator: '|',
    separatorPreFix: ' ',
    separatorPostFix: ' ',
  },
};
PipeSignAsSeparator.args = pieSignProps;
