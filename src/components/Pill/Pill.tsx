import './Pill.scss';
import React from 'react';
import { Box } from '@material-ui/core';
import { PillProps } from './Pill.types';

const interalClassName = 'bcn-pill';

const Pill: React.FC<PillProps> = ({
  children,
  className,
  color = 'primary',
  pillStyle,
}) => (
  <Box
    className={`${interalClassName} ${interalClassName}--${color} ${className}`}
    style={pillStyle?.container}
  >
    {children}
  </Box>
);

export default Pill;
