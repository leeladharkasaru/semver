import Pill from './Pill';

export type {
  PillProps,
  PillColor,
  PillStyle,
} from './Pill.types';
export { Pill };
