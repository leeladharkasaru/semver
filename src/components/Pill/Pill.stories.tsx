import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import Pill from './Pill';

const meta: Meta<typeof Pill> = {
  component: Pill,
  title: 'Components/Pill',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Pill>;

export const Default: ComponentStory = (args: any) => (
  <Pill {...args} />
);
Default.args = {
  children: 'Primary',
  color: 'primary',
};

export const OverrideStyles: ComponentStory = (args: any) => (
  <Pill {...args} />
);
OverrideStyles.args = {
  children: 'Test',
  color: 'primary',
  pillStyle: {
    container: {
      color: 'red',
    },
  },
};
