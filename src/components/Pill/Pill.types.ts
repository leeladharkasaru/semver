export interface PillProps {
    className?: string;
    color?: PillColor;
    pillStyle?: PillStyle;
}

export type PillColor = "primary";

export type PillStyle = {
    container?: Object;
};
