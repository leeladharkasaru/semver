import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import React from 'react';
import Pill from './Pill';
import { PillProps } from './Pill.types';

describe('Pill Component', () => {
  let props: PillProps;

  const renderComponent = () => render(<Pill {...props} />);

  beforeEach(() => {
    props = {
      className: 'test-class',
      color: 'primary',
      pillStyle: {
        container: {
          backgroundColor: 'red',
        },
      },
      children: 'Test Pill',
    } as any;
  });

  it('should render the pill with the correct text', () => {
    const { getByText } = renderComponent();
    expect(getByText('Test Pill')).toBeInTheDocument();
  });

  it('should render the pill with primary and test class', () => {
    const { container } = renderComponent();
    expect(container.firstChild).toHaveClass('bcn-pill');
    expect(container.firstChild).toHaveClass('bcn-pill--primary');
    expect(container.firstChild).toHaveClass('test-class');
  });

  it('should have the  red background color style', () => {
    const { container } = renderComponent();
    expect(container.firstChild).toHaveStyle('background-color: red');
  });
});
