import { SelectProps } from '@material-ui/core';

export interface DropDownListProps {
    className?: string;
    selectId?: string;
    labelId?: string;
    items?: Array<DropDownListItem>;
    value?: string | number;
    defaultValue?: string | number;
    label?: string;
    displayEmpty?: boolean;
    radius?: DropDownListRadius;
    fullWidth?: boolean;
    disabled?: boolean;
    testId?: string;
    dropDownListStyle?: DropDownListStyle;
    onChange?: (value: string) => void;
    renderValue?: (value: SelectProps['value']) => React.ReactNode;
}

export type DropDownListItem = {
    value: string | number;
    label: string;
};

export type DropDownListRadius = "normal" | "sharp";

export type DropDownListStyle = {
    container?: Object;
    select?: Object;
    menuItem?: Object;
};
