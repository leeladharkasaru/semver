import "@testing-library/jest-dom";
import { render, fireEvent } from "@testing-library/react";
import React from "react";
import DropDownList from "./DropDownList";
import { DropDownListProps } from "./DropDownList.types";

describe("DropDownList Component", () => {
    let props: DropDownListProps;

    const renderComponent = () => render(<DropDownList {...props} />);

    beforeEach(() => {
        props = {
            selectId: "testSelect",
            testId: "testSelect",
            items: [
                { value: "1", label: "Item 1" },
                { value: "2", label: "Item 2" },
            ],
            defaultValue: "",
            disabled: false,
            label: "Test Label",
            onChange: jest.fn(),
        };
    });

    it("renders correctly", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("testSelect")).toBeInTheDocument();
    });

    it("calls onChange when a new value is selected", () => {
        // Act
        const { getByTestId } = renderComponent();
        const input = getByTestId("testSelect").querySelector("input") as HTMLInputElement;
        fireEvent.change(input, { target: { value: "2" } });

        // Assert
        expect(props.onChange).toHaveBeenCalledWith("2");
    });

    it("applies 'is-empty' class when no value is selected", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("testSelect")).toHaveClass("bcn-dropdownlist--empty");
    });

    it("applies styles from dropDownListStyle prop", async () => {
        // Arrange
        props.dropDownListStyle = {
            container: { backgroundColor: 'red' },
            select: { backgroundColor: 'blue' },
        };

        // Act
        const { getByTestId } = renderComponent();
    
        // Assert
        const select = getByTestId("testSelect");
        const container = select.parentElement;
        expect(container).toHaveStyle('background-color: red');
        expect(select).toHaveStyle('background-color: blue');
    });

    it("applies custom render value", async () => {
        // Arrange
        props.defaultValue = "1";
        props.renderValue = jest.fn();

        // Act
        renderComponent();
    
        // Assert
        expect(props.renderValue).toHaveBeenCalledWith("1");
    });

    it("renders as disabled", async () => {
        // Arrange
        props.disabled = true;

        // Act
        const { getByTestId } = renderComponent();

        // Assert
        expect(getByTestId("testSelect")).toHaveClass("Mui-disabled");
    });
});
