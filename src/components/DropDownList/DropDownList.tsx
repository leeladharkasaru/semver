import './DropDownList.scss';
import React from 'react';
import { FormControl, Select, MenuItem, InputLabel } from '@material-ui/core';
import { KeyboardArrowDown, KeyboardArrowUp } from '@material-ui/icons';
import { DropDownListProps } from './DropDownList.types';

const internalClassName = 'bcn-dropdownlist';

const DropDownList: React.FC<DropDownListProps> = ({
  className = '',
  selectId,
  labelId,
  items,
  value,
  defaultValue,
  label,
  radius = 'normal',
  fullWidth = false,
  disabled = false,
  testId,
  dropDownListStyle,
  onChange,
  renderValue
}) => {
  // Is the value empty?
  const isEmpty =
    (value === undefined || value === '') &&
    (defaultValue === undefined || defaultValue === '');

  // Handle the render value internally
  const handleRenderValue = (val: any) => {
    // If a custom render value is provided, use it
    if (renderValue) {
      return renderValue(val);
    }

    // Use the label if it exists
    const labelValue = items?.find((item) => item.value === value)?.label;
    return labelValue || value;
  };

  // Trigger the onChange event
  const triggerOnChange = (triggerValue: string) => {
    if (onChange) {
      onChange(triggerValue);
    }
  };
  // Handle the change event
  const handleOnChange = (event: React.ChangeEvent<{ value: string }>) => {
    const newValue = event.target.value;
    triggerOnChange(newValue.toString());
  };

  // Render the icon for the dropdown
  // eslint-disable-next-line no-shadow
  const IconComponent = ({ className }: any) => {
    const iconClassName = `${internalClassName}--icon ${className}`;
    const isOpen = className.includes('MuiSelect-iconOpen');
    return isOpen ? (
      <KeyboardArrowUp className={iconClassName} />
    ) : (
      <KeyboardArrowDown className={iconClassName} />
    );
  };

  return (
    <FormControl
      variant="outlined"
      fullWidth={fullWidth}
      style={dropDownListStyle?.container}>
      {label && (
        <InputLabel
          id={labelId}
          className={`${internalClassName}__label ${
            disabled ? `${internalClassName}__label--disabled` : ''
          }`}>
          {label}
        </InputLabel>
      )}
      <Select
        id={selectId}
        label={label}
        labelId={labelId}
        data-testid={testId}
        className={`${className} ${internalClassName} ${internalClassName}--radius-${radius} ${
          isEmpty ? `${internalClassName}--empty` : ''
        }`}
        value={value}
        defaultValue={defaultValue}
        disabled={disabled}
        style={dropDownListStyle?.select}
        IconComponent={IconComponent}
        onChange={handleOnChange}
        renderValue={handleRenderValue}>
        {items &&
          items.map((item, index) => (
            <MenuItem
              key={index}
              value={item.value}
              style={dropDownListStyle?.menuItem}>
              {item.label}
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  );
};

export default DropDownList;
