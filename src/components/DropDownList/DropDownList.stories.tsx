import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import DropDownList from './DropDownList';

const meta: Meta<typeof DropDownList> = {
  component: DropDownList,
  title: 'Components/DropDownList',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof DropDownList>;

export const Default: ComponentStory = (args: any) => (
  <DropDownList {...args} />
);
Default.args = {
  selectId: 'age',
  items: [
    { value: '', label: 'Select Age' },
    { value: 10, label: 'Ten' },
    { value: 20, label: 'Twenty' },
    { value: 30, label: 'Thirty' },
  ],
  label: 'Age',
  testId: 'test-drop-down-list',
  dropDownListStyle: {
    container: {
      minWidth: '200px',
    },
  },
  onChange: (value: string) => {
    console.log(value);
  },
};

export const RadiusSharp: ComponentStory = (args: any) => (
  <DropDownList {...args} />
);
RadiusSharp.args = {
  selectId: 'age',
  items: [
    { value: 10, label: 'Ten' },
    { value: 20, label: 'Twenty' },
    { value: 30, label: 'Thirty' },
  ],
  defaultValue: 10,
  radius: 'sharp',
  testId: 'test-drop-down-list',
  onChange: (value: string) => {
    console.log(value);
  },
};

export const Disabled: ComponentStory = (args: any) => (
  <DropDownList {...args} />
);
Disabled.args = {
  selectId: 'age',
  disabled: true,
  defaultValue: 10,
  items: [
    { value: '', label: 'Select Age' },
    { value: 10, label: 'Ten' },
    { value: 20, label: 'Twenty' },
    { value: 30, label: 'Thirty' },
  ],
  label: 'Age',
  testId: 'test-drop-down-list',
  dropDownListStyle: {
    container: {
      minWidth: '200px',
    },
  },
  onChange: (value: string) => {
    console.log(value);
  },
};

export const Override: ComponentStory = (args: any) => (
  <DropDownList {...args} />
);
Override.args = {
  selectId: 'age',
  items: [
    { value: 10, label: 'Ten' },
    { value: 20, label: 'Twenty' },
    { value: 30, label: 'Thirty' },
  ],
  defaultValue: 10,
  testId: 'test-drop-down-list',
  onChange: (value: string) => {
    console.log(value);
  },
  dropDownListStyle: {
    container: { border: 'solid 2px red' },
    select: { backgroundColor: 'blue' },
    menuItem: { backgroundColor: 'green' },
  },
};
