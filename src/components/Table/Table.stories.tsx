import {
  Box,
  Button,
  Input,
  MenuItem,
  Select,
  TableCell,
  Typography,
} from '@material-ui/core';
import { ComponentStoryObj, Meta } from '@storybook/react';
import React, { ChangeEvent, useState } from 'react';
import { default as BCNTable, default as Table } from './Table';
import { Row } from './Table.types';

const meta: Meta<typeof Table> = {
  component: Table,
  title: 'Components/Table',
  argTypes: {},
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Table>;

const standardQuoteItems = [
  {
    unitPrice: 67.75,
    itemNumber: '407262',
    itemType: 'I',
    mincronDisplayName: '',
    quantity: 1,
    color: null,
    productId: 'C-349425',
    unitOfMeasure: 'BDL',
    displayName:
      'Atlas Roofing StormMaster&reg; Slate Shingles with Scotchgard&trade; Protector Weathered Slate',
    itemTotalPrice: 67.75,
    imageOnErrorUrl: '/images/large/brand/atlas_roofing_brand_thumb.jpg',
    currencySymbol: '$',
    productNumber: 'ATLSMSLSPWE',
    formatItemTotalPrice: '67.75',
    bodyComments: null,
    PDPUrl: '/productDetail/C-349425?skuId=407262&Color=Weathered+Slate',
    deleteStatus: null,
    formatUnitPrice: '67.75',
    imageURL: '/images/large/407262_default_thumb.jpg',
    stickerImageURL: null,
    id: null,
    itemDescription: 'ATL AR SMASTER SLATE WEATHERED',
    dimensions: null,
  },
  {
    unitPrice: 26.66,
    itemNumber: '428681',
    itemType: 'I',
    mincronDisplayName: '',
    quantity: 1,
    color: null,
    productId: 'C-428681',
    unitOfMeasure: 'BDL',
    displayName: 'IKO Cambridge&reg; Shingles Aged Redwood',
    itemTotalPrice: 26.66,
    imageOnErrorUrl: '/images/large/brand/iko_brand_thumb.jpg',
    currencySymbol: '$',
    productNumber: 'IKOCAMAR',
    formatItemTotalPrice: '26.66',
    bodyComments: null,
    PDPUrl: '/productDetail/C-428681?skuId=428681&Color=Aged+Redwood',
    deleteStatus: null,
    formatUnitPrice: '26.66',
    imageURL: '/images/large/428681_default_thumb.jpg',
    stickerImageURL: null,
    id: null,
    itemDescription: 'IKO+CAMBRIDGE AR AGED REDWOD',
    dimensions: null,
  },
];
export const OrderItemTable: ComponentStory = (args) => <BCNTable {...args} />;

interface ItemProps {
  Product?: any;
  Details?: any;
  Unit?: any;
  Quantity?: any;
  Action?: any;
}
OrderItemTable.args = {
  columns: [
    { header: 'Product', type: 'image' },
    { header: 'Details' },
    { header: 'Unit Price' },
    {
      header: 'Order Qty',
      columnType: 'select',
      options: ['Order Qty', 'Shipped Qty'],
    },
    { header: 'Sub Total' },
  ],
  meta: {
    cell: ({
      value, style, type, options,
    }: any) => {
      const [changeValue, setValue] = useState(value);
      const changehandler = (e: any) => {
        setValue(e.target.value);
      };
      return (
        <TableCell style={style}>
          {type === 'button' ? (
            <Button>{value}</Button>
          ) : type === 'text' ? (
            <Input value={changeValue} onChange={changehandler} />
          ) : type === 'image' ? (
            <img src={value} style={style} />
          ) : type === 'select' ? (
            <Select
              variant="outlined"
              style={{
                ...style,
                border: '1px solid #5F6369',
              }}
              value={changeValue}
              onChange={changehandler}
            >
              {options?.map((val, index) => (
                <MenuItem key={index} value={val}>
                  {val}
                </MenuItem>
              ))}
            </Select>
          ) : (
            value
          )}
        </TableCell>
      );
    },
  },
  rows: standardQuoteItems.map((item, index) => {
    const itemData = {
      Product: `https://beaconproplus.com${item.imageURL}`,
      Details: (
        <Box>
          <a
            style={{
              color: '#285D99',
              fontFamily: '',
              fontSize: '1rem',
              fontWeight: 400,
            }}
          >
            {item.displayName}
          </a>
          <Box display="flex">
            <Typography
              style={{
                fontFamily: 'Proxima Nova,arial,sans-serif',
                fontSize: '1rem',
                fontWeight: 400,
                color: '#000000',
              }}
            >
              Item#:&nbsp;
            </Typography>
            <Typography
              style={{
                fontFamily: 'Proxima Nova,arial,sans-serif',
                fontSize: '1rem',
                fontWeight: 400,
                color: '#000000',
              }}
            >
              {item.itemNumber}
            </Typography>
          </Box>
        </Box>
      ),
      'Unit Price': item.unitOfMeasure,
      'Order Qty': item.quantity,
      'Sub Total': `$${item.formatItemTotalPrice}`,
    };
    const row = { cellData: itemData };
    return row;
  }),
  columnStyles: [
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      lineHeight: '1rem',
      fontWeight: 400,
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      lineHeight: '1rem',
      fontWeight: 400,
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      lineHeight: '1rem',
      fontWeight: 400,
      width: '15%',
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 400,
      width: '9.5rem',
      height: '3rem',
    },
    {
      color: '#5F6369',
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
      fontWeight: 400,
      width: '15%',
    },
  ],
  rowStyle: [
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      fontSize: '1rem',
    },
  ],
};

export const MobileTableView: ComponentStory = (args) => <BCNTable {...args} />;

MobileTableView.args = {
  isMobile: true,
  meta: {
    cell: ({ value, style, type }: any) => {
      const [changeValue, setValue] = useState(value);
      const changehandler = (e: ChangeEvent<HTMLInputElement>) => {
        setValue(e.target.value);
      };
      return (
        <TableCell style={style}>
          {type === 'box' ? (
            <Box>{value}</Box>
          ) : type === 'image' ? (
            <img src={value} width="77px" height="77px" />
          ) : (
            value
          )}
        </TableCell>
      );
    },
  },
  columns: [
    { header: 'Product', type: 'image' },
    { header: 'Details', type: 'box' },
  ],
  rows: standardQuoteItems.map((item, index) => {
    const box = (
      <Box>
        <Typography>{item.displayName}</Typography>
        <Typography>
          Item:
          {item.itemNumber}
        </Typography>
        <Typography>
          Product Number:
          {item.productNumber}
        </Typography>
        <Typography>
          Unit:
          {item.unitOfMeasure}
        </Typography>
        <Typography>
          Quantity:
          {item.quantity}
        </Typography>
      </Box>
    );
    const itemData: ItemProps = {
      Product: `https://beaconproplus.com${item.imageURL}`,
      Details: box,
    };
    const row: Row<ItemProps> = { cellData: itemData };
    return row;
  }),

  rowStyle: [
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      verticalAlign: 'top',
    },
    {
      fontFamily: 'Proxima Nova,arial,sans-serif',
      verticalAlign: 'top',
    },
  ],
};
