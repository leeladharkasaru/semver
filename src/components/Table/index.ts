import BCNTable from './Table';

export type {
  TableProps, TableMeta, Column, Row,
} from './Table.types';
export { BCNTable };
