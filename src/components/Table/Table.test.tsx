import {
  Box,
  Button,
  Input,
  MenuItem,
  Select,
  TableCell,
  Typography,
} from '@material-ui/core';
import { fireEvent, render } from '@testing-library/react';
import React, { useState } from 'react';
import Table from './Table';
import { TableProps } from './Table.types';
import { standardQuoteItems } from '../../mocks/quoteItems';

describe('Test Component', () => {
  const props: TableProps = {
    columns: [
      { header: 'Product', type: 'image' },
      { header: 'Details' },
      { header: 'Unit' },
      { header: 'Quantity', type: 'text' },
      { header: 'Action', type: 'button' },
    ],

    rows: standardQuoteItems.map((item) => {
      const itemData = {
        Product: `https://beaconproplus.com${item.imageURL}`,
        Details: item.displayName,
        Unit: item.unitOfMeasure,
        Quantity: item.quantity,
        Action: 'Delete',
      };
      const row = { cellData: itemData };
      return row;
    }),
    columnStyles: [
      {
        color: 'rgba(0,0,0,.54)',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        width: 'fit-content',
        fontSize: '1rem',
        fontWeight: 600,
      },
      {
        color: 'rgba(0,0,0,.54)',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        fontWeight: 600,
      },
      {
        color: 'rgba(0,0,0,.54)',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        fontWeight: 600,
      },
      {
        color: 'rgba(0,0,0,.54)',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        fontWeight: 600,
      },
      {
        color: 'rgba(0,0,0,.54)',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        fontWeight: 600,
      },
    ],
    rowStyle: [
      {
        fontFamily: 'Proxima Nova,arial,sans-serif',
      },
      {
        fontFamily: 'Proxima Nova,arial,sans-serif',
      },
    ],
  };
  const props1: TableProps = {
    columns: [
      { header: 'Product', type: 'image' },
      { header: 'Details' },
      { header: 'Unit' },
      { header: 'Quantity', columnType: 'text' },
      { header: 'Action', type: 'button' },
    ],

    rows: standardQuoteItems.map((item) => {
      const itemData = {
        Product: `https://beaconproplus.com${item.imageURL}`,
        Details: item.displayName,
        Unit: item.unitOfMeasure,
        Quantity: item.quantity,
        Action: 'Delete',
      };
      const row = { cellData: itemData };
      return row;
    }),
    columnStyles: [
      {
        color: 'rgba(0,0,0,.54)',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        width: 'fit-content',
        fontSize: '1rem',
        fontWeight: 600,
      },
      {
        color: 'rgba(0,0,0,.54)',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        fontWeight: 600,
      },
      {
        color: 'rgba(0,0,0,.54)',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        fontWeight: 600,
      },
      {
        color: 'rgba(0,0,0,.54)',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        fontWeight: 600,
      },
      {
        color: 'rgba(0,0,0,.54)',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        fontWeight: 600,
      },
    ],
    rowStyle: [
      {
        fontFamily: 'Proxima Nova,arial,sans-serif',
      },
      {
        fontFamily: 'Proxima Nova,arial,sans-serif',
      },
    ],
  };
  const propsWithMeta: TableProps = {
    columns: [
      { header: 'Product', type: 'image' },
      { header: 'Details' },
      { header: 'Unit Price' },
      {
        header: 'Order Qty',
        columnType: 'select',
        options: ['Order Qty', 'Shipped Qty'],
      },
      { header: 'Sub Total' },
    ],
    meta: {
      cell: ({ value, style, type, options }: any) => {
        const [changeValue, setValue] = useState(value);
        const changehandler = (e: any) => {
          setValue(e.target.value);
        };
        return (
          <TableCell style={style}>
            {type === 'button' ? (
              <Button>{value}</Button>
            ) : type === 'text' ? (
              <Input value={changeValue} onChange={changehandler} />
            ) : type === 'image' ? (
              <img src={value} style={style} />
            ) : type === 'select' ? (
              <Select
                variant="outlined"
                style={{
                  ...style,
                  border: '1px solid #5F6369',
                  '.MuiSelect-outlined': {
                    border: '2px solid red',
                  },
                  root: {
                    background: 'red',
                  },
                  '.MenuItem.selected': {
                    color: 'red',
                  },
                }}
                value={changeValue}
                onChange={changehandler}
              >
                {options?.map((val: any, index: number) => (
                  <MenuItem key={index} value={val}>
                    {val}
                  </MenuItem>
                ))}
              </Select>
            ) : (
              value
            )}
          </TableCell>
        );
      },
    },
    rows: standardQuoteItems.map((item, index) => {
      const itemData = {
        Product: `https://beaconproplus.com${item.imageURL}`,
        Details: (
          <Box>
            <a
              style={{
                color: '#285D99',
                fontFamily: '',
                fontSize: '1rem',
                fontWeight: 400,
              }}
            >
              {item.displayName}
            </a>
            <Box display="flex">
              <Typography
                style={{
                  fontFamily: 'Proxima Nova,arial,sans-serif',
                  fontSize: '1rem',
                  fontWeight: 400,
                  color: '#000000',
                }}
              >
                Item#:
              </Typography>
              <Typography
                style={{
                  fontFamily: 'Proxima Nova,arial,sans-serif',
                  fontSize: '1rem',
                  fontWeight: 400,
                  color: '#000000',
                }}
              >
                {item.itemNumber}
              </Typography>
            </Box>
          </Box>
        ),
        'Unit Price': item.unitOfMeasure,
        'Order Qty': item.quantity,
        'Sub Total': `$${item.formatItemTotalPrice}`,
      };
      const row = { cellData: itemData };
      return row;
    }),
    columnStyles: [
      {
        color: '#5F6369',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        lineHeight: '1rem',
        fontWeight: 400,
      },
      {
        color: '#5F6369',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        lineHeight: '1rem',
        fontWeight: 400,
      },
      {
        color: '#5F6369',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        lineHeight: '1rem',
        fontWeight: 400,
        width: '15%',
      },
      {
        color: '#5F6369',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        fontWeight: 400,
        width: '9.5rem',
        height: '3rem',
      },
      {
        color: '#5F6369',
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
        fontWeight: 400,
        width: '15%',
      },
    ],
    rowStyle: [
      {
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
      },
      {
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
      },
      {
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
      },
      {
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
      },
      {
        fontFamily: 'Proxima Nova,arial,sans-serif',
        fontSize: '1rem',
      },
    ],
  };

  const renderComponent = () => render(<Table {...props} />);
  const renderComponent1 = () => render(<Table {...props1} />);
  const renderWithMetaComponent = () => render(<Table {...propsWithMeta} />);

  it('Render Table', () => {
    const { getByTestId, getAllByTestId } = renderComponent();
    const table = getByTestId('Table');
    expect(table).toBeDefined();
    const inputfeild = getAllByTestId('1-3');
    fireEvent.change(inputfeild[0], {
      target: {
        value: 'abc',
      },
    });
    expect(inputfeild[0].getAttribute('value')).toEqual('abc');
  });
  it('Render Table with Action Type COlumn', () => {
    const { getByTestId } = renderComponent1();
    const table = getByTestId('Table');
    expect(table).toBeDefined();
  });
  it('Render Table with Metadeta', () => {
    const { getByTestId } = renderWithMetaComponent();
    const table = getByTestId('Table');
    expect(table).toBeDefined();
  });
});
