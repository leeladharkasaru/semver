import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from '@material-ui/core';
import React, { ChangeEvent, useState } from 'react';
import { TableProps } from './Table.types';

const TabCell = ({ value, type, style, index }: any) => {
  const [changeValue, setValue] = useState(value);
  const changehandler = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };
  return (
    <TableCell style={style}>
      {type === 'button' ? (
        <Button>{value}</Button>
      ) : type === 'text' ? (
        <input
          type="text"
          value={changeValue}
          onChange={changehandler}
          data-testid={`${value}-${index}`}
        />
      ) : type === 'image' ? (
        <img src={value} alt="table-cell" style={style} />
      ) : (
        value
      )}
    </TableCell>
  );
};

const BCNTable: React.FC<TableProps> = ({
  columns,
  rows,
  isMobile,
  columnStyles,
  rowStyle,
  meta
}) => (
  <Table data-testid="Table">
    <TableContainer>
      {columns && !isMobile && (
        <TableHead>
          <TableRow>
            {columns.map((column, index) =>
              column.columnType ? (
                meta?.cell({
                  value: column.header,
                  style: columnStyles && columnStyles[index],
                  type: column.columnType,
                  options: column.options
                })
              ) : (
                <TabCell
                  key={`table-cell-${index}`}
                  value={column.header}
                  style={columnStyles && columnStyles[index]}
                />
              )
            )}
          </TableRow>
        </TableHead>
      )}
      {rows && (
        <TableBody>
          {rows.map((cell, rowIndex) => (
            <TableRow
              key={rowIndex}
              data-testid={`row-${rowIndex}`}
              style={rowStyle && rowStyle[rowIndex]}>
              {columns &&
                columns.map((column, index) => {
                  if (!meta) {
                    return (
                      <TabCell
                        key={index}
                        index={index}
                        value={cell.cellData[column.header]}
                        type={column.type}
                        style={rowStyle && rowStyle[rowIndex]}
                      />
                    );
                  }
                  return (
                    cell &&
                    cell.cellData &&
                    meta.cell({
                      value: cell.cellData[column.header],
                      style: rowStyle && rowStyle[index],
                      type: column.type,
                      index: rowIndex
                    })
                  );
                })}
            </TableRow>
          ))}
        </TableBody>
      )}
    </TableContainer>
  </Table>
);

export default BCNTable;
