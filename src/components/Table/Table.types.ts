import React from 'react';

export type TableProps = {
  columns?: Array<Column>;
  rows?: Array<Row<any>>;
  columnStyles?: Array<Object>;
  rowStyle?: Array<Object>;
  meta?: TableMeta;
  isMobile?: boolean;
};

export type TableMeta = {
  cell: React.FunctionComponent<any>;
};

export type Column = {
  header: any;
  type?: string;
  style?: Object;
  columnType?: string;
  options?: any;
};
export type Row<T> = {
  cellData: T;
};
