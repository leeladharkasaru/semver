import './Button2.scss';
import React from 'react';
import { Button } from '@material-ui/core';
import { Button2Props } from './Button2.types';

const internalClassName = 'bcn-button';

const Button2: React.FC<Button2Props> = ({
  children,
  className = '',
  type = 'button',
  disabled = false,
  variant = 'contained',
  size = 'medium',
  color = 'primary',
  testId,
  button2Style,
  onClick,
}) => (
  <Button
    color="default"
    type={type}
    className={`${internalClassName} ${internalClassName}--${color} ${className}`}
    disabled={disabled}
    variant={variant}
    size={size}
    data-testid={testId}
    style={button2Style?.button}
    onClick={onClick}
  >
    {children}
  </Button>
);

export default Button2;
