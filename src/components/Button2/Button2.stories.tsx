import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import Button2 from './Button2';

const meta: Meta<typeof Button2> = {
  component: Button2,
  title: 'Components/Button2',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof Button2>;

export const Default: ComponentStory = (args: any) => (
  <Button2 {...args} />
);
Default.args = {
  children: 'Button',
  type: 'button',
  disabled: false,
  variant: 'contained',
  size: 'medium',
  color: 'primary',
  testId: 'test-button',
  onClick: () => console.log('Button clicked'),
};

export const Outlined: ComponentStory = (args: any) => (
  <Button2 {...args} />
);
Outlined.args = {
  children: 'Button',
  type: 'button',
  disabled: false,
  variant: 'outlined',
  size: 'medium',
  color: 'primary',
  testId: 'test-button',
  onClick: () => console.log('Button clicked'),
};

export const Small: ComponentStory = (args: any) => (
  <Button2 {...args} />
);
Small.args = {
  children: 'Button',
  type: 'button',
  disabled: false,
  variant: 'contained',
  size: 'small',
  color: 'primary',
  testId: 'test-button',
  onClick: () => console.log('Button clicked'),
};

export const OverrideStyles: ComponentStory = (args: any) => (
  <Button2 {...args} />
);
OverrideStyles.args = {
  children: 'Button',
  type: 'button',
  disabled: false,
  variant: 'contained',
  size: 'medium',
  color: 'primary',
  testId: 'test-button',
  onClick: () => console.log('Button clicked'),
  button2Style: {
    button: {
      backgroundColor: 'red',
    },
  },
};
