import '@testing-library/jest-dom';
import { render, fireEvent } from '@testing-library/react';
import React from 'react';
import Button2 from './Button2';
import { Button2Props } from './Button2.types';

describe('BeaconButton Component', () => {
  let props: Button2Props;

  const renderComponent = () => render(<Button2 {...props}>Click me</Button2>);

  beforeEach(() => {
    props = {
      type: 'button',
      disabled: false,
      variant: 'contained',
      size: 'medium',
      color: 'primary',
      testId: 'button',
      onClick: jest.fn(),
      button2Style: { button: {} },
    };
  });

  it('renders the button', () => {
    const { getByTestId } = renderComponent();
    const button = getByTestId('button');
    expect(button).toBeInTheDocument();
  });

  it('handles click events', () => {
    const { getByTestId } = renderComponent();
    const button = getByTestId('button');
    fireEvent.click(button);
    expect(props.onClick).toHaveBeenCalled();
  });

  it('displays the correct text', () => {
    const { getByTestId } = renderComponent();
    const button = getByTestId('button');
    expect(button).toHaveTextContent('Click me');
  });

  it('is disabled when the disabled prop is true', () => {
    props.disabled = true;
    const { getByTestId } = renderComponent();
    const button = getByTestId('button');
    expect(button).toBeDisabled();
  });

  it('has the correct type', () => {
    const { getByTestId } = renderComponent();
    const button = getByTestId('button');
    expect(button).toHaveAttribute('type', props.type);
  });
});
