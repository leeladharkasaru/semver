import Button2 from './Button2';

export type {
  Button2Props,
  Button2Style,
} from './Button2.types';
export { Button2 };
