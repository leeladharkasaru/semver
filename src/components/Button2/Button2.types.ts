import { MouseEventHandler } from 'react';

export interface Button2Props {
    className?: string;
    type?: "submit" | "reset" | "button" | undefined;
    disabled?: boolean;
    variant?: 'contained' | 'outlined';
    size?: 'small' | 'medium';
    color?: 'primary';
    testId?: string;
    button2Style?: Button2Style;
    onClick?: MouseEventHandler<HTMLButtonElement>;
}

export type Button2Style = {
    button?: Object;
};
