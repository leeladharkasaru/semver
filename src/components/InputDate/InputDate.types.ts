import { ChangeEventHandler } from 'react';

export interface InputDateProps {
    id?: string;
    testId?: string;
    className?: string;
    variant?: InputDateVariant;
    label?: string;
    error?: boolean;
    message?: string;
    disabled?: boolean;
    value?: string;
    defaultValue?: string;
    inputDateStyle?: InputDateStyle;
    onChange?: ChangeEventHandler<HTMLInputElement>;
}

export type InputDateVariant = "standard" | "outlined";

export type InputDateStyle = {
    container?: Object;
    label?: Object;
    dateField?: Object;
    message?: Object;
};
