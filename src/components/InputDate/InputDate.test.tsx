import "@testing-library/jest-dom";
import { render, fireEvent } from "@testing-library/react";
import React from "react";
import InputDate from "./InputDate";
import { InputDateProps } from "./InputDate.types";

describe("InputDate Component", () => {
    let props: InputDateProps;

    const renderComponent = () => render(<InputDate {...props} />);

    beforeEach(() => {
        props = {
            id: "test-id",
            testId: "test-id",
            className: "test-class",
            label: "Test Label",
            error: false,
            message: "Test Message",
            disabled: false,
            defaultValue: "2022-01-01",
            inputDateStyle: {
                container: {
                    backgroundColor: "red"
                },
                label: {
                    color: "blue"
                },
                dateField: {
                    border: "1px solid black"
                },
                message: {
                    color: "green"
                }
            },
            onChange: jest.fn()
        };
    });

    it("should render the input date with the correct label", () => {
        // Act
        const { getByText } = renderComponent();

        // Assert
        expect(getByText("Test Label")).toBeInTheDocument();
    });

    it("should render the input date with the correct message", () => {
        // Act
        const { getByText } = renderComponent();

        // Assert
        expect(getByText("Test Message")).toBeInTheDocument();
    });

    it("should have the correct class", () => {
        // Act
        const { container } = renderComponent();

        // Assert
        expect(container.firstChild).toHaveClass("bcn-input-date--container");
        expect(container.querySelector("input")).toHaveClass("MuiInputBase-input");
        expect(container.querySelector("input")).toHaveClass("MuiInput-input");
    });
    
    it("should have the correct style", () => {
        // Act
        const { container } = renderComponent();

        // Assert
        expect(container.firstChild).toHaveStyle("background-color: red");
        expect(container.querySelector("label")).toHaveStyle("color: blue");
        expect(container.querySelector("input")).not.toHaveStyle("border: 1px solid black");
        expect(container.querySelector("p")).toHaveStyle("color: green");
    });
    
    it("should call onChange when input value changes", () => {
        // Act
        const { getByTestId } = renderComponent();
        const input = getByTestId("test-id").querySelector('input') as HTMLInputElement;
        fireEvent.change(input, { target: { value: '2022-01-02' } });

        // Assert
        expect(props.onChange).toHaveBeenCalled();
    });
    
    it("should have the correct default value", () => {
        // Act
        const { getByTestId } = renderComponent();

        // Assert
        const input = getByTestId("test-id").querySelector('input') as HTMLInputElement;
        expect(input.value).toBe("2022-01-01");
    });
});
