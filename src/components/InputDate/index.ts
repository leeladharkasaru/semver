import InputDate from './InputDate';

export type {
  InputDateProps,
  InputDateStyle,
} from './InputDate.types';
export { InputDate };
