import React from 'react';
import { Meta, ComponentStoryObj } from '@storybook/react';
import InputDate from './InputDate';

const meta: Meta<typeof InputDate> = {
  component: InputDate,
  title: 'Components/InputDate',
};
export default meta;

type ComponentStory = ComponentStoryObj<typeof InputDate>;

export const Default: ComponentStory = (args: any) => (
  <InputDate {...args} />
);
Default.args = {
  id: 'date',
  testId: 'test-date',
  className: 'test',
  error: false,
  disabled: false,
  onChange: (e) => {
    console.log('Date changed', e);
  },
};

export const LabelAndMessage: ComponentStory = (args: any) => (
  <InputDate {...args} />
);
LabelAndMessage.args = {
  id: 'date',
  testId: 'test-date',
  className: 'test',
  label: 'Date',
  error: false,
  disabled: false,
  defaultValue: '2021-01-01',
  message: 'Enter a valid date',
  onChange: (e) => {
    console.log('Date changed', e);
  },
};

export const Disabled: ComponentStory = (args: any) => (
  <InputDate {...args} />
);
Disabled.args = {
  id: 'date',
  testId: 'test-date',
  className: 'test',
  label: 'Date',
  error: false,
  disabled: true,
  onChange: (e) => {
    console.log('Date changed', e);
  },
};

export const Error: ComponentStory = (args: any) => (
  <InputDate {...args} />
);
Error.args = {
  id: 'date',
  testId: 'test-date',
  className: 'test',
  label: 'Date',
  error: true,
  disabled: false,
  message: 'Invalid date',
  onChange: (e) => {
    console.log('Date changed', e);
  },
};

export const Outlined: ComponentStory = (args: any) => (
  <InputDate {...args} />
);
Outlined.args = {
  id: 'date',
  testId: 'test-date',
  className: 'test',
  error: false,
  disabled: false,
  variant: 'outlined',
  onChange: (e) => {
    console.log('Date changed', e);
  },
};

export const OutlinedAndLabel: ComponentStory = (args: any) => (
  <InputDate {...args} />
);
OutlinedAndLabel.args = {
  id: 'date',
  testId: 'test-date',
  className: 'test',
  error: false,
  disabled: false,
  variant: 'outlined',
  label: 'Date',
  onChange: (e) => {
    console.log('Date changed', e);
  },
};

export const OutlinedAndDisabled: ComponentStory = (args: any) => (
  <InputDate {...args} />
);
OutlinedAndDisabled.args = {
  id: 'date',
  testId: 'test-date',
  className: 'test',
  error: false,
  disabled: true,
  variant: 'outlined',
  label: 'Date',
  onChange: (e) => {
    console.log('Date changed', e);
  },
};

export const OverrideStyles: ComponentStory = (args: any) => (
  <InputDate {...args} />
);
OverrideStyles.args = {
  id: 'date',
  testId: 'test-date',
  className: 'test',
  label: 'Date',
  error: false,
  disabled: false,
  defaultValue: '2021-01-01',
  message: 'Enter a valid date',
  onChange: (e) => {
    console.log('Date changed', e);
  },
  inputDateStyle: {
    container: {
      border: '1px solid red',
      padding: '10px',
    },
    label: {
      color: 'blue',
    },
    dateField: {
      border: '1px solid green',
      padding: '10px',
    },
    message: {
      color: 'purple',
    },
  },
};
