import './InputDate.scss';
import React from 'react';
import {
  Box, FormHelperText, InputLabel, TextField,
} from '@material-ui/core';
import { InputDateProps } from './InputDate.types';

const internalClassName = 'bcn-input-date';

const InputDate: React.FC<InputDateProps> = ({
  id,
  testId,
  className = '',
  variant = 'standard',
  label,
  error,
  message,
  disabled,
  value,
  defaultValue,
  inputDateStyle,
  onChange,
}) => {
  const isVariantOutlined = variant === 'outlined';

  return (
    <Box
      className={`${internalClassName}--container ${className}`}
      style={inputDateStyle?.container}
    >
      {!isVariantOutlined && (
        <InputLabel
          className={`${internalClassName}--label`}
          style={inputDateStyle?.label}
        >
          {label}
        </InputLabel>
      )}
      <TextField
        type="date"
        id={id}
        data-testid={testId}
        value={value}
        variant={variant}
        defaultValue={defaultValue}
        className={`${internalClassName}--date-field`}
        disabled={disabled}
        error={error}
        label={isVariantOutlined ? label : undefined}
        style={inputDateStyle?.dateField}
        InputLabelProps={{
          shrink: !!(isVariantOutlined && label),
        }}
        onChange={onChange}
      />
      <FormHelperText
        className={`${internalClassName}--message`}
        error={error}
        style={inputDateStyle?.message}
      >
        {message}
      </FormHelperText>
    </Box>
  );
};

export default InputDate;
