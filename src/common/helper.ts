import { decode } from "html-entities";
import * as xlsx from "xlsx";
import {
  DOWNLOAD_FILE_NAME,
  DOWNLOAD_FILE_TYPE,
  DOWNLOAD_TYPE,
  ORDER_STATUS,
  WITH_PRICE_LABELS,
} from "./constants";
import {
  BranchAddress,
  LineItem,
  Order,
  OrderCSVItem,
  OrderHistoryPermissions,
  OrderResponse,
  OrderResult,
  OrderStatus,
} from "./models/order/OrderDetails";
import {
  Quote,
  QuoteCSVItem,
  QuoteGridItem,
  QuoteItem,
  StandardQuoteItem,
} from "./models/quote/QuoteDetails";

const BASE_URL =
  "https://dev-static.becn.digital/becn/rest/model/REST/NewRestService/v2/rest/com/becn";
const accountId = "280381";
const accountToken = "qIgveza0om3hnryaLVohd_qKd5SnnuCthD6ElsgdCbU=";

export interface ItemDetailsProps {
  productUrl: string;
  productDetails: string;
  unitPrice: string | number;
  quantity: number;
  subtotal?: string | number;
  unitOfMeasureDisplay: string;
  itemNumber?: string;
  productNumber?: string | null;
  price: string;
}

export type ItemDetailsOrderHistory = {
  productUrl: string;
  productDetails: string;
  unitPrice: string | number;
  quantity: number;
  subtotal: string | number;
  unitOfMeasureDisplay: string;
  itemNumber?: string;
};

export type ItemDetailsQuotePage = {
  productUrl: string;
  productDetails: string;
  quantity: number;
  unitOfMeasureDisplay: string;
  itemNumber: string;
  productNumber: string;
};

export const mapQuoteItemData = (
  quoteItems: QuoteItem[],
): ItemDetailsProps[] => {
  return quoteItems?.map((quoteItem: QuoteItem) => {
    const item: ItemDetailsProps = {
      productDetails: quoteItem.displayName,
      productUrl: quoteItem.imageURL,
      quantity: quoteItem.quantity,
      productNumber: quoteItem.productNumber,
      unitOfMeasureDisplay: quoteItem.unitOfMeasure,
      unitPrice: quoteItem.unitPrice,
      itemNumber: quoteItem.itemNumber,
      price: quoteItem.unitPrice,
    };
    return item;
  });
};

export const mapQuoteData = (quote: Quote): QuoteGridItem => {
  return {
    id: quote.id,
    quoteName: quote.displayName,
    city: quote.city,
    phone: quote.phoneNumber,
    address1: quote.address1,
    address2: quote.address2,
    state: quote.state,
    workType: quote.workType,
    creationDate: quote.creationDate,
    createdUserName: quote.createdUserName,
    lastModified: quote.lastModifiedDate,
    jobName: quote.jobName,
    quoteItems: quote.quoteItems?.map((quote) => quote),
    quoteNote: quote.quoteNotes,
    summary: {
      tax: getFormattedPrice(quote?.tax, ""),
      otherCharges: getFormattedPrice(quote.otherCharges, ""),
      subtotal: getFormattedPrice(quote.subTotal, ""),
      total: getFormattedPrice(quote.total, ""),
    },
    addtionalQuoteItems: quote.addtionalQuoteItems,
    standardQuoteItems: quote.standardQuoteItems,
    expires: quote.expires,
    status: quote.status,
    quoteType: quote.quoteType,
  };
};

export const formatPhone = (phone: string) => {
  if (phone) {
    phone = phone.replace(/^(\d{3})\-*(\d{3})\-*(\d{4})$/, "$1-$2-$3");
  }
  return phone;
};

/**
 * This will format with dollar symbol and decimal two digits.
 */

export const getFormattedPrice = (price: number, format: string): string => {
  if (price) {
    return `${format || "$"}${price.toFixed(2)}`;
  } else {
    return "";
  }
};

const mapQuoteResponseToCSVObjectList = (quote: Quote, pricing: boolean) => {
  const { addtionalQuoteItems, standardQuoteItems } = quote;
  const quoteCSVItemList: QuoteCSVItem[] = [];
  standardQuoteItems?.forEach((item) => {
    quoteCSVItemList.push(mapQuoteResponseToCSVObject(quote, pricing, item));
  });
  addtionalQuoteItems?.forEach((item) => {
    quoteCSVItemList.push(
      mapQuoteResponseToCSVObject(quote, pricing, undefined, item),
    );
  });
  if (pricing) {
    WITH_PRICE_LABELS.QUOTE.map((itemDesc) =>
      quoteCSVItemList.push(
        mapQuoteResponseToCSVObject(
          quote,
          pricing,
          undefined,
          undefined,
          itemDesc,
        ),
      ),
    );
  }
  return quoteCSVItemList;
};

const mapQuoteResponseToCSVObject = (
  quote: Quote,
  pricing: boolean,
  standardQuoteItems?: StandardQuoteItem,
  addtionalQuoteItems?: QuoteItem,
  customItemDescription?: string,
) => {
  let quoteCSVItem: QuoteCSVItem = {
    QuoteName: quote.displayName,
    QuoteExpires: quote.expires,
    QuoteStatus: quote.status,
    PhoneNumber: formatPhone(quote.phoneNumber),
    CreatedDate: quote.creationDate,
    CreatedBy: quote.createdUserName,
    LastModifiedDate: quote.lastModifiedDate,
    JobName: quote.jobName,
    Address: `${quote.address1} ${quote.mailingCity}, ${quote.mailingState}`,
    QuoteNote: quote.quoteNotes,
    ItemDescription: standardQuoteItems
      ? decodeText(standardQuoteItems.displayName)
      : addtionalQuoteItems
      ? addtionalQuoteItems.itemDescription
      : customItemDescription,
    ItemNumber: standardQuoteItems ? standardQuoteItems.itemNumber : "",
    ProductNumber: standardQuoteItems ? standardQuoteItems.productNumber : "",
    UoM: standardQuoteItems
      ? standardQuoteItems.unitOfMeasure
      : addtionalQuoteItems?.unitOfMeasure || "",
    Quantity: standardQuoteItems
      ? standardQuoteItems.quantity
      : addtionalQuoteItems?.quantity || "",
  };
  if (pricing) {
    quoteCSVItem.UnitPrice = standardQuoteItems
      ? standardQuoteItems.unitPrice
      : "";
    quoteCSVItem.ItemTotal = standardQuoteItems
      ? standardQuoteItems.itemTotalPrice
      : customItemDescription === WITH_PRICE_LABELS.QUOTE[0]
      ? quote.otherCharges
      : customItemDescription === WITH_PRICE_LABELS.QUOTE[1]
      ? quote.tax
      : customItemDescription === WITH_PRICE_LABELS.QUOTE[2]
      ? quote.subTotal
      : customItemDescription === WITH_PRICE_LABELS.QUOTE[3]
      ? quote.total
      : "";
  }
  return quoteCSVItem;
};

export const exportDownload = async (
  downloadType: string,
  pricing: boolean,
  type: string,
  handleClose: Function,
  orderResponse: any,
): Promise<void> => {
  if (downloadType === DOWNLOAD_TYPE.ORDER) {
    const { order } = orderResponse?.result as OrderResult;
    if (type === DOWNLOAD_FILE_TYPE.XLSX || type === DOWNLOAD_FILE_TYPE.CSV) {
      const csvObjList: OrderCSVItem[] = mapOrderListAsCsvOrXlsx(
        orderResponse,
        pricing,
      );
      saveCSVOrXLSX(type, csvObjList, DOWNLOAD_FILE_NAME.ORDER);
    } else {
      // PDF download
      const { orderId, accountId, accountToken } = order;
      const showPrice = pricing.toString();
      const result = await getOrderPdfBlob({
        accountId,
        orderId,
        accountToken,
        showPrice,
      });
      if (result) {
        let id = orderId;
        downloadPdf(result as Blob, id);
      }
    }
  } else {
    //Quote Download
  }
};

const mapOrderListAsCsvOrXlsx = (
  orderResponse: OrderResponse,
  pricing: boolean,
) => {
  const orderCSVObjList: OrderCSVItem[] = [];
  const orderHistoryPermissions = {
    withoutPrice: false,
  } as OrderHistoryPermissions;
  let { lineItems, order } = orderResponse?.result;
  order.job &&
    order.shipping &&
    order.shipping.address &&
    lineItems?.forEach((item) => {
      orderCSVObjList.push(
        mapOrderResponseToCSVObject(
          order,
          orderHistoryPermissions,
          pricing,
          "",
          item,
        ),
      );
    });
  if (pricing) {
    WITH_PRICE_LABELS.ORDER.map((itemDescription) => {
      orderCSVObjList.push(
        mapOrderResponseToCSVObject(
          order,
          orderHistoryPermissions,
          pricing,
          itemDescription,
        ),
      );
    });
  }
  return orderCSVObjList;
};

const saveCSVOrXLSX = (
  type: string,
  //csvObjectList: OrderCSVItem[] | QuoteCSVItem[],
  csvObjectList: OrderCSVItem[],
  fileName?: string,
) => {
  if (csvObjectList) {
    let name = fileName ? fileName : DOWNLOAD_FILE_NAME.DEFAULT;
    name = `${name}.${
      type === DOWNLOAD_FILE_TYPE.CSV ? DOWNLOAD_FILE_TYPE.CSV : "xlsx"
    }`;
    const option: xlsx.WritingOptions = {
      type: "array",
      cellDates: false,
      bookSST: false,
      bookType: type === "csv" ? "csv" : "xlsx",
      sheet: "",
      compression: false,
      ignoreEC: true,
    };
    const sheet = xlsx.utils.json_to_sheet(csvObjectList);
    const book = xlsx.utils.book_new();
    xlsx.utils.book_append_sheet(book, sheet, fileName);
    xlsx.writeFile(book, name, option);
  }
};

const mapOrderResponseToCSVObject = (
  order: Order,
  orderHistoryPermissions: OrderHistoryPermissions,
  pricing: boolean,
  customItemDescription?: string,
  item?: LineItem,
) => {
  const orderCSVObj: OrderCSVItem = {
    OrderPlacedDate: order.orderPlacedDate,
    OrderNumber: order.orderId,
    OrderStatus: formatStatus(order.orderStatusCode),
    CustPO: order.purchaseOrderNo,
    JobName: order.job.jobName,
    ShippingAddress: formatAddress(order.shipping.address),
    ItemDescription: item
      ? decodeText(item.itemOrProductDescription)
      : customItemDescription,
    ItemNumber: item ? item.itemNumber : "",
  };
  if (pricing) {
    orderCSVObj.UnitPrice =
      item && item.unitPrice > 0 ? item.unitPrice : undefined;
    orderCSVObj.UoM = item ? item.unitOfMeasure : "";
    orderCSVObj.ShippedQty = item ? item.quantity : "";
    orderCSVObj.SubTotal =
      item && item.subTotal > 0
        ? item.subTotal
        : customItemDescription === WITH_PRICE_LABELS.ORDER[2]
        ? order.total
        : customItemDescription === WITH_PRICE_LABELS.ORDER[0]
        ? order.otherCharges || 0
        : customItemDescription === WITH_PRICE_LABELS.ORDER[1]
        ? order.tax
        : undefined;
  }
  return orderCSVObj;
};

const downloadPdf = (blob: Blob, id: string) => {
  if (!blob) {
    return;
  }
  const downloadURL = URL.createObjectURL(blob);
  let link: HTMLAnchorElement | null = document.createElement("a");
  link.href = downloadURL;
  link.download = id + ".pdf";
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
  link.remove();
  URL.revokeObjectURL(downloadURL);
};

const formatAddress = (shipAdd: BranchAddress) => {
  return [
    shipAdd.address1,
    shipAdd.address2,
    shipAdd.address3,
    `${shipAdd.city}, ${shipAdd.state}`,
  ];
};

const formatStatus = (orderStatus: OrderStatus) => {
  return ORDER_STATUS[orderStatus ? orderStatus : "default"];
};

export const decodeText = (rawHTML: string) => {
  return decode(rawHTML);
};

const HTTP_METHOD = {
  GET: "GET",
  POST: "POST",
};

const fetcher = async (resource: any, init: any) => {
  try {
    return await fetch(resource, init).then((res: { json: () => any }) => {
      const resp = res.json().then((data: any) => {
        return data;
      });
      return resp;
    });
  } catch (error) {
    return [];
  }
};
const pdfFetcher = async (resource: any, init: any) => {
  try {
    return await fetch(resource, init).then((res: { blob: () => any }) =>
      res.blob(),
    );
  } catch (error) {
    console.log("Error in fetching data in pdfFetcher");
    return [];
  }
};

export const fetchOrderDetailsQuery = async ({
  orderId,
  accountId,
  accountToken,
  showDT,
}: any) =>
  await fetcher(
    `${BASE_URL}/orderdetail?orderId=${orderId}&accountId=${accountId}&accountToken=${accountToken}&showDT=${showDT}`,
    {
      credentials: "include",
      headers: {},
      withCredetials: true,
    },
  );
export const fetchItemsDetails = async (value: any) =>
  await fetcher(
    `${BASE_URL}/itemDetails?productId=${value?.productId}&itemNumber=${value?.itemNumber}&accountId=${accountId}`,
    {
      credentials: "include",
      withCredetials: true,
    },
  );

export async function getOrderPdfBlob({
  accountId,
  orderId,
  accountToken,
  showPrice,
}: {
  accountId: string;
  orderId: string;
  accountToken: string;
  showPrice: string;
}) {
  const resp = await pdfFetcher(
    `${BASE_URL}/downloadOrderDetailAsPDF?orderId=${orderId}&accountId=${accountId}&showPrice=true&accountToken=${accountToken}`,
    {
      credentials: "include",
      withCredetials: true,
      responseHandler: async (res: { blob: () => any }) => await res.blob(),
      cache: "no-cache",
    },
  );
  return resp;
}

export async function getQuotePdfBlob({
  quoteId,
  account,
  showPrice,
}: {
  quoteId: string;
  account: string;
  showPrice: boolean;
}) {
  let url = `${BASE_URL}/downloadQuoteAsPDF?quoteId=${quoteId}&account=${account}&showPrice=${showPrice}`;
  const resp = await pdfFetcher(url, {
    credentials: "include",
    withCredetials: true,
    responseHandler: async (res: { blob: () => any }) => await res.blob(),
    cache: "no-cache",
  });
  return resp;
}

export const exportQuote = async (
  quote: Quote,
  pricing: boolean,
  type: string,
  handleClose: Function,
) => {
  handleClose();
  const { mincronId, accountNumber } = quote;
  const quoteCSVItemList: QuoteCSVItem[] = mapQuoteResponseToCSVObjectList(
    quote,
    pricing,
  );
  if (type === DOWNLOAD_FILE_TYPE.XLSX || type === DOWNLOAD_FILE_TYPE.CSV)
    saveCSVOrXLSX(type, quoteCSVItemList, mincronId);
  else {
    const result = await getQuotePdfBlob({
      quoteId: mincronId,
      account: accountNumber,
      showPrice: pricing,
    });
    if (result) {
      let id = mincronId;
      downloadPdf(result as Blob, id);
    }
  }
};
