// ITEMS CONTANTS
export const INVOICE_DOWNLOAD_AS_BUTTON = 'Download As';
export const INVOICE_DOWNLOAD_BUTTON = 'Download';
export const ADD_CUSTOM_ITEM = 'Custom Item';
export const NO_PRODUCT_DOCUMENT_LABEL = 'No Product';

export const ORDER_SUMMARY = 'Order Summary';
export const SUB_TOTAL = 'Sub Total';
export const PRICE_CALCULATED_TEXT = 'Price calculated at invoicing';
export const OTHER_CHARGES = 'Other Charges';
export const TAX = 'Tax';
export const TOTAL = 'Total';
export const PRODUCT = 'Product';

// Tab buttons
export const ITEM_LIST_BUTTON = 'Item List';
export const CUSTOM_ITEM_LIST_BUTTON = 'Custom Items';
export const PRODUCT_DOCUMENT_BUTTON = 'Product Documents';
export const WARRANTY_INFO_BUTTON = 'Warranty Submission Information';

export const SHIPPED_QTY = 'Shipped Qty';
export const ORDER_QTY = 'Order Qty';
// ORDER CONTANTS
export const ORDER = 'Order';

// BUTTON TAB CONSTANT
export const ADD_ORDER_TO_CART = 'Add Order To Cart';
export const ADD_ITEMS_TO_TEMPLATE = 'Add Items To Template';
export const ADD_ITEMS_TO_FAVORITES = 'Add Items To Favorites';
export const STORE_DETAILS_GET_ALERTS = 'Get Alerts';

// Tab Panel Constants
export const Panel = {
  Item: ITEM_LIST_BUTTON,
  CustomItem: CUSTOM_ITEM_LIST_BUTTON,
  Product: PRODUCT_DOCUMENT_BUTTON,
  Warranty: WARRANTY_INFO_BUTTON,
};
export const WARRANTY_BROCHURE = 'Warranty Brochure';
export const WARRANTY_INFORMATION = 'Warranty Information';

// Order/Invoice "Download As" button options
export const FILE_TYPE = 'File type';
export const EXCEL = 'Excel';
export const CSV = 'CSV';
export const PDF = 'PDF';
export const INCLUDE_PRICE = 'Include Price';

export const QUOTE_TYPE = {
  QUOTE_DRAFT: 'OPEN',
  QUOTE_IN_PROCESS: 'INPROGRESS',
  QUOTE_RECIEVED: 'ORDERED',
};

export const TABLE_LABELS = {
  ITEM_NUMBER: 'Item #:',
  PRODUCT_NUMBER: 'Product Number:',
  DETAILS: 'Details',
  UNIT_PRICE: 'Unit Price',
  UNIT: 'Unit',
  PRICE: 'Price',
  QUANTITY: 'Qty',
  ACTIONS: 'Actions',
  PRODUCT: 'Products',
};
export const ORDER_TYPE = 'ORDER';

export const WARRANTY_MESSAGE = {
  ORDER:
    'Warranty information is not provided for the products listed in your order. For questions, please reach out to your local branch.',
  QUOTE:
    'Warranty information is not available for products listed in this quote. For questions, please reach out to your local branch.',
};

export const VIEW_SUBMMITED_LABELS = {
  QUOTE_DETAIL: 'QUOTE DETAIL',
  VIEW_SUBMITTED_REQUEST: 'View Submitted Request',
  TABLE_HEAD_LABEL: ['Product', 'Details', 'Unit', 'Qty'],
  PRINT_LABEL: 'Print',
  CLOSE_LABEL: 'Close',
  CUSTOM_ITEM_HEADING: 'Custom Items',
};

export const ADDRESS_1 = 'Address 1';
export const ADDRESS_2 = 'Address 2';
export const STATE = 'State';
export const JOB_NAME = 'Job Name';
export const LAST_MODIFIED = 'Last Modified';
export const JOB = 'Job Account *';
export const WORK_TYPE = 'Work Type';
export const CITY = 'City';
export const PHONE_NUMBER = 'Phone Number';
export const CREATED = 'Created';
export const QUOTE = 'Quote Name';
// return order status
export const ORDER_STATUS = {
  I: 'Invoiced',
  R: 'Delivered',
  P: 'Delivered',
  C: 'Processing',
  O: 'Ready Delivery / Pick up',
  K: 'Processing',
  N: 'Pending',
  default: 'Status Unavailable',
};

// to add price labels if exported file contains price check

export const WITH_PRICE_LABELS = {
  ORDER: ['Other charges', 'Order Tax', 'Order Total'],
  QUOTE: ['Other charges', 'Tax', 'Sub Total', 'Total'],
};

export const DOWNLOAD_FILE_TYPE = { CSV: 'csv', XLSX: 'excel', PDF: 'pdf' };

export const DOWNLOAD_FILE_NAME = {
  DEFAULT: 'Summary',
  ORDER: 'OrderSummary',
};

export const DOWNLOAD_TYPE = {
  QUOTE: 'Quote',
  ORDER: 'Order',
};
export const DATE_FORMATS=["MM-DD-YYYY"];