export interface QuoteResponse {
  callBackParam: any;
  code: any;
  messageCode: any;
  messages: any;
  quote: Quote;
  result: any;
  success: boolean;
  status: string;
}

export interface Quote {
  accountNumber: string;
  address1: string;
  address2: string;
  addtionalQuoteItems: QuoteItem[];
  atgOrderId: any;
  bidLockEmail: any;
  bidLockTimestamp: any;
  city: string;
  createdUser: any;
  createdUserName: string;
  creationDate: string;
  creationDate4Integration: string;
  deleteStatus: boolean;
  displayName: string;
  documentName: any;
  expires: any;
  formatOtherCharges: any;
  formatQuoteTotal: any;
  formatSubTotal: any;
  formatTax: any;
  formatTotal: any;
  gst: any;
  id: string;
  jobName: string;
  jobNumber: string;
  lastModifiedDate: string;
  lastModifiedDate4Integration: string;
  lastModifiedUser: any;
  lastModifiedUserEmail: string;
  mailingAddress1: any;
  mailingAddress2: any;
  mailingAddress3: any;
  mailingCity: any;
  mailingState: any;
  mailingZip: any;
  mincronId: string;
  mincronOrderId: any;
  otherCharges: any;
  pageControl: any;
  phoneNumber: string;
  postalCode: any;
  profileEmail: string;
  profileId: string;
  quoteItems: QuoteItem[];
  quoteNotes: string;
  quoteTotalPrice: any;
  siteID: string;
  sourceOfSale: any;
  standardQuoteItems: StandardQuoteItem[];
  state: string;
  status: string;
  statusDescription: any;
  subTotal: any;
  tax: any;
  total: any;
  uploadedFiles: any;
  workType: string;
  workTypeName: string;
  quoteType: string;
}

export interface QuoteItem {
  bodyComments: any;
  color: any;
  currencySymbol: any;
  deleteStatus: any;
  dimensions: any;
  displayName: string;
  formatItemTotalPrice: any;
  formatUnitPrice: any;
  id: string;
  imageOnErrorUrl: string;
  imageURL: string;
  itemDescription: string;
  itemNumber: string;
  itemTotalPrice: any;
  itemType: string;
  mincronDisplayName: any;
  PDPUrl: string | null;
  productId: string | null;
  productNumber: string | null;
  quantity: number;
  stickerImageURL: any;
  unitOfMeasure: string;
  unitPrice: any;
}
export interface QuoteSummary {
  tax: string;
  total: string;
  subtotal: string;
  otherCharges: string;
}

export interface QuoteGridItem {
  id: string;
  quoteName: string;
  city: string;
  phone: string;
  address1: string;
  address2: string;
  state: string;
  workType: string;
  creationDate: string;
  createdUserName: string;
  lastModified: string;
  jobName: string;
  quoteItems: QuoteItem[];
  quoteNote: string;
  summary: QuoteSummary;
  addtionalQuoteItems: QuoteItem[];
  standardQuoteItems: QuoteItem[];
  expires: string;
  status: string;
  quoteType: string;
}

export interface StandardQuoteItem {
  bodyComments: any;
  color: any;
  currencySymbol: any;
  deleteStatus: any;
  dimensions: any;
  displayName: string;
  formatItemTotalPrice: any;
  formatUnitPrice: any;
  id: string;
  imageOnErrorUrl: string;
  imageURL: string;
  itemDescription: string;
  itemNumber: string;
  itemTotalPrice: any;
  itemType: string;
  mincronDisplayName: any;
  PDPUrl: string;
  productId: string;
  productNumber: string;
  quantity: number;
  stickerImageURL: any;
  unitOfMeasure: string;
  unitPrice: any;
}
export interface UpdateQuoteResponse {
  result: any;
  success: boolean;
  messages: string;
}

export interface QuoteBaseItem {
  displayName: string;
  itemType: ItemType;
  itemNumber: string;
  quantity: number;
  uom: UOM;
}

export type UpdateQuoteAction = "CREATE_ITEM" | "DELETE_ITEM";
export type ItemType = "E";
export type UOM = "EA";

export interface QuoteCSVItem {
  QuoteName?: string;
  QuoteExpires?: string;
  QuoteStatus?: string;
  PhoneNumber?: string;
  CreatedDate?: string;
  CreatedBy?: string;
  LastModifiedDate?: string;
  JobName?: string;
  Address?: string;
  QuoteNote?: string;
  ItemDescription?: string | any;
  ItemNumber?: string;
  ProductNumber?: string;
  UnitPrice?: string | number;
  UoM?: string;
  Quantity?: string | number;
  ItemTotal?: string;
}

//View Submitted Interfaces
export interface IViewSubmittedForm {
  quoteDetails: IQuoteDetails;
  itemList: Array<ICustomItem>;
  customItemsList: Array<ICustomItem>;
}

export interface IQuoteDetails {
  "Quote Name"?: string;
  "Phone Number"?: string;
  Created?: any;
  "Last Modified"?: any;
  "Work Type"?: string;
  "Job Name"?: string;
  "Address 1"?: string;
  "Address 2"?: string;
  City?: string;
  State?: string;
}

export interface ICustomItem {
  Product?: string;
  Details?: string;
  Unit?: string;
  Qty?: number;
}

// keys of the IQuoteDetails
export type QuoteDetailsKeys = Array<keyof IQuoteDetails>;
