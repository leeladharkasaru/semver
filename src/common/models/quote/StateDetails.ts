export interface State {
  value: string;
  key: string;
}
