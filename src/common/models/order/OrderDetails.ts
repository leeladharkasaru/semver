export interface OrderResponse {
  result: OrderResult;
  success: boolean;
  messages: string;
}

export interface OrderResult {
  lineItems: LineItem[];
  DTInfo: string;
  branchDTEnabled: boolean;
  hasDTError: boolean;
  DTEnabled: boolean;
  DTStatus: {
    displayName: string;
    contKey: string;
    contValue: string;
    statusEndDate: string;
    infos: [
      {
        defaultValue: string;
        imageUrl: string;
        active: true;
        key: string;
        value: string;
      },
    ];
  };
  EVEligibleForUpgrade: boolean;
  DTPhotos: string;
  EVReportId: number;
  order: Order;
}

export interface LineItem {
  itemNumber: string;
  itemType: string;
  sendToMincronDirectly: boolean;
  unitOfMeasureDisplay: string;
  displayQuantityMsg: string;
  pdpUrl: string;
  description: string;
  productOrItemNumber: string;
  unitPriceNull: boolean;
  subTotal: number;
  productOrItemDescription: string;
  isAddedToFavorites: boolean;
  orderQuantity: number;
  productImageUrl: string;
  unitPrice: number;
  itemUnitPrice: number;
  quantity: number;
  itemOrProductDescription: string;
  productId: string;
  unitOfMeasure: string;
  skuPDPDisplayTitle: string;
  orderUnitOfMeasure: string;
  productOnErrorImageUrl: string;
  productNumber: string;
  orderOfMeasureDisplay: string;
  orderSubTotal: number;
  shipQuantity: number;
  ATGItemMapping: string;
  itemSubTotal: number;
  nonStockItem: boolean;
  sendDescriptionOnlyToMincron: boolean;
  lineComments: string;
  orderUnitPrice: number;
  quantityMsg: string;
  extendedDescription: string;
  subTotalNull: boolean;
}

export interface Order {
  atgOrderID: string;
  shipInfoMsg: string;
  orderId: string;
  orderRelatedDocuments: string;
  otherCharges: number;
  totalMsg: string;
  source: string;
  subTotal: number;
  taxNull: boolean;
  creditHold: string;
  purchaseOrderNo: string;
  orderPlacedDate: string;
  total: number;
  shipping: Shipping;
  inStore: string;
  showShipTips: string;
  orderStatusValue: string;
  omniToken: boolean;
  otherChargesNull: boolean;
  UUID: string;
  sellingBranch: string;
  onHold: boolean;
  specialInstruction: string;
  displayTotalMsg: string;
  showDropDown: string;
  tax: number;
  accountId: string;
  shipTipsMsg: string;
  orderStatusCode: OrderStatus;
  shipContentMsg: string;
  accountToken: string;
  job: Job;
  subTotalNull: boolean;
  showDeliveryNotification: boolean;
  totalNull: boolean;
}

export interface Job {
  jobName: string;
  jobLocationAddress2: string;
  jobLocationAddress3: string;
  jobLocationZipCode: string;
  jobLocationCity: string;
  jobLocationAddress1: string;
  customerName: string;
  jobNumber: string;
  customerTelephoneNumber: string;
  jobLocationState: string;
}

export interface Shipping {
  shippingBranch: string;
  address: Address;
  shippingBranchDisplayName: string;
  shippingMethod: string;
}

export interface Address {
  address3: string;
  address2: string;
  city: string;
  address1: string;
  postalCode: string;
  state: string;
}

export type OrderStatus = "P" | "R" | "I" | "N" | "C" | "K" | "O" | undefined;

export interface BranchAddress {
  postalCode?: string;
  state?: string;
  address1?: string;
  address2?: string;
  address3?: string;
  country?: string;
  city?: string;
}

export interface OrderCSVItem {
  OrderPlacedDate?: string;
  OrderNumber?: string;
  OrderStatus?: string;
  CustPO?: string;
  JobName?: string;
  ShippingAddress?: string | any;
  ItemDescription?: string;
  ItemNumber?: string;
  UnitPrice?: number | string;
  UoM?: string;
  ShippedQty?: number | string;
  SubTotal?: number;
}
export type OrderHistoryPermissions = {
  queryList: boolean;
  queryDetail: boolean;
  withPrice: boolean;
  withoutPrice: boolean;
};
