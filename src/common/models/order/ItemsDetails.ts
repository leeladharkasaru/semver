export interface ItemsDetails {
  result: null;
  product: Product;
  skuList: CurrentSku[];
  currentSKU: CurrentSku;
  resource: Resource;
  variations: ItemsDetailsVariations;
  specification: Specification;
  message: null;
}

export interface CurrentSku {
  itemNumber: string;
  uomlist?: string[];
  productNumber: string;
  hoverAttributes: null;
  currentUOM: string;
  skuShortDesc: null;
  itemImage: string;
  swatchImage: string;
  thumbImage: string;
  manufactureNumber?: string;
  auxiliaryImages: Image[];
  variations: CurrentSKUVariations;
  heroImages?: Image[];
}

export interface Image {
  image: string;
}

export interface CurrentSKUVariations {
  color: string[];
  size: string[];
  width: string[];
  length: string[];
}

export interface Product {
  itemNumber: string;
  skuList: null;
  productId: string;
  productOnErrorImage: string;
  relatedProducts: RelatedProduct[];
  productName: string;
  url: null;
  internalProductName: string;
  baseProductName: string;
  manufactureNumber: string;
  productImage: string;
  productAdditionalOnErrorImage: string;
  shortDesc: null;
  categories: Category[];
  brand: string;
  longDesc: string;
}

export interface Category {
  facetId: string;
  categoryName: string;
  categoryId: string;
}

export interface RelatedProduct {
  internalProductName: string;
  productImage: string;
  productId: string;
  productOnErrorImage: string;
  categories: Category[];
  brand: string;
  productName: string;
  url: string;
}

export interface Resource {
  "Designer Shingles Brochure": string;
  "Guarantee Information": string;
  "Safety Data Sheet": string;
  "Residential Full Line Brochure": string;
  "Product Installation Video": string;
  "Installation Instructions": string;
  "Florida Product Approval": string;
  "Product Data Sheet": string;
  "Glenwood Shingles Brochure": string;
  "Detail Drawing": string;
}

export interface Specification {
  Length: string;
  "Bundles Per Square": string;
  "Applicable Standards": string;
  "Wind Resistance": string;
  "Shingles Per Square": string;
  Exposure: string;
  Width: string;
  "Base Material": string;
}

export interface ItemsDetailsVariations {
  color: Color;
  size: Size;
}

export interface Color {
  "Adobe Clay": string[];
  "Autumn Harvest": string[];
  "Dusky Grey": string[];
  "Golden Prairie": string[];
  "Weathered Wood": string[];
  "Chelsea Grey": string[];
}

export interface Size {
  '12-1/2" x 36-5/16"': string[];
}
