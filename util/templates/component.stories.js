module.exports = (componentName) => ({
  content: `import React from "react";
import ${componentName} from "./${componentName}";

export default {
    title: "${componentName}"
};

export const WithBar = () => <${componentName} beacon="bar" />;
export const WithBaz = () => <${componentName} beacon="baz" />;
`,
  extension: `.stories.tsx`
});
