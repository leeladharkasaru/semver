module.exports = (componentName) => ({
  content: `import React from "react";
import { ${componentName}Props } from "./${componentName}.types";
import "./${componentName}.scss";

const ${componentName}: React.FC<${componentName}Props> = ({ beacon }) => (
    <div data-testid="${componentName}" className="beacon-bar">{beacon}</div>
);

export default ${componentName};
`,
  extension: `.tsx`
});
