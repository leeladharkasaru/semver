module.exports = (componentName) => ({
  content: `import React from "react";
import { render } from "@testing-library/react";
import ${componentName} from "./${componentName}";
import { ${componentName}Props } from "./${componentName}.types";

describe("Test Component", () => {
  let props: ${componentName}Props;

  beforeEach(() => {
    props = {
      beacon: "bar"
    };
  });

  const renderComponent = () => render(<${componentName} {...props} />);

  it("should render beacon text correctly", () => {
    //Assign
    props.beacon = "Test component";
    const { getByTestId } = renderComponent();
    //Act
    const component = getByTestId("${componentName}");
    //Assert
    expect(component).toHaveTextContent("Test component");
  });
});
`,
  extension: `.test.tsx`
});
