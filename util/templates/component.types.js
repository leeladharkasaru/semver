module.exports = (componentName) => ({
  content: `export interface ${componentName}Props {
    beacon: string;
}
`,
  extension: `.types.ts`
});
