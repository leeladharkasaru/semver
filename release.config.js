module.exports = {
    branches: [{ name: '*' }], // Allows all branches
    repositoryUrl: "https://gitlab.com/beacon-ecm/commercial/ui/beacon-component-library.git", // replace with your repository URL
    plugins: [
      '@semantic-release/commit-analyzer',
      '@semantic-release/release-notes-generator',
      '@semantic-release/changelog',
      '@semantic-release/git',
      '@semantic-release/gitlab'
    ]
  };