###############################
# BUILD FOR LOCAL DEVELOPMENT #
###############################

FROM node:18-alpine AS development

# Define the build argument for the GitLab auth token
# Set the GitLab auth token as an environment variable
# Used to authenticate with the GitLab npm registry
# ARG GITLAB_AUTH_TOKEN
# ENV GITLAB_AUTH_TOKEN=$GITLAB_AUTH_TOKEN

# Create app directory
WORKDIR /app

COPY package*.json ./
COPY docker/certs/Umbrella_Root_CA.pem /etc/ssl/certs/
ENV NODE_EXTRA_CA_CERTS=/etc/ssl/certs/Umbrella_Root_CA.pem

COPY . .
# Clear npm cache
RUN npm cache clean --force
RUN npm install -g npm
RUN npm install

RUN npm run build

EXPOSE 6006

CMD ["npm", "run", "start"]


